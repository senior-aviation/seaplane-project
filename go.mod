module gitlab.com/senior-aviation/seaplane

go 1.16

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/durango/go-credit-card v0.0.0-20220120003050-58f94081e6d2
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/render v1.0.1
	github.com/golang/snappy v0.0.3 // indirect
	github.com/google/go-cmp v0.5.7 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/swaggo/http-swagger v1.1.1
	github.com/swaggo/swag v1.7.0
	go.mongodb.org/mongo-driver v1.7.1
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	golang.org/x/net v0.0.0-20220325170049-de3da57026de // indirect
	golang.org/x/sys v0.0.0-20220328115105-d36c6a25d886 // indirect
	golang.org/x/tools v0.1.5 // indirect
)
