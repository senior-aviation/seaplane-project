package order

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/order"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

const (
	duplicateCode = 11000
	fault         = 0.01
)

// Collection the order collection structure.
type Collection struct {
	collection *mongo.Collection
}

// NewProfileCollection initializes the order collection.
func NewProfileCollection(collection *mongo.Collection) *Collection {
	return &Collection{collection: collection}
}

// CreateOrder creates new order.
func (c *Collection) CreateOrder(ctx context.Context, order *order.Order) (int, error) {
	if order == nil {
		return 0, errors.ErrEmptyStruct
	}

	id, err := c.retrieveTheLastOrderID(ctx)
	if err != nil && err != mongo.ErrNilDocument {
		return 0, errors.ErrOrderIDCreation
	}

	id += 1

	now := time.Now().UTC()

	body := bson.D{
		{Key: "_id", Value: id},
		{Key: "created_at", Value: now},
		{Key: "updated_at", Value: now},
		{Key: "status", Value: order.Status},
		{Key: "from", Value: order.From},
		{Key: "to", Value: order.To},
		{Key: "departure_time", Value: order.DepartureTime},
		{Key: "arrival_time", Value: order.ArrivalTime},
		{Key: "price", Value: order.Price},
		{Key: "passengers", Value: order.Passengers},
		{Key: "user_id", Value: order.UserID},
		{Key: "joined_users", Value: order.JoinedUsers},
		{Key: "phone_number", Value: order.PhoneNumber},
		{Key: "email", Value: order.Email},
		{Key: "sharing", Value: order.Sharing},
	}

	result, err := c.collection.InsertOne(ctx, body)
	if isDup(err) {
		return 0, errors.ErrAlreadyExists
	} else if err != nil {
		return 0, err
	}

	if res, ok := result.InsertedID.(int32); ok {
		return int(res), nil
	}

	return 0, errors.ErrObjectID
}

// UpdateOrder updates the order info.
func (c *Collection) UpdateOrder(ctx context.Context, order *order.Order) error {
	if order == nil {
		return errors.ErrEmptyStruct
	}

	if order.ID == 0 {
		return errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: int32(order.ID),
	}}

	body := bson.D{
		{
			Key: "$set",
			Value: bson.D{
				{Key: "updated_at", Value: time.Now().UTC()},
				{Key: "status", Value: order.Status},
				{Key: "from", Value: order.From},
				{Key: "to", Value: order.To},
				{Key: "price", Value: order.Price},
				{Key: "departure_time", Value: order.DepartureTime},
				{Key: "arrival_time", Value: order.ArrivalTime},
				{Key: "passengers", Value: order.Passengers},
				{Key: "user_id", Value: order.UserID},
				{Key: "joined_users", Value: order.JoinedUsers},
				{Key: "phone_number", Value: order.PhoneNumber},
				{Key: "email", Value: order.Email},
				{Key: "sharing", Value: order.Sharing},
			},
		},
	}

	result, err := c.collection.UpdateOne(ctx, filter, body)
	if err != nil {
		return err
	}

	if result.ModifiedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// OrderByID returns the order info by its internal ID.
func (c *Collection) OrderByID(ctx context.Context, id int) (*order.Order, error) {
	if id == 0 {
		return nil, errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: int32(id),
	}}

	result := new(order.Order)

	err := c.collection.FindOne(ctx, filter).Decode(&result)
	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, errors.ErrDoesNotExist
	default:
		return nil, err
	}
}

// Orders returns the list of orders based on filters and paging info.
func (c *Collection) Orders(ctx context.Context, filter *order.Filter, paging *models.Paging) ([]*order.Order, error) {
	opts := options.FindOptions{}

	if paging != nil {
		if paging.Page != nil {
			opts.Skip = paging.Page
		}

		if paging.Limit != nil {
			opts.Limit = paging.Limit
		}

		opts.SetSort(bson.D{{Key: paging.OrderBy, Value: paging.OrderKey}})
	}

	cursor, err := c.collection.Find(ctx, buildFilters(filter), &opts)
	if err != nil {
		return nil, err
	}

	defer func(c *mongo.Cursor) {
		_ = c.Close(ctx)
	}(cursor)

	if cursor.RemainingBatchLength() == 0 {
		return nil, errors.ErrDoesNotExist
	}

	result := make([]*order.Order, 0, cursor.RemainingBatchLength())

	err = cursor.All(ctx, &result)

	return result, err
}

// SharingOrders returns the list of shareable orders.
func (c *Collection) SharingOrders(ctx context.Context, filters *order.SharingOrderFilters, paging *models.Paging) ([]*order.SharingOrderInfo, error) {
	opts := options.FindOptions{}

	if paging != nil {
		if paging.Page != nil {
			opts.Skip = paging.Page
		}

		if paging.Limit != nil {
			opts.Limit = paging.Limit
		}

		opts.SetSort(bson.D{{Key: paging.OrderBy, Value: paging.OrderKey}})
	}

	cursor, err := c.collection.Find(ctx, buildSharingFilters(filters), &opts)
	if err != nil {
		return nil, err
	}

	defer func(c *mongo.Cursor) {
		_ = c.Close(ctx)
	}(cursor)

	if cursor.RemainingBatchLength() == 0 {
		return nil, errors.ErrDoesNotExist
	}

	orders := make([]*order.Order, 0, cursor.RemainingBatchLength())

	if err = cursor.All(ctx, &orders); err != nil {
		return nil, err
	}

	result := make([]*order.SharingOrderInfo, 0, len(orders))

	for _, item := range orders {
		result = append(result, &order.SharingOrderInfo{
			ID:             item.ID,
			Status:         item.Status,
			From:           item.From,
			To:             item.To,
			AvailableSeats: item.Sharing.AvailableSeats,
			DepartureTime:  item.DepartureTime,
			ArrivalTime:    item.ArrivalTime,
			Price:          item.Price,
		})
	}

	return result, err
}

// SharingOrderInfo returns the sharing order info by its ID.
func (c *Collection) SharingOrderInfo(ctx context.Context, id int) (*order.SharingOrderInfo, error) {
	filter := bson.D{
		{
			Key:   "_id",
			Value: int32(id),
		},
		{
			Key:   "sharing.shareable",
			Value: true,
		},
	}

	res := new(order.Order)

	err := c.collection.FindOne(ctx, filter).Decode(res)
	switch err {
	case nil:
	case mongo.ErrNoDocuments:
		return nil, errors.ErrDoesNotExist
	default:
		return nil, err
	}

	return &order.SharingOrderInfo{
		ID:             res.ID,
		Status:         res.Status,
		From:           res.From,
		To:             res.To,
		AvailableSeats: res.Sharing.AvailableSeats,
		DepartureTime:  res.DepartureTime,
		ArrivalTime:    res.ArrivalTime,
		Price:          res.Price,
	}, nil
}

// Count counts the needed orders by the provided filters.
func (c *Collection) Count(ctx context.Context, filter *order.Filter) (int64, error) {
	return c.collection.CountDocuments(ctx, buildFilters(filter))
}

// JoinShareable is used to join the available shared flight.
func (c *Collection) JoinShareable(ctx context.Context, passengers []*order.Passenger, orderID int, userIDs []string) error {
	filter := bson.D{
		{
			Key:   "_id",
			Value: int32(orderID),
		},
	}

	update := bson.D{
		{
			Key:   "$inc",
			Value: bson.M{"sharing.available_seats": -len(passengers)},
		},
		{
			Key:   "$set",
			Value: bson.D{{Key: "updated_at", Value: time.Now().UTC()}},
		},
		{
			Key:   "$push",
			Value: bson.M{"passengers": bson.M{"$each": passengers}},
		},
	}

	if len(userIDs) != 0 {
		update = append(update,
			bson.E{
				Key:   "$push",
				Value: bson.M{"joined_users": bson.M{"$each": userIDs}},
			},
		)
	}

	res, err := c.collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	if res.ModifiedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// SharingOrdersCount counts the needed sharing orders by the provided filters.
func (c *Collection) SharingOrdersCount(ctx context.Context, filters *order.SharingOrderFilters) (int64, error) {
	return c.collection.CountDocuments(ctx, buildSharingFilters(filters))
}

// retrieveTheLastOrderID retrieves the last order ID to create the next sequential ID.
func (c *Collection) retrieveTheLastOrderID(ctx context.Context) (int, error) {
	opts := options.FindOptions{Limit: singleLimit()}

	opts.SetSort(bson.D{{Key: "created_at", Value: -1}})

	cursor, err := c.collection.Find(ctx, bson.D{}, &opts)
	if err != nil {
		return 0, err
	}

	if cursor.RemainingBatchLength() == 0 {
		return 0, nil
	}

	o := new(order.Order)

	for cursor.Next(ctx) {
		err = cursor.Decode(o)
		if err != nil {
			return 0, err
		}

		break
	}

	return o.ID, nil
}

// Earnings returns the total income for the specified period of time for analytics.
func (c *Collection) Earnings(ctx context.Context, request *models.AnalyticsRequest) (float64, error) {
	if request == nil {
		return 0, errors.ErrEmptyStruct
	}

	if request.From == nil {
		request.From = new(time.Time)
	}

	if request.To == nil {
		to := time.Now().UTC()
		request.To = &to
	}

	pipeline := bson.A{
		bson.M{
			"$match": bson.M{
				"status": order.ACTIVE,
				"updated_at": bson.M{
					"$gte": *request.From,
					"$lte": *request.To,
				},
			},
		},
		bson.M{
			"$group": bson.M{
				"_id": nil,
				"earning": bson.M{
					"$sum": bson.M{
						"$toDouble": "$price",
					},
				},
			},
		},
	}

	cursor, err := c.collection.Aggregate(ctx, pipeline)
	if err != nil {
		return 0, err
	}

	var res []struct {
		ID      interface{} `bson:"_id"`
		Earning float64     `bson:"earning"`
	}

	if cursor.RemainingBatchLength() == 0 {
		return 0, nil
	}

	if err = cursor.All(ctx, &res); err != nil {
		return 0, err
	}

	return res[0].Earning, nil
}

// PopularDestinations returns the number of distinct destinations for analytics.
func (c *Collection) PopularDestinations(ctx context.Context, request *models.AnalyticsRequest) (map[*order.Location]float64, error) {
	if request == nil {
		return nil, errors.ErrEmptyStruct
	}

	if request.From == nil {
		request.From = new(time.Time)
	}

	if request.To == nil {
		to := time.Now().UTC()
		request.To = &to
	}

	pipeline := bson.A{
		bson.M{
			"$match": bson.M{
				"status": order.ACTIVE,
				"updated_at": bson.M{
					"$gte": *request.From,
					"$lte": *request.To,
				},
			},
		},
		bson.M{
			"$group": bson.M{
				"_id": bson.M{
					"latitude":  "$to.latitude",
					"longitude": "$to.longitude",
					"name":      "$to.name",
				},
				"count": bson.M{"$sum": 1},
			},
		},
		bson.M{
			"$sort": bson.M{
				"count": -1,
			},
		},
	}

	cursor, err := c.collection.Aggregate(ctx, pipeline)
	if err != nil {
		return nil, err
	}

	var res []struct {
		ID    *order.Location `bson:"_id"`
		Count float64         `bson:"count"`
	}

	if err = cursor.All(ctx, &res); err != nil {
		return nil, err
	}

	result := make(map[*order.Location]float64, len(res))

	for _, item := range res {
		if item.ID == nil {
			continue
		}

		result[&order.Location{
			Latitude:  item.ID.Latitude,
			Longitude: item.ID.Longitude,
			Name:      item.ID.Name,
		}] = item.Count
	}

	return result, nil
}

// buildFilters builds the filters to search the needed order(s).
func buildFilters(filter *order.Filter) bson.D {
	result := bson.D{}

	if filter == nil {
		return result
	}

	if filter.OrderID != nil {
		result = append(result, bson.E{
			Key:   "_id",
			Value: int32(*filter.OrderID),
		})
	}

	if filter.Status != nil {
		result = append(result, bson.E{
			Key:   "status",
			Value: *filter.Status,
		})
	}

	if filter.UserID != nil {
		result = append(result, bson.E{
			Key: "$or",
			Value: bson.A{
				bson.D{{Key: "user_id", Value: filter.UserID}},
				bson.D{{Key: "joined_users", Value: filter.UserID}},
			},
		})
	}

	return result
}

// buildSharingFilters build the bson filters for getting the list of sharing orders.
func buildSharingFilters(filters *order.SharingOrderFilters) bson.D {
	result := bson.D{
		{
			Key:   "sharing.shareable",
			Value: true,
		},
		{
			Key:   "status",
			Value: order.ACTIVE,
		},
	}

	if filters.NumberOfPassengers != nil {
		result = append(result, bson.E{
			Key:   "sharing.available_seats",
			Value: bson.M{"$gte": *filters.NumberOfPassengers},
		})
	}

	if filters.Flexible && filters.DateFrom != nil {
		result = append(result, bson.E{
			Key: "departure_time",
			Value: bson.M{
				"$gte": (*filters.DateFrom).Add(-time.Hour * 24 * 5).UTC(),
				"$lte": (*filters.DateFrom).Add(time.Hour * 24 * 5).UTC(),
			},
		})
	} else if filters.DateFrom != nil {
		df := *filters.DateFrom

		dfBegin := time.Date(df.Year(), df.Month(), df.Day(), 0, 0, 0, 0, time.UTC)
		dfEnd := time.Date(df.Year(), df.Month(), df.Day(), 23, 59, 59, 0, time.UTC)

		result = append(result, bson.E{
			Key: "departure_time",
			Value: bson.M{
				"$gte": dfBegin,
				"$lte": dfEnd,
			},
		})
	} else {
		result = append(result, bson.E{
			Key: "departure_time",
			Value: bson.M{
				"$gte": time.Now().UTC(),
			},
		})
	}

	if filters.Flexible && filters.DateTo != nil {
		result = append(result, bson.E{
			Key: "arrival_time",
			Value: bson.M{
				"$gte": (*filters.DateTo).Add(-time.Hour * 24 * 5).UTC(),
				"$lte": (*filters.DateTo).Add(time.Hour * 24 * 5).UTC(),
			},
		})
	} else if filters.DateTo != nil {
		dt := *filters.DateTo

		dtBegin := time.Date(dt.Year(), dt.Month(), dt.Day(), 0, 0, 0, 0, time.UTC)
		dtEnd := time.Date(dt.Year(), dt.Month(), dt.Day(), 23, 59, 59, 0, time.UTC)

		result = append(result, bson.E{
			Key: "arrival_time",
			Value: bson.M{
				"$gte": dtBegin,
				"$lte": dtEnd,
			},
		})
	}

	if filters.LocationFrom != nil {
		result = append(result, bson.E{
			Key: "$and",
			Value: bson.A{
				bson.D{
					{
						Key: "from.latitude",
						Value: bson.M{
							"$gte": filters.LocationFrom.Latitude - fault,
							"$lte": filters.LocationFrom.Latitude + fault,
						},
					},
				},
				bson.D{
					{
						Key: "from.longitude",
						Value: bson.M{
							"$lte": filters.LocationFrom.Longitude + fault,
							"$gte": filters.LocationFrom.Longitude - fault,
						},
					},
				},
			},
		})
	}

	if filters.LocationTo != nil {
		result = append(result, bson.E{
			Key: "$and",
			Value: bson.A{
				bson.D{
					{
						Key: "to.latitude",
						Value: bson.M{
							"$gte": filters.LocationTo.Latitude - fault,
							"$lte": filters.LocationTo.Latitude + fault,
						},
					},
				},
				bson.D{
					{
						Key: "to.longitude",
						Value: bson.M{
							"$lte": filters.LocationTo.Longitude + fault,
							"$gte": filters.LocationTo.Longitude - fault,
						},
					},
				},
			},
		})
	}

	return result
}

// isDup checks if the returned error is about the duplicate writing.
func isDup(err error) bool {
	var e mongo.WriteException

	if err == &e {
		for _, we := range e.WriteErrors {
			if we.Code == duplicateCode {
				return true
			}
		}
	}

	return false
}

// singleLimit returns the limit of 1 as *int64.
func singleLimit() *int64 {
	var res int64 = 1

	return &res
}
