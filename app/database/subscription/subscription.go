package subscription

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/subscription"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

const (
	duplicateCode = 11000
)

// Collection the subscription's collection structure.
type Collection struct {
	collection *mongo.Collection
}

// NewSubscriptionCollection initializes the subscription collection.
func NewSubscriptionCollection(collection *mongo.Collection) *Collection {
	return &Collection{collection: collection}
}

// Subscribe subscribes the user to the system notifications.
func (c *Collection) Subscribe(ctx context.Context, request *subscription.Subscription) error {
	if request == nil {
		return errors.ErrEmptyStruct
	}

	if request.Email == "" {
		return errors.ErrMissingEmail
	}

	filter := bson.D{
		{
			Key:   "email",
			Value: request.Email,
		},
	}

	s := new(subscription.Subscription)

	now := time.Now().UTC()

	if err := c.collection.FindOne(ctx, filter).Decode(s); err != nil {
		if err != mongo.ErrNoDocuments {
			return err
		}

		body := bson.D{
			{Key: "_id", Value: primitive.NewObjectID()},
			{Key: "created_at", Value: now},
			{Key: "updated_at", Value: now},
			{Key: "email", Value: request.Email},
			{Key: "user_id", Value: request.UserID},
			{Key: "is_active", Value: true},
		}

		_, err = c.collection.InsertOne(ctx, body)
		if isDup(err) {
			return errors.ErrAlreadyExists
		}

		return err
	}

	if s.IsActive == true {
		return nil
	}

	update := bson.D{
		{
			Key: "$set",
			Value: bson.D{
				{
					Key:   "is_active",
					Value: true,
				},
				{
					Key:   "updated_at",
					Value: now,
				},
			},
		},
	}

	if _, err := c.collection.UpdateOne(ctx, filter, update); err != nil {
		return err
	}

	return nil
}

// Unsubscribe unsubscribes the user from the system notifications.
func (c *Collection) Unsubscribe(ctx context.Context, request *subscription.Subscription) error {
	if request == nil {
		return errors.ErrEmptyStruct
	}

	if request.Email == "" {
		return errors.ErrMissingEmail
	}

	filter := bson.D{
		{
			Key:   "email",
			Value: request.Email,
		},
	}

	s := new(subscription.Subscription)

	now := time.Now().UTC()

	if err := c.collection.FindOne(ctx, filter).Decode(s); err != nil {
		if err != mongo.ErrNoDocuments {
			return err
		}

		body := bson.D{
			{Key: "_id", Value: primitive.NewObjectID()},
			{Key: "created_at", Value: now},
			{Key: "updated_at", Value: now},
			{Key: "email", Value: request.Email},
			{Key: "user_id", Value: request.UserID},
			{Key: "is_active", Value: false},
		}

		_, err = c.collection.InsertOne(ctx, body)
		if isDup(err) {
			return errors.ErrAlreadyExists
		}

		return err
	}

	if s.IsActive == false {
		return nil
	}

	update := bson.D{
		{
			Key: "$set",
			Value: bson.D{
				{
					Key:   "is_active",
					Value: false,
				},
				{
					Key:   "updated_at",
					Value: now,
				},
			},
		},
	}

	if _, err := c.collection.UpdateOne(ctx, filter, update); err != nil {
		return err
	}

	return nil
}

// Subscriptions returns the list of subscriptions by paging and filters.
func (c *Collection) Subscriptions(ctx context.Context, paging *models.Paging, isActive *bool, email *string) ([]*subscription.Subscription, error) {
	opts := options.FindOptions{}

	if paging != nil {
		if paging.Page != nil {
			opts.Skip = paging.Page
		}

		if paging.Limit != nil {
			opts.Limit = paging.Limit
		}

		opts.SetSort(bson.D{{Key: paging.OrderBy, Value: paging.OrderKey}})
	}

	filter := bson.D{}

	if isActive != nil {
		filter = append(filter, bson.E{
			Key:   "is_active",
			Value: *isActive,
		})
	}

	if email != nil {
		filter = append(filter, bson.E{
			Key:   "email",
			Value: *email,
		})
	}

	cursor, err := c.collection.Find(ctx, filter, &opts)
	if err != nil {
		return nil, err
	}

	defer func(c *mongo.Cursor) {
		_ = c.Close(ctx)
	}(cursor)

	if cursor.RemainingBatchLength() == 0 {
		return nil, errors.ErrDoesNotExist
	}

	result := make([]*subscription.Subscription, 0, cursor.RemainingBatchLength())

	err = cursor.All(ctx, &result)

	return result, err
}

// SubscriptionByID returns the subscription by its internal object ID.
func (c *Collection) SubscriptionByID(ctx context.Context, id string) (*subscription.Subscription, error) {
	if id == "" {
		return nil, errors.ErrObjectID
	}

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: objectID,
	}}

	result := new(subscription.Subscription)

	err = c.collection.FindOne(ctx, filter).Decode(result)
	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, errors.ErrDoesNotExist
	default:
		return nil, err
	}
}

// CountSubscriptions returns the number of subscriptions matching the filters.
func (c *Collection) CountSubscriptions(ctx context.Context, isActive *bool, email *string) (int64, error) {
	filter := bson.D{}

	if isActive != nil {
		filter = append(filter, bson.E{
			Key:   "is_active",
			Value: *isActive,
		})
	}

	if email != nil {
		filter = append(filter, bson.E{
			Key:   "email",
			Value: *email,
		})
	}

	return c.collection.CountDocuments(ctx, filter)
}

// isDup checks if the returned error is about the duplicate writing.
func isDup(err error) bool {
	var e mongo.WriteException

	if err == &e {
		for _, we := range e.WriteErrors {
			if we.Code == duplicateCode {
				return true
			}
		}
	}

	return false
}
