package profile

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/profile"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

const (
	duplicateCode = 11000
)

// Collection the profile' collection structure.
type Collection struct {
	collection *mongo.Collection
}

// NewProfileCollection initializes the profile collection.
func NewProfileCollection(collection *mongo.Collection) *Collection {
	return &Collection{collection: collection}
}

// CreateProfile creates the new profile.
func (c *Collection) CreateProfile(ctx context.Context, profile *profile.Profile) (string, error) {
	if profile == nil {
		return "", errors.ErrEmptyStruct
	}

	now := time.Now().UTC()

	body := bson.D{
		{Key: "_id", Value: primitive.NewObjectID()},
		{Key: "created_at", Value: now},
		{Key: "updated_at", Value: now},
		{Key: "first_name", Value: profile.FirstName},
		{Key: "last_name", Value: profile.LastName},
		{Key: "middle_name", Value: profile.MiddleName},
		{Key: "birth_date", Value: profile.BirthDate},
		{Key: "phone_number", Value: profile.PhoneNumber},
		{Key: "email", Value: profile.Email},
		{Key: "password", Value: profile.Password},
		{Key: "document", Value: profile.Document},
		{Key: "is_admin", Value: profile.IsAdmin},
		{Key: "is_active", Value: profile.IsActive},
		{Key: "liked_blogs", Value: []string{}},
	}

	result, err := c.collection.InsertOne(ctx, body)
	if isDup(err) {
		return "", errors.ErrAlreadyExists
	} else if err != nil {
		return "", err
	}

	if res, ok := result.InsertedID.(primitive.ObjectID); ok {
		return res.Hex(), nil
	}

	return "", errors.ErrObjectID
}

// UpdateProfile updates the profile information.
func (c *Collection) UpdateProfile(ctx context.Context, profile *profile.Profile) error {
	if profile == nil {
		return errors.ErrEmptyStruct
	}

	if profile.ID == "" {
		return errors.ErrObjectID
	}

	id, err := primitive.ObjectIDFromHex(profile.ID)
	if err != nil {
		return errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: id,
	}}

	body := bson.D{
		{
			Key: "$set",
			Value: bson.D{
				{Key: "updated_at", Value: time.Now().UTC()},
				{Key: "first_name", Value: profile.FirstName},
				{Key: "last_name", Value: profile.LastName},
				{Key: "middle_name", Value: profile.MiddleName},
				{Key: "birth_date", Value: profile.BirthDate},
				{Key: "phone_number", Value: profile.PhoneNumber},
				{Key: "email", Value: profile.Email},
				{Key: "password", Value: profile.Password},
				{Key: "document", Value: profile.Document},
				{Key: "is_admin", Value: profile.IsAdmin},
				{Key: "is_active", Value: profile.IsActive},
			},
		},
	}

	result, err := c.collection.UpdateOne(ctx, filter, body)
	if err != nil {
		return err
	}

	if result.ModifiedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// DeleteProfile deletes the profile by their internal id.
func (c *Collection) DeleteProfile(ctx context.Context, id string) error {
	if id == "" {
		return errors.ErrObjectID
	}

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: objectID,
	}}

	result, err := c.collection.DeleteOne(ctx, filter)
	if err != nil {
		return err
	}

	if result.DeletedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// ProfileByID returns the profile by their internal ObjectID.
func (c *Collection) ProfileByID(ctx context.Context, id string) (*profile.Profile, error) {
	if id == "" {
		return nil, errors.ErrObjectID
	}

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: objectID,
	}}

	result := new(profile.Profile)

	err = c.collection.FindOne(ctx, filter).Decode(&result)
	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, errors.ErrDoesNotExist
	default:
		return nil, err
	}
}

// Profiles returns the list of profile based on the provided filters.
func (c *Collection) Profiles(ctx context.Context, filter *profile.Filter, paging *models.Paging) ([]*profile.Profile, error) {
	opts := options.FindOptions{}

	if paging != nil {
		if paging.Page != nil {
			opts.Skip = paging.Page
		}

		if paging.Limit != nil {
			opts.Limit = paging.Limit
		}

		opts.SetSort(bson.D{{Key: paging.OrderBy, Value: paging.OrderKey}})
	}

	cursor, err := c.collection.Find(ctx, buildFilters(filter), &opts)
	if err != nil {
		return nil, err
	}

	defer func(c *mongo.Cursor) {
		_ = c.Close(ctx)
	}(cursor)

	if cursor.RemainingBatchLength() == 0 {
		return nil, errors.ErrDoesNotExist
	}

	result := make([]*profile.Profile, 0, cursor.RemainingBatchLength())

	err = cursor.All(ctx, &result)

	return result, err
}

// ResetPassword resets the password of the user based on the provided credentials.
func (c *Collection) ResetPassword(ctx context.Context, email, password string) error {
	if email == "" || password == "" {
		return errors.ErrBadCredentials
	}

	filter := bson.D{{
		Key:   "email",
		Value: email,
	}}

	update := bson.D{{
		Key: "$set",
		Value: bson.D{
			{
				Key:   "password",
				Value: password,
			},
			{
				Key:   "updated_at",
				Value: time.Now().UTC(),
			},
		},
	}}

	res, err := c.collection.UpdateOne(context.Background(), filter, update)
	switch err {
	case nil:
	case mongo.ErrNoDocuments:
		return errors.ErrDoesNotExist
	default:
		return err
	}

	if res.ModifiedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// Count counts the needed profiles by the provided filters.
func (c *Collection) Count(ctx context.Context, filter *profile.Filter) (int64, error) {
	return c.collection.CountDocuments(ctx, buildFilters(filter))
}

// LikeBlog likes the blog by the given ID for the user.
func (c *Collection) LikeBlog(ctx context.Context, userID, id string) error {
	if id == "" {
		return errors.ErrObjectID
	}

	oid, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return errors.ErrObjectID
	}

	filter := bson.D{
		{
			Key:   "_id",
			Value: oid,
		},
	}

	body := bson.D{
		{
			Key:   "$addToSet",
			Value: bson.M{"liked_blogs": id},
		},
	}

	if _, err = c.collection.UpdateOne(ctx, filter, body); err != nil {
		return err
	}

	return nil
}

// UnLikeBlog unlikes the blog by the given ID for the user.
func (c *Collection) UnLikeBlog(ctx context.Context, userID, id string) error {
	if id == "" {
		return errors.ErrObjectID
	}

	oid, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return errors.ErrObjectID
	}

	filter := bson.D{
		{
			Key:   "_id",
			Value: oid,
		},
	}

	body := bson.D{
		{
			Key:   "$pull",
			Value: bson.M{"liked_blogs": bson.M{"$in": []string{id}}},
		},
	}

	if _, err = c.collection.UpdateOne(ctx, filter, body); err != nil {
		return err
	}

	return nil
}

// LikedBlogs returns the list of liked blogs for the user by their internal ID.
func (c *Collection) LikedBlogs(ctx context.Context, userID string) ([]string, error) {
	if userID == "" {
		return nil, errors.ErrObjectID
	}

	oid, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return nil, errors.ErrObjectID
	}

	filter := bson.D{
		{
			Key:   "_id",
			Value: oid,
		},
	}

	filter = append(filter, bson.E{
		Key:   "liked_blogs",
		Value: bson.M{"$exists": true},
	})

	opts := new(options.FindOptions)

	opts.SetProjection(bson.M{"liked_blogs": 1})

	cursor, err := c.collection.Find(ctx, filter, opts)
	if err != nil {
		return nil, err
	}

	defer func(c *mongo.Cursor) {
		_ = c.Close(ctx)
	}(cursor)

	if cursor.RemainingBatchLength() == 0 {
		return []string{}, nil
	}

	var lb []struct {
		LikedBlogs []string `bson:"liked_blogs"`
	}

	if err = cursor.All(ctx, &lb); err != nil {
		return nil, err
	}

	if len(lb) == 0 {
		return nil, errors.ErrDoesNotExist
	}

	return lb[0].LikedBlogs, nil
}

// PopularBlogs returns the number of popular blogs' ObjectIDs for analytics.
func (c *Collection) PopularBlogs(ctx context.Context) (map[string]float64, error) {
	pipeline := bson.A{
		bson.M{
			"$unwind": "$liked_blogs",
		},
		bson.M{
			"$group": bson.M{
				"_id": "$liked_blogs",
				"count": bson.M{
					"$sum": 1,
				},
			},
		},
		bson.M{
			"$sort": bson.M{
				"count": -1,
			},
		},
	}

	cursor, err := c.collection.Aggregate(ctx, pipeline)
	if err != nil {
		return nil, err
	}

	var res []struct {
		ID    string  `bson:"_id"`
		Count float64 `bson:"count"`
	}

	if err = cursor.All(ctx, &res); err != nil {
		return nil, err
	}

	result := make(map[string]float64, len(res))

	for _, item := range res {
		if item.ID == "" {
			continue
		}

		result[item.ID] = item.Count
	}

	return result, nil
}

// buildFilters builds the filters to search the needed profile.
func buildFilters(filter *profile.Filter) bson.D {
	result := bson.D{}

	if filter.FirstName != nil {
		result = append(result, bson.E{
			Key:   "first_name",
			Value: *filter.FirstName,
		})
	}

	if filter.LastName != nil {
		result = append(result, bson.E{
			Key:   "last_name",
			Value: *filter.LastName,
		})
	}

	if filter.MiddleName != nil {
		result = append(result, bson.E{
			Key:   "middle_name",
			Value: *filter.MiddleName,
		})
	}

	if filter.PhoneNumber != nil {
		result = append(result, bson.E{
			Key:   "phone_number",
			Value: *filter.PhoneNumber,
		})
	}

	if filter.Email != nil {
		result = append(result, bson.E{
			Key:   "email",
			Value: *filter.Email,
		})
	}

	if filter.IsActive != nil {
		result = append(result, bson.E{
			Key:   "is_active",
			Value: *filter.IsActive,
		})
	}

	return result
}

// isDup checks if the returned error is about the duplicate writing.
func isDup(err error) bool {
	var e mongo.WriteException

	if err == &e {
		for _, we := range e.WriteErrors {
			if we.Code == duplicateCode {
				return true
			}
		}
	}

	return false
}
