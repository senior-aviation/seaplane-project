package port

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/port"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

const (
	duplicateCode = 11000
	fault         = 0.01
)

// Collection the port's collection structure.
type Collection struct {
	collection *mongo.Collection
}

// NewPortCollection initializes the port collection.
func NewPortCollection(collection *mongo.Collection) *Collection {
	return &Collection{collection: collection}
}

// CreatePort creates the new port.
func (c *Collection) CreatePort(ctx context.Context, port *port.Port) (string, error) {
	if port == nil {
		return "", errors.ErrEmptyStruct
	}

	now := time.Now().UTC()

	body := bson.D{
		{Key: "_id", Value: primitive.NewObjectID()},
		{Key: "created_at", Value: now},
		{Key: "updated_at", Value: now},
		{Key: "name", Value: port.Name},
		{Key: "latitude", Value: port.Latitude},
		{Key: "longitude", Value: port.Longitude},
	}

	result, err := c.collection.InsertOne(ctx, body)
	if isDup(err) {
		return "", errors.ErrAlreadyExists
	}

	if res, ok := result.InsertedID.(primitive.ObjectID); ok {
		return res.Hex(), nil
	}

	return "", errors.ErrObjectID
}

// UpdatePort updates the port information.
func (c *Collection) UpdatePort(ctx context.Context, port *port.Port) error {
	if port == nil {
		return errors.ErrEmptyStruct
	}

	if port.ID == "" {
		return errors.ErrObjectID
	}

	id, err := primitive.ObjectIDFromHex(port.ID)
	if err != nil {
		return errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: id,
	}}

	body := bson.D{
		{
			Key: "$set",
			Value: bson.D{
				{Key: "updated_at", Value: time.Now().UTC()},
				{Key: "name", Value: port.Name},
				{Key: "latitude", Value: port.Latitude},
				{Key: "longitude", Value: port.Longitude},
			},
		},
	}

	result, err := c.collection.UpdateOne(ctx, filter, body)
	if err != nil {
		return err
	}

	if result.ModifiedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// DeletePort deletes the port by their internal id.
func (c *Collection) DeletePort(ctx context.Context, id string) error {
	if id == "" {
		return errors.ErrObjectID
	}

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: objectID,
	}}

	result, err := c.collection.DeleteOne(ctx, filter)
	if err != nil {
		return err
	}

	if result.DeletedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// PortByID returns the port by their internal ObjectID.
func (c *Collection) PortByID(ctx context.Context, id string) (*port.Port, error) {
	if id == "" {
		return nil, errors.ErrObjectID
	}

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: objectID,
	}}

	result := new(port.Port)

	err = c.collection.FindOne(ctx, filter).Decode(&result)
	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, errors.ErrDoesNotExist
	default:
		return nil, err
	}
}

// Ports returns the list of ports.
func (c *Collection) Ports(ctx context.Context, paging *models.Paging) ([]*port.Port, error) {
	opts := options.FindOptions{}

	if paging != nil {
		if paging.Page != nil {
			opts.Skip = paging.Page
		}

		if paging.Limit != nil {
			opts.Limit = paging.Limit
		}

		opts.SetSort(bson.D{{Key: paging.OrderBy, Value: paging.OrderKey}})
	}

	cursor, err := c.collection.Find(ctx, bson.D{}, &opts)
	if err != nil {
		return nil, err
	}

	defer func(c *mongo.Cursor) {
		_ = c.Close(ctx)
	}(cursor)

	if cursor.RemainingBatchLength() == 0 {
		return nil, errors.ErrDoesNotExist
	}

	result := make([]*port.Port, 0, cursor.RemainingBatchLength())

	err = cursor.All(ctx, &result)

	return result, err
}

// PortByCoordinates returns the port object by the provided coordinates.
func (c *Collection) PortByCoordinates(ctx context.Context, lat, lng float64) (*port.Port, error) {
	filter := bson.D{
		{
			Key: "latitude",
			Value: bson.M{
				"$gte": lat - fault,
				"$lte": lat + fault,
			},
		},
		{
			Key: "longitude",
			Value: bson.M{
				"$gte": lng - fault,
				"$lte": lng + fault,
			},
		},
	}

	p := new(port.Port)

	err := c.collection.FindOne(ctx, filter).Decode(p)
	switch err {
	case nil:
		return p, nil
	case mongo.ErrNoDocuments:
		return nil, errors.ErrDoesNotExist
	default:
		return nil, err
	}
}

// Count counts the needed ports by the provided filters.
func (c *Collection) Count(ctx context.Context) (int64, error) {
	return c.collection.CountDocuments(ctx, bson.D{})
}

// isDup checks if the returned error is about the duplicate writing.
func isDup(err error) bool {
	var e mongo.WriteException

	if err == &e {
		for _, we := range e.WriteErrors {
			if we.Code == duplicateCode {
				return true
			}
		}
	}

	return false
}
