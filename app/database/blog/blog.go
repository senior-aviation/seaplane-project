package blog

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/blog"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

const (
	duplicateCode = 11000
)

// Collection the blog's collection structure.
type Collection struct {
	collection *mongo.Collection
}

// NewBlogCollection initializes the blog collection.
func NewBlogCollection(collection *mongo.Collection) *Collection {
	return &Collection{collection: collection}
}

// CreateBlog creates the new blog.
func (c *Collection) CreateBlog(ctx context.Context, blog *blog.Blog) (string, error) {
	if blog == nil {
		return "", errors.ErrEmptyStruct
	}

	now := time.Now().UTC()

	body := bson.D{
		{Key: "_id", Value: primitive.NewObjectID()},
		{Key: "created_at", Value: now},
		{Key: "updated_at", Value: now},
		{Key: "title", Value: blog.Title},
		{Key: "content", Value: blog.Content},
		{Key: "image", Value: blog.Image},
		{Key: "is_active", Value: blog.IsActive},
	}

	result, err := c.collection.InsertOne(ctx, body)
	if isDup(err) {
		return "", errors.ErrAlreadyExists
	}

	if err != nil {
		return "", err
	}

	if res, ok := result.InsertedID.(primitive.ObjectID); ok {
		return res.Hex(), nil
	}

	return "", errors.ErrObjectID
}

// UpdateBlog updates the blog information.
func (c *Collection) UpdateBlog(ctx context.Context, blog *blog.Blog) error {
	if blog == nil {
		return errors.ErrEmptyStruct
	}

	if blog.ID == "" {
		return errors.ErrObjectID
	}

	id, err := primitive.ObjectIDFromHex(blog.ID)
	if err != nil {
		return errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: id,
	}}

	body := bson.D{{
		Key: "$set",
		Value: bson.D{
			{Key: "updated_at", Value: time.Now().UTC()},
			{Key: "title", Value: blog.Title},
			{Key: "content", Value: blog.Content},
			{Key: "image", Value: blog.Image},
			{Key: "is_active", Value: blog.IsActive},
		},
	}}

	result, err := c.collection.UpdateOne(ctx, filter, body)
	if err != nil {
		return err
	}

	if result.ModifiedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// DeleteBlog deletes the blog by their internal id.
func (c *Collection) DeleteBlog(ctx context.Context, id string) error {
	if id == "" {
		return errors.ErrObjectID
	}

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: objectID,
	}}

	result, err := c.collection.DeleteOne(ctx, filter)
	if err != nil {
		return err
	}

	if result.DeletedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// BlogByID returns the blog by their internal ObjectID.
func (c *Collection) BlogByID(ctx context.Context, id string) (*blog.Blog, error) {
	if id == "" {
		return nil, errors.ErrObjectID
	}

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: objectID,
	}}

	result := new(blog.Blog)

	err = c.collection.FindOne(ctx, filter).Decode(result)
	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, errors.ErrDoesNotExist
	default:
		return nil, err
	}
}

// BlogsByIDs returns the blogs by the list of their internal ObjectIDs.
func (c *Collection) BlogsByIDs(ctx context.Context, ids []string, paging *models.Paging, isActive *bool) ([]*blog.Blog, error) {
	opts := options.FindOptions{}

	if paging != nil {
		if paging.Page != nil {
			opts.Skip = paging.Page
		}

		if paging.Limit != nil {
			opts.Limit = paging.Limit
		}

		opts.SetSort(bson.D{{Key: paging.OrderBy, Value: paging.OrderKey}})
	}

	cursor, err := c.collection.Find(ctx, filtersForIDsRequest(ids, isActive), &opts)
	if err != nil {
		return nil, err
	}

	defer func(c *mongo.Cursor) {
		_ = c.Close(ctx)
	}(cursor)

	if cursor.RemainingBatchLength() == 0 {
		return []*blog.Blog{}, nil
	}

	result := make([]*blog.Blog, 0, cursor.RemainingBatchLength())

	err = cursor.All(ctx, &result)

	return result, err
}

// Blogs returns the list of blogs.
func (c *Collection) Blogs(ctx context.Context, paging *models.Paging, isActive *bool) ([]*blog.Blog, error) {
	opts := options.FindOptions{}

	if paging != nil {
		if paging.Page != nil {
			opts.Skip = paging.Page
		}

		if paging.Limit != nil {
			opts.Limit = paging.Limit
		}

		opts.SetSort(bson.D{{Key: paging.OrderBy, Value: paging.OrderKey}})
	}

	filter := bson.D{}

	if isActive != nil {
		filter = append(filter, bson.E{
			Key:   "is_active",
			Value: *isActive,
		})
	}

	cursor, err := c.collection.Find(ctx, filter, &opts)
	if err != nil {
		return nil, err
	}

	defer func(c *mongo.Cursor) {
		_ = c.Close(ctx)
	}(cursor)

	if cursor.RemainingBatchLength() == 0 {
		return nil, errors.ErrDoesNotExist
	}

	result := make([]*blog.Blog, 0, cursor.RemainingBatchLength())

	err = cursor.All(ctx, &result)

	return result, err
}

// Count returns the number of documents by the provided filters.
func (c *Collection) Count(ctx context.Context) (int64, error) {
	return c.collection.CountDocuments(ctx, bson.D{})
}

// BlogsByIDsCount counts the number of blogs requested by the list of ObjectIDs.
func (c *Collection) BlogsByIDsCount(ctx context.Context, ids []string, isActive *bool) (int64, error) {
	return c.collection.CountDocuments(ctx, filtersForIDsRequest(ids, isActive))
}

// filtersForIDsRequest builds filters for fetching the list of blogs by the lift of their internal ObjectIDs.
func filtersForIDsRequest(ids []string, isActive *bool) bson.D {
	oids := make([]primitive.ObjectID, 0, len(ids))

	for _, id := range ids {
		if oid, err := primitive.ObjectIDFromHex(id); err == nil {
			oids = append(oids, oid)
		}
	}

	result := bson.D{
		bson.E{
			Key:   "_id",
			Value: bson.M{"$in": oids},
		},
	}

	if isActive != nil {
		result = append(result, bson.E{
			Key:   "is_active",
			Value: *isActive,
		})
	}

	return result
}

// isDup checks if the returned error is about the duplicate writing.
func isDup(err error) bool {
	var e mongo.WriteException

	if err == &e {
		for _, we := range e.WriteErrors {
			if we.Code == duplicateCode {
				return true
			}
		}
	}

	return false
}
