package database

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/config"
	"gitlab.com/senior-aviation/seaplane/app/database/blog"
	"gitlab.com/senior-aviation/seaplane/app/database/message"
	"gitlab.com/senior-aviation/seaplane/app/database/order"
	"gitlab.com/senior-aviation/seaplane/app/database/port"
	"gitlab.com/senior-aviation/seaplane/app/database/price"
	"gitlab.com/senior-aviation/seaplane/app/database/profile"
	"gitlab.com/senior-aviation/seaplane/app/database/promocode"
	"gitlab.com/senior-aviation/seaplane/app/database/subscription"
	customErrors "gitlab.com/senior-aviation/seaplane/app/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"time"
)

const (
	connectionTimeout = time.Second * 3
)

type DataStore interface {
	Connect() error
	Ping() error
	Close(ctx context.Context) error

	Profile() Profile
	Order() Order
	Price() Price
	Port() Port
	Blog() Blog
	Subscription() Subscription
	PromoCode() PromoCode
	Message() Message
}

type Mongo struct {
	url               string
	name              string
	client            *mongo.Client
	DataBase          *mongo.Database
	connectionTimeout time.Duration

	profileCollection      *profile.Collection
	orderCollection        *order.Collection
	priceCollection        *price.Collection
	portCollection         *port.Collection
	blogCollection         *blog.Collection
	subscriptionCollection *subscription.Collection
	promoCodeCollection    *promocode.Collection
	messageCollection      *message.Collection
}

// NewDataBase creates a new database object based on the configuration settings.
func NewDataBase(configuration *config.Configuration) (DataStore, error) {
	if configuration.DatabaseURL == "" {
		return nil, customErrors.ErrInvalidConfig
	}

	return &Mongo{
		url:               configuration.DatabaseURL,
		name:              configuration.DatabaseName,
		connectionTimeout: connectionTimeout,
	}, nil
}

// Connect establishes the connection with the database.
func (m *Mongo) Connect() error {
	ctx, cancel := context.WithTimeout(context.Background(), connectionTimeout)

	defer cancel()

	var err error

	m.client, err = mongo.Connect(ctx, options.Client().ApplyURI(m.url))
	if err != nil {
		log.Printf("[ERROR] connection to Mongo: %s", err)
		return err
	}

	if err = m.Ping(); err != nil {
		log.Printf("[ERROR] ping the database: %s", err)
		return err
	}

	m.DataBase = m.client.Database(m.name)

	return m.constructIndexes()
}

// Ping pings the database connection.
func (m *Mongo) Ping() error {
	ctx, cancel := context.WithTimeout(context.Background(), connectionTimeout)
	defer cancel()

	return m.client.Ping(ctx, readpref.Primary())
}

// Close closes (disconnects) the database connection.
func (m *Mongo) Close(ctx context.Context) error {
	return m.client.Disconnect(ctx)
}

// constructIndexes build the database indexation.
func (m *Mongo) constructIndexes() error {
	ctx, cancel := context.WithTimeout(context.Background(), connectionTimeout)
	defer cancel()

	if err := m.profileIndexes(ctx); err != nil {
		return err
	}

	if err := m.orderIndexes(ctx); err != nil {
		return err
	}

	if err := m.blogIndexes(ctx); err != nil {
		return err
	}

	if err := m.subscriptionIndexes(ctx); err != nil {
		return err
	}

	if err := m.promoCodeIndexes(ctx); err != nil {
		return err
	}

	if err := m.messageIndexes(ctx); err != nil {
		return err
	}

	return nil
}

// profileIndexes creates the indexing for the profile collection.
func (m *Mongo) profileIndexes(ctx context.Context) error {
	indexes := []mongo.IndexModel{
		{
			Keys:    bson.D{{Key: "phone_number", Value: 1}},
			Options: options.Index().SetName("phone_number").SetUnique(true),
		},
		{
			Keys:    bson.D{{Key: "email", Value: 1}},
			Options: options.Index().SetName("email").SetUnique(true),
		},
	}

	if _, err := m.DataBase.Collection(profileCollection).Indexes().CreateMany(ctx, indexes); err != nil {
		log.Printf("[ERROR] profile indexes creation: %s", err)
		return err
	}

	return nil
}

// orderIndexes creates the indexing for the order collection.
func (m *Mongo) orderIndexes(ctx context.Context) error {
	indexes := []mongo.IndexModel{
		{
			Keys:    bson.D{{Key: "created_at", Value: 1}},
			Options: options.Index().SetName("created_at"),
		},
	}

	if _, err := m.DataBase.Collection(orderCollection).Indexes().CreateMany(ctx, indexes); err != nil {
		log.Printf("[ERROR] order indexes creation: %s", err)
		return err
	}

	return nil
}

// blogIndexes creates the indexing for the blog collection.
func (m *Mongo) blogIndexes(ctx context.Context) error {
	indexes := []mongo.IndexModel{
		{
			Keys:    bson.D{{Key: "title", Value: 1}},
			Options: options.Index().SetName("title").SetUnique(true),
		},
	}

	if _, err := m.DataBase.Collection(blogCollection).Indexes().CreateMany(ctx, indexes); err != nil {
		log.Printf("[ERROR] blog indexes creation: %s", err)
		return err
	}

	return nil
}

// subscriptionIndexes creates the indexing for the subscription collection.
func (m *Mongo) subscriptionIndexes(ctx context.Context) error {
	indexes := []mongo.IndexModel{
		{
			Keys:    bson.D{{Key: "email", Value: 1}, {Key: "user_id", Value: 1}},
			Options: options.Index().SetName("user").SetUnique(true),
		},
	}

	if _, err := m.DataBase.Collection(subscriptionCollection).Indexes().CreateMany(ctx, indexes); err != nil {
		log.Printf("[ERROR] subscription indexes creation: %s", err)
		return err
	}

	return nil
}

// promoCodeIndexes creates the indexing for the promocode collection.
func (m *Mongo) promoCodeIndexes(ctx context.Context) error {
	indexes := []mongo.IndexModel{
		{
			Keys:    bson.D{{Key: "name", Value: 1}},
			Options: options.Index().SetName("name").SetUnique(true),
		},
	}

	if _, err := m.DataBase.Collection(promoCodeCollection).Indexes().CreateMany(ctx, indexes); err != nil {
		log.Printf("[ERROR] promocode indexes creation: %s", err)
		return err
	}

	return nil
}

// messageIndexes creates the indexing for the message collection.
func (m *Mongo) messageIndexes(ctx context.Context) error {
	indexes := []mongo.IndexModel{
		{
			Keys:    bson.D{{Key: "created_at", Value: 1}},
			Options: options.Index().SetName("created_at"),
		},
		{
			Keys:    bson.D{{Key: "email", Value: 1}},
			Options: options.Index().SetName("email"),
		},
	}

	if _, err := m.DataBase.Collection(messageCollection).Indexes().CreateMany(ctx, indexes); err != nil {
		log.Printf("[ERROR] message indexes creation: %s", err)
		return err
	}

	return nil
}

// Profile creates the profile collection in the database.
func (m *Mongo) Profile() Profile {
	if m.profileCollection == nil {
		m.profileCollection = profile.NewProfileCollection(m.DataBase.Collection(profileCollection))
	}

	return m.profileCollection
}

// Order creates the order collection in the database.
func (m *Mongo) Order() Order {
	if m.orderCollection == nil {
		m.orderCollection = order.NewProfileCollection(m.DataBase.Collection(orderCollection))
	}

	return m.orderCollection
}

// Price creates the price collection in the database.
func (m *Mongo) Price() Price {
	if m.priceCollection == nil {
		m.priceCollection = price.NewPriceCollection(m.DataBase.Collection(priceCollection))
	}

	return m.priceCollection
}

// Port creates the port collection in the database.
func (m *Mongo) Port() Port {
	if m.portCollection == nil {
		m.portCollection = port.NewPortCollection(m.DataBase.Collection(portCollection))
	}

	return m.portCollection
}

// Blog creates the blog collection in the database.
func (m *Mongo) Blog() Blog {
	if m.blogCollection == nil {
		m.blogCollection = blog.NewBlogCollection(m.DataBase.Collection(blogCollection))
	}

	return m.blogCollection
}

// Subscription creates the subscriptions collection in the database.
func (m *Mongo) Subscription() Subscription {
	if m.subscriptionCollection == nil {
		m.subscriptionCollection = subscription.NewSubscriptionCollection(m.DataBase.Collection(subscriptionCollection))
	}

	return m.subscriptionCollection
}

// PromoCode creates the promocode collection in the database.
func (m *Mongo) PromoCode() PromoCode {
	if m.promoCodeCollection == nil {
		m.promoCodeCollection = promocode.NewPromoCodeCollection(m.DataBase.Collection(promoCodeCollection))
	}

	return m.promoCodeCollection
}

// Message creates the message collection in the database.
func (m *Mongo) Message() Message {
	if m.messageCollection == nil {
		m.messageCollection = message.NewMessageCollection(m.DataBase.Collection(messageCollection))
	}

	return m.messageCollection
}
