package database

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/blog"
	"gitlab.com/senior-aviation/seaplane/app/models/message"
	"gitlab.com/senior-aviation/seaplane/app/models/order"
	"gitlab.com/senior-aviation/seaplane/app/models/port"
	"gitlab.com/senior-aviation/seaplane/app/models/price"
	"gitlab.com/senior-aviation/seaplane/app/models/profile"
	"gitlab.com/senior-aviation/seaplane/app/models/promocode"
	"gitlab.com/senior-aviation/seaplane/app/models/subscription"
)

const (
	profileCollection      = "profile"
	orderCollection        = "order"
	priceCollection        = "price"
	portCollection         = "port"
	blogCollection         = "blog"
	subscriptionCollection = "subscription"
	promoCodeCollection    = "promocode"
	messageCollection      = "message"
)

type Profile interface {
	CreateProfile(ctx context.Context, profile *profile.Profile) (string, error)
	UpdateProfile(ctx context.Context, profile *profile.Profile) error
	DeleteProfile(ctx context.Context, id string) error
	ProfileByID(ctx context.Context, id string) (*profile.Profile, error)
	Profiles(ctx context.Context, filter *profile.Filter, paging *models.Paging) ([]*profile.Profile, error)
	ResetPassword(ctx context.Context, email, password string) error
	Count(ctx context.Context, filter *profile.Filter) (int64, error)
	LikeBlog(ctx context.Context, userID, id string) error
	UnLikeBlog(ctx context.Context, userID, id string) error
	LikedBlogs(ctx context.Context, userID string) ([]string, error)
	PopularBlogs(ctx context.Context) (map[string]float64, error)
}

type Order interface {
	CreateOrder(ctx context.Context, order *order.Order) (int, error)
	UpdateOrder(ctx context.Context, order *order.Order) error
	OrderByID(ctx context.Context, id int) (*order.Order, error)
	Orders(ctx context.Context, filter *order.Filter, paging *models.Paging) ([]*order.Order, error)
	SharingOrders(ctx context.Context, filters *order.SharingOrderFilters, paging *models.Paging) ([]*order.SharingOrderInfo, error)
	SharingOrdersCount(ctx context.Context, filters *order.SharingOrderFilters) (int64, error)
	SharingOrderInfo(ctx context.Context, id int) (*order.SharingOrderInfo, error)
	Count(ctx context.Context, filter *order.Filter) (int64, error)
	JoinShareable(ctx context.Context, passengers []*order.Passenger, orderID int, userIDs []string) error
	Earnings(ctx context.Context, request *models.AnalyticsRequest) (float64, error)
	PopularDestinations(ctx context.Context, request *models.AnalyticsRequest) (map[*order.Location]float64, error)
}

type Price interface {
	CreatePrice(ctx context.Context, price string) error
	UpdatePrice(ctx context.Context, price string) error
	Price(ctx context.Context) (*price.Price, error)
}

type Port interface {
	CreatePort(ctx context.Context, port *port.Port) (string, error)
	UpdatePort(ctx context.Context, port *port.Port) error
	DeletePort(ctx context.Context, id string) error
	PortByID(ctx context.Context, id string) (*port.Port, error)
	Ports(ctx context.Context, paging *models.Paging) ([]*port.Port, error)
	PortByCoordinates(ctx context.Context, lat, lng float64) (*port.Port, error)
	Count(ctx context.Context) (int64, error)
}

type Blog interface {
	CreateBlog(ctx context.Context, blog *blog.Blog) (string, error)
	UpdateBlog(ctx context.Context, blog *blog.Blog) error
	DeleteBlog(ctx context.Context, id string) error
	BlogByID(ctx context.Context, id string) (*blog.Blog, error)
	BlogsByIDs(ctx context.Context, ids []string, paging *models.Paging, isActive *bool) ([]*blog.Blog, error)
	Blogs(ctx context.Context, paging *models.Paging, isActive *bool) ([]*blog.Blog, error)
	Count(ctx context.Context) (int64, error)
	BlogsByIDsCount(ctx context.Context, ids []string, isActive *bool) (int64, error)
}

type Subscription interface {
	Subscribe(ctx context.Context, request *subscription.Subscription) error
	Unsubscribe(ctx context.Context, request *subscription.Subscription) error
	Subscriptions(ctx context.Context, paging *models.Paging, isActive *bool, email *string) ([]*subscription.Subscription, error)
	SubscriptionByID(ctx context.Context, id string) (*subscription.Subscription, error)
	CountSubscriptions(ctx context.Context, isActive *bool, email *string) (int64, error)
}

type PromoCode interface {
	CreatePromoCode(ctx context.Context, promoCode *promocode.PromoCode) (string, error)
	UpdatePromoCode(ctx context.Context, promoCode *promocode.PromoCode) error
	DeletePromoCode(ctx context.Context, id string) error
	PromoCodeByID(ctx context.Context, id string) (*promocode.PromoCode, error)
	PromoCodes(ctx context.Context, paging *models.Paging, name *string, isActive *bool) ([]*promocode.PromoCode, error)
	Count(ctx context.Context, name *string, isActive *bool) (int64, error)
	DecreaseLimit(ctx context.Context, name string) error
}

type Message interface {
	CreateMessage(ctx context.Context, message *message.Message) (string, error)
	Messages(ctx context.Context, paging *models.Paging, email *string, isProcessed *bool) ([]*message.Message, error)
	MessageByID(ctx context.Context, id string) (*message.Message, error)
	ProcessMessage(ctx context.Context, id string, isProcessed bool) error
	Count(ctx context.Context, email *string, isProcessed *bool) (int64, error)
}
