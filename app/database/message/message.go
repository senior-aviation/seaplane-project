package message

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/message"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

const (
	duplicateCode = 11000
)

// Collection the message's collection structure.
type Collection struct {
	collection *mongo.Collection
}

// NewMessageCollection initializes the message collection.
func NewMessageCollection(collection *mongo.Collection) *Collection {
	return &Collection{collection: collection}
}

// CreateMessage creates the new message.
func (c *Collection) CreateMessage(ctx context.Context, message *message.Message) (string, error) {
	if message == nil {
		return "", errors.ErrEmptyStruct
	}

	now := time.Now().UTC()

	body := bson.D{
		{Key: "_id", Value: primitive.NewObjectID()},
		{Key: "created_at", Value: now},
		{Key: "name", Value: message.Name},
		{Key: "email", Value: message.Email},
		{Key: "message", Value: message.Message},
		{Key: "is_processed", Value: false},
	}

	result, err := c.collection.InsertOne(ctx, body)
	if isDup(err) {
		return "", errors.ErrAlreadyExists
	}

	if err != nil {
		return "", err
	}

	if res, ok := result.InsertedID.(primitive.ObjectID); ok {
		return res.Hex(), nil
	}

	return "", errors.ErrObjectID
}

// Messages returns the list of messages.
func (c *Collection) Messages(ctx context.Context, paging *models.Paging, email *string, isProcessed *bool) ([]*message.Message, error) {
	opts := options.FindOptions{}

	if paging != nil {
		if paging.Page != nil {
			opts.Skip = paging.Page
		}

		if paging.Limit != nil {
			opts.Limit = paging.Limit
		}

		opts.SetSort(bson.D{{Key: paging.OrderBy, Value: paging.OrderKey}})
	}

	filter := buildMessageFilters(email, isProcessed)

	cursor, err := c.collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	defer func(c *mongo.Cursor) {
		_ = c.Close(ctx)
	}(cursor)

	if cursor.RemainingBatchLength() == 0 {
		return nil, errors.ErrDoesNotExist
	}

	result := make([]*message.Message, 0, cursor.RemainingBatchLength())

	err = cursor.All(ctx, &result)

	return result, err
}

// MessageByID returns the message by their internal ObjectID.
func (c *Collection) MessageByID(ctx context.Context, id string) (*message.Message, error) {
	if id == "" {
		return nil, errors.ErrObjectID
	}

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: objectID,
	}}

	result := new(message.Message)

	err = c.collection.FindOne(ctx, filter).Decode(result)
	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, errors.ErrDoesNotExist
	default:
		return nil, err
	}
}

// ProcessMessage processes the message.
func (c *Collection) ProcessMessage(ctx context.Context, id string, isProcessed bool) error {
	if id == "" {
		return errors.ErrObjectID
	}

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: objectID,
	}}

	set := bson.D{
		{
			Key: "$set",
			Value: bson.D{
				{
					Key:   "is_processed",
					Value: isProcessed,
				},
			},
		},
	}

	res, err := c.collection.UpdateOne(ctx, filter, set)
	if err != nil {
		return err
	}

	if res.ModifiedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// Count returns the number of documents by the provided filters.
func (c *Collection) Count(ctx context.Context, email *string, isProcessed *bool) (int64, error) {
	filter := buildMessageFilters(email, isProcessed)

	return c.collection.CountDocuments(ctx, filter)
}

// buildMessageFilters builds the filters to search the messages.
func buildMessageFilters(email *string, isProcessed *bool) bson.D {
	filter := bson.D{}

	if email != nil {
		filter = append(filter, bson.E{
			Key:   "email",
			Value: *email,
		})
	}

	if isProcessed != nil {
		filter = append(filter, bson.E{
			Key:   "is_processed",
			Value: *isProcessed,
		})
	}

	return filter
}

// isDup checks if the returned error is about the duplicate writing.
func isDup(err error) bool {
	var e mongo.WriteException

	if err == &e {
		for _, we := range e.WriteErrors {
			if we.Code == duplicateCode {
				return true
			}
		}
	}

	return false
}
