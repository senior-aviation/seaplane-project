package price

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	priceModels "gitlab.com/senior-aviation/seaplane/app/models/price"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

// Collection the price's collection structure.
type Collection struct {
	collection *mongo.Collection
}

// NewPriceCollection initializes the price collection.
func NewPriceCollection(collection *mongo.Collection) *Collection {
	return &Collection{collection: collection}
}

// CreatePrice creates a new price.
func (c *Collection) CreatePrice(ctx context.Context, price string) error {
	if price == "" {
		return errors.ErrInconvertiblePrice
	}

	res, err := c.collection.Find(ctx, bson.D{})
	if err != nil {
		return err
	}

	if res.RemainingBatchLength() != 0 {
		return errors.ErrAlreadyExists
	}

	now := time.Now().UTC()

	body := bson.D{
		{
			Key:   "_id",
			Value: primitive.NewObjectID(),
		},
		{
			Key:   "created_at",
			Value: now,
		},
		{
			Key:   "updated_at",
			Value: now,
		},
		{
			Key:   "price_value",
			Value: price,
		},
	}

	if _, err = c.collection.InsertOne(ctx, body); err != nil {
		return err
	}

	return nil
}

// UpdatePrice updates the price.
func (c *Collection) UpdatePrice(ctx context.Context, price string) error {
	if price == "" {
		return errors.ErrInconvertiblePrice
	}

	res, err := c.collection.Find(ctx, bson.D{})
	if err != nil {
		return err
	}

	if res.RemainingBatchLength() == 0 {
		return errors.ErrDoesNotExist
	}

	p := new(priceModels.Price)

	for res.Next(ctx) {
		if err = res.Decode(p); err != nil {
			return err
		}
	}

	oid, err := primitive.ObjectIDFromHex(p.ID)
	if err != nil {
		return err
	}

	filter := bson.D{
		{
			Key:   "_id",
			Value: oid,
		},
	}

	body := bson.D{
		{
			Key: "$set",
			Value: bson.D{
				{
					Key:   "updated_at",
					Value: time.Now().UTC(),
				},
				{
					Key:   "price_value",
					Value: price,
				},
			},
		},
	}

	result, err := c.collection.UpdateOne(ctx, filter, body)
	if err != nil {
		return err
	}

	if result.ModifiedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// Price returns the price.
func (c *Collection) Price(ctx context.Context) (*priceModels.Price, error) {
	res, err := c.collection.Find(ctx, bson.D{})
	if err != nil {
		return nil, err
	}

	if res.RemainingBatchLength() == 0 {
		return nil, errors.ErrDoesNotExist
	}

	p := new(priceModels.Price)

	for res.Next(ctx) {
		if err = res.Decode(p); err != nil {
			return nil, err
		}
	}

	return p, nil
}
