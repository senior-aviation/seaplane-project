package promocode

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/promocode"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

const (
	duplicateCode = 11000
)

// Collection the promocodes' collection structure.
type Collection struct {
	collection *mongo.Collection
}

// NewPromoCodeCollection initializes the promocode collection.
func NewPromoCodeCollection(collection *mongo.Collection) *Collection {
	return &Collection{collection: collection}
}

// CreatePromoCode creates the new promocode.
func (c *Collection) CreatePromoCode(ctx context.Context, promoCode *promocode.PromoCode) (string, error) {
	if promoCode == nil {
		return "", errors.ErrEmptyStruct
	}

	now := time.Now().UTC()

	body := bson.D{
		{Key: "_id", Value: primitive.NewObjectID()},
		{Key: "created_at", Value: now},
		{Key: "updated_at", Value: now},
		{Key: "name", Value: promoCode.Name},
		{Key: "start_date", Value: promoCode.StartDate},
		{Key: "end_date", Value: promoCode.EndDate},
		{Key: "limit", Value: promoCode.Limit},
		{Key: "condition", Value: promoCode.Condition},
		{Key: "profit", Value: promoCode.Profit},
		{Key: "is_active", Value: promoCode.IsActive},
	}

	result, err := c.collection.InsertOne(ctx, body)
	if isDup(err) {
		return "", errors.ErrAlreadyExists
	}

	if err != nil {
		return "", err
	}

	if res, ok := result.InsertedID.(primitive.ObjectID); ok {
		return res.Hex(), nil
	}

	return "", errors.ErrObjectID
}

// UpdatePromoCode updates the promocode information.
func (c *Collection) UpdatePromoCode(ctx context.Context, promoCode *promocode.PromoCode) error {
	if promoCode == nil {
		return errors.ErrEmptyStruct
	}

	if promoCode.ID == "" {
		return errors.ErrObjectID
	}

	id, err := primitive.ObjectIDFromHex(promoCode.ID)
	if err != nil {
		return err
	}

	filter := bson.D{{
		Key:   "_id",
		Value: id,
	}}

	body := bson.D{
		{
			Key: "$set",
			Value: bson.D{
				{Key: "updated_at", Value: time.Now().UTC()},
				{Key: "name", Value: promoCode.Name},
				{Key: "start_date", Value: promoCode.StartDate},
				{Key: "end_date", Value: promoCode.EndDate},
				{Key: "condition", Value: promoCode.Condition},
				{Key: "profit", Value: promoCode.Profit},
				{Key: "is_active", Value: promoCode.IsActive},
			},
		},
	}

	result, err := c.collection.UpdateOne(ctx, filter, body)
	if err != nil {
		return err
	}

	if result.ModifiedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// DeletePromoCode deletes the promocode by their internal id.
func (c *Collection) DeletePromoCode(ctx context.Context, id string) error {
	if id == "" {
		return errors.ErrObjectID
	}

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: objectID,
	}}

	result, err := c.collection.DeleteOne(ctx, filter)
	if err != nil {
		return err
	}

	if result.DeletedCount == 0 {
		return errors.ErrDoesNotExist
	}

	return nil
}

// PromoCodeByID returns the promocode by their internal ObjectID.
func (c *Collection) PromoCodeByID(ctx context.Context, id string) (*promocode.PromoCode, error) {
	if id == "" {
		return nil, errors.ErrObjectID
	}

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, errors.ErrObjectID
	}

	filter := bson.D{{
		Key:   "_id",
		Value: objectID,
	}}

	result := new(promocode.PromoCode)

	err = c.collection.FindOne(ctx, filter).Decode(result)
	switch err {
	case nil:
		return result, nil
	case mongo.ErrNoDocuments:
		return nil, errors.ErrDoesNotExist
	default:
		return nil, err
	}
}

// PromoCodes returns the list of promocodes.
func (c *Collection) PromoCodes(ctx context.Context, paging *models.Paging, name *string, isActive *bool) ([]*promocode.PromoCode, error) {
	opts := options.FindOptions{}

	if paging != nil {
		if paging.Page != nil {
			opts.Skip = paging.Page
		}

		if paging.Limit != nil {
			opts.Limit = paging.Limit
		}

		opts.SetSort(bson.D{{Key: paging.OrderBy, Value: paging.OrderKey}})
	}

	filter := bson.D{}

	if isActive != nil {
		filter = append(filter, bson.E{
			Key:   "is_active",
			Value: *isActive,
		})
	}

	if name != nil {
		filter = append(filter, bson.E{
			Key:   "name",
			Value: *name,
		})
	}

	cursor, err := c.collection.Find(ctx, filter, &opts)
	if err != nil {
		return nil, err
	}

	defer func(c *mongo.Cursor) {
		_ = c.Close(ctx)
	}(cursor)

	if cursor.RemainingBatchLength() == 0 {
		return nil, errors.ErrDoesNotExist
	}

	result := make([]*promocode.PromoCode, 0, cursor.RemainingBatchLength())

	err = cursor.All(ctx, &result)

	return result, err
}

// Count returns the number of documents by the provided filters.
func (c *Collection) Count(ctx context.Context, name *string, isActive *bool) (int64, error) {
	filter := bson.D{}

	if isActive != nil {
		filter = append(filter, bson.E{Key: "is_active", Value: *isActive})
	}

	if name != nil {
		filter = append(filter, bson.E{Key: "name", Value: *name})
	}

	return c.collection.CountDocuments(ctx, filter)
}

// DecreaseLimit decreases the limit for the promocode by name.
func (c *Collection) DecreaseLimit(ctx context.Context, name string) error {
	if name == "" {
		return errors.ErrMissingPromoCodeName
	}

	filter := bson.D{
		{
			Key:   "name",
			Value: name,
		},
		{
			Key:   "limit",
			Value: bson.M{"$ne": 0},
		},
	}

	update := bson.D{
		{
			Key:   "$inc",
			Value: bson.M{"limit": -1},
		},
	}

	_, err := c.collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	return nil
}

// isDup checks if the returned error is about the duplicate writing.
func isDup(err error) bool {
	var e mongo.WriteException

	if err == &e {
		for _, we := range e.WriteErrors {
			if we.Code == duplicateCode {
				return true
			}
		}
	}

	return false
}
