package worker

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/database"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/message"
)

type MessageWorker interface {
	CreateMessage(ctx context.Context, message *message.Message) (string, error)
	Messages(ctx context.Context, paging *models.Paging, email *string, isProcessed *bool) ([]*message.Message, error)
	MessageByID(ctx context.Context, id string) (*message.Message, error)
	ProcessMessage(ctx context.Context, id string, isProcessed bool) error
	Count(ctx context.Context, email *string, isProcessed *bool) (int64, error)
}

type Message struct {
	db database.Message
}

// NewMessageWorker creates a new instance of message worker.
func NewMessageWorker(db database.Message) MessageWorker {
	return &Message{db: db}
}

// CreateMessage creates the new message.
func (m *Message) CreateMessage(ctx context.Context, message *message.Message) (string, error) {
	return m.db.CreateMessage(ctx, message)
}

// Messages returns the list of messages.
func (m *Message) Messages(ctx context.Context, paging *models.Paging, email *string, isProcessed *bool) ([]*message.Message, error) {
	return m.db.Messages(ctx, paging, email, isProcessed)
}

// MessageByID returns the message by their internal ObjectID.
func (m *Message) MessageByID(ctx context.Context, id string) (*message.Message, error) {
	return m.db.MessageByID(ctx, id)
}

// ProcessMessage processes the message.
func (m *Message) ProcessMessage(ctx context.Context, id string, isProcessed bool) error {
	return m.db.ProcessMessage(ctx, id, isProcessed)
}

// Count returns the number of documents by the provided filters.
func (m *Message) Count(ctx context.Context, email *string, isProcessed *bool) (int64, error) {
	return m.db.Count(ctx, email, isProcessed)
}
