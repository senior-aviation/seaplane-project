package worker

import (
	"context"
	"fmt"
	"gitlab.com/senior-aviation/seaplane/app/database"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/order"
	"gitlab.com/senior-aviation/seaplane/app/models/promocode"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"math"
	"strconv"
	"time"
)

const (
	earthRadius  = 6371
	averageSpeed = 60
	maximumSeats = 8
)

const (
	bitSize        = 64
	minutesPerHour = 60
)

// CreateOrderResponse response for the order's creation.
type CreateOrderResponse struct {
	ID     int    `json:"id" example:"123456"` // ID of the order
	Price  string `json:"price" example:"10"`  // price of the order
	Status string `json:"status" example:"OK"` // status of the request
}

type OrderWorker interface {
	CreateOrder(ctx context.Context, order *api.NewOrder) (*CreateOrderResponse, error)
	UpdateOrder(ctx context.Context, order *order.Order) error
	OrderByID(ctx context.Context, id int) (*order.Order, error)
	Orders(ctx context.Context, filter *order.Filter, paging *models.Paging) ([]*order.Order, error)
	SharingOrders(ctx context.Context, filters *order.SharingOrderFilters, paging *models.Paging) ([]*order.SharingOrderInfo, error)
	SharingOrdersCount(ctx context.Context, filters *order.SharingOrderFilters) (int64, error)
	SharingOrderInfo(ctx context.Context, id int) (*order.SharingOrderInfo, error)
	JoinShareable(ctx context.Context, order *api.NewJoinOrder) (string, error)
	Count(ctx context.Context, filter *order.Filter) (int64, error)
	Earnings(ctx context.Context, request *models.AnalyticsRequest) (float64, error)
	PopularDestinations(ctx context.Context, request *models.AnalyticsRequest) ([]*order.DestinationAnalytics, error)
}

type Order struct {
	db          database.Order
	priceDB     database.Price
	profileDB   database.Profile
	portDB      database.Port
	promoCodeDB database.PromoCode
}

// NewOrderWorker creates a new instance of order worker.
func NewOrderWorker(db database.Order, priceDB database.Price, profileDB database.Profile, portDB database.Port, promoCodeDB database.PromoCode) OrderWorker {
	return &Order{db: db, priceDB: priceDB, profileDB: profileDB, portDB: portDB, promoCodeDB: promoCodeDB}
}

// CreateOrder creates new order.
func (o *Order) CreateOrder(ctx context.Context, order *api.NewOrder) (*CreateOrderResponse, error) {
	newOrder, err := o.orderMapper(order)
	if err != nil {
		return nil, err
	}

	id, err := o.db.CreateOrder(ctx, newOrder)
	if err != nil {
		return nil, err
	}

	return &CreateOrderResponse{
		ID:     id,
		Price:  newOrder.Price,
		Status: api.OK,
	}, nil
}

// UpdateOrder updates the order info.
func (o *Order) UpdateOrder(ctx context.Context, order *order.Order) error {
	return o.db.UpdateOrder(ctx, order)
}

// OrderByID returns the order info by its internal ID.
func (o *Order) OrderByID(ctx context.Context, id int) (*order.Order, error) {
	return o.db.OrderByID(ctx, id)
}

// Orders returns the list of orders based on filters and paging info.
func (o *Order) Orders(ctx context.Context, filter *order.Filter, paging *models.Paging) ([]*order.Order, error) {
	return o.db.Orders(ctx, filter, paging)
}

// SharingOrders returns the list of shareable orders.
func (o *Order) SharingOrders(ctx context.Context, filters *order.SharingOrderFilters, paging *models.Paging) ([]*order.SharingOrderInfo, error) {
	sharingOrders, err := o.db.SharingOrders(ctx, filters, paging)
	if err != nil {
		return nil, err
	}

	for i := range sharingOrders {
		p, err := o.portDB.PortByCoordinates(ctx, sharingOrders[i].From.Latitude, sharingOrders[i].From.Longitude)
		if err == nil && p != nil {
			sharingOrders[i].From.Name = p.Name
		}
	}

	return sharingOrders, nil
}

// SharingOrdersCount counts the needed sharing orders by the provided filters.
func (o *Order) SharingOrdersCount(ctx context.Context, filters *order.SharingOrderFilters) (int64, error) {
	return o.db.SharingOrdersCount(ctx, filters)
}

// SharingOrderInfo returns the sharing order info by its ID.
func (o *Order) SharingOrderInfo(ctx context.Context, id int) (*order.SharingOrderInfo, error) {
	return o.db.SharingOrderInfo(ctx, id)
}

// Count counts the needed orders by the provided filters.
func (o *Order) Count(ctx context.Context, filter *order.Filter) (int64, error) {
	return o.db.Count(ctx, filter)
}

// JoinShareable is used to join the available shared flight.
func (o *Order) JoinShareable(ctx context.Context, request *api.NewJoinOrder) (string, error) {
	passengers := make([]*order.Passenger, 0, len(request.Passengers)+1)

	targetOrder, err := o.db.OrderByID(ctx, request.OrderID)
	if err != nil {
		return "", err
	}

	if request.UserID != "" {
		if targetOrder.UserID == request.UserID {
			return "", errors.ErrTheSameUser
		}

		profile, err := o.profileDB.ProfileByID(ctx, request.UserID)
		if err != nil {
			return "", err
		}

		passengers = append(passengers, &order.Passenger{
			FirstName:   profile.FirstName,
			LastName:    profile.LastName,
			MiddleName:  profile.MiddleName,
			PhoneNumber: profile.PhoneNumber,
			Email:       profile.Email,
			Document:    profile.Document,
			UserID:      profile.ID,
			Status:      order.INPROCESS,
		})
	}

	if request.Email == targetOrder.Email || request.PhoneNumber == targetOrder.PhoneNumber {
		return "", errors.ErrTheSameUser
	}

	for _, pass := range request.Passengers {
		if pass.FirstName == "" || pass.LastName == "" || pass.Document == nil {
			return "", errors.ErrMissingDocument
		}

		if pass.Direction == "" {
			return "", errors.ErrNoDirection
		}

		pass.Status = order.INPROCESS

		passengers = append(passengers, pass)
	}

	price, err := strconv.ParseFloat(targetOrder.Price, 64)
	if err != nil {
		return "", err
	}

	price = price / float64(len(passengers)+len(targetOrder.Passengers))

	if request.PromoCode != nil {
		t := true

		promoCodes, err := o.promoCodeDB.PromoCodes(context.Background(), nil, request.PromoCode, &t)
		if err != nil {
			return "", err
		}

		if len(promoCodes) == 0 {
			return "", errors.ErrPromoNotFound
		}

		if err = promoCodes[0].Validate(targetOrder); err != nil {
			return "", err
		}

		if promoCodes[0].Profit == nil {
			return "", errors.ErrInvalidProfit
		}

		switch (*promoCodes[0].Profit).Type {
		case promocode.FIXED:
			price = price - float64((*promoCodes[0].Profit).Amount)
		case promocode.PERCENT:
			price = price * float64(100-(*promoCodes[0].Profit).Amount)
		default:
			return "", errors.ErrInvalidProfit
		}

		if price < 0 {
			price = 0
		}

		if promoCodes[0].Limit != nil && *promoCodes[0].Limit != 0 {
			if err = o.promoCodeDB.DecreaseLimit(context.Background(), promoCodes[0].Name); err != nil {
				return "", err
			}
		}
	}

	if err = o.db.JoinShareable(ctx, passengers, request.OrderID, []string{request.UserID}); err != nil {
		return "", err
	}

	return fmt.Sprintf("%f", price), nil
}

// orderMapper maps the new order to the full structure.
func (o *Order) orderMapper(newOrder *api.NewOrder) (*order.Order, error) {
	if newOrder == nil {
		return nil, errors.ErrEmptyStruct
	}

	result := new(order.Order)

	from, err := o.portDB.PortByCoordinates(context.Background(), newOrder.From.Latitude, newOrder.From.Longitude)
	switch err {
	case nil, errors.ErrDoesNotExist:
	default:
		return nil, err
	}

	to, err := o.portDB.PortByCoordinates(context.Background(), newOrder.To.Latitude, newOrder.To.Longitude)
	switch err {
	case nil, errors.ErrDoesNotExist:
	default:
		return nil, err
	}

	var status order.Status

	if from != nil && to != nil {
		status = order.INPROCESS
	} else {
		status = order.WAITING
	}

	result.Status = status

	result.From = newOrder.From

	if from != nil {
		result.From.Name = from.Name
	}

	result.To = newOrder.To

	if to != nil {
		result.To.Name = to.Name
	}

	result.DepartureTime = newOrder.DepartureTime

	price, err := o.priceDB.Price(context.Background())
	if err != nil {
		return nil, err
	}

	floatPrice, err := strconv.ParseFloat(price.PriceValue, bitSize)
	if err != nil {
		return nil, err
	}

	distance := calculateDistance(newOrder.From, newOrder.To)
	if distance > 1000 || distance < 3 {
		return nil, errors.ErrInvalidDistance
	}

	result.Price = fmt.Sprintf("%f", floatPrice*distance)

	hour := math.Floor(distance / averageSpeed)
	minute := int((distance/float64(averageSpeed))*minutesPerHour) % minutesPerHour

	result.ArrivalTime = newOrder.DepartureTime.Add(time.Hour * time.Duration(hour)).Add(time.Minute * time.Duration(minute))

	result.Passengers = newOrder.Passengers

	for i := range result.Passengers {
		result.Passengers[i].Status = status
		result.Passengers[i].Direction = order.Forward
	}

	result.Document = newOrder.Document

	if newOrder.UserID != "" {
		res, err := o.profileDB.ProfileByID(context.Background(), newOrder.UserID)
		if err != nil {
			return nil, err
		}

		result.Email = res.Email

		result.PhoneNumber = res.PhoneNumber

		result.UserID = newOrder.UserID

		result.FirstName = res.FirstName

		result.LastName = res.LastName

		result.MiddleName = res.MiddleName

		if res.Document != nil {
			result.Document = res.Document
		}
	} else {
		result.Email = newOrder.Email

		result.PhoneNumber = newOrder.PhoneNumber
	}

	if result.Document == nil {
		return nil, errors.ErrMissingDocument
	}

	if !newOrder.ReturnTime.IsZero() {
		if newOrder.ReturnTime.Before(newOrder.DepartureTime) {
			return nil, errors.ErrInvalidTimes
		}

		result.RoundDepartureTime = newOrder.ReturnTime

		result.RoundArrivalTime = newOrder.ReturnTime.Add(time.Hour * time.Duration(hour)).Add(time.Minute * time.Duration(minute))

		for i := range result.Passengers {
			result.Passengers[i].Direction = order.Full
		}
	}

	if newOrder.PromoCode != nil {
		t := true

		promoCodes, err := o.promoCodeDB.PromoCodes(context.Background(), nil, newOrder.PromoCode, &t)
		if err != nil {
			return nil, err
		}

		if len(promoCodes) == 0 {
			return nil, errors.ErrPromoNotFound
		}

		if err = promoCodes[0].Validate(result); err != nil {
			return nil, err
		}

		if promoCodes[0].Profit == nil {
			return nil, errors.ErrInvalidProfit
		}

		p, err := strconv.ParseFloat(result.Price, 64)
		if err != nil {
			return nil, err
		}

		switch (*promoCodes[0].Profit).Type {
		case promocode.FIXED:
			p = p - float64((*promoCodes[0].Profit).Amount)
		case promocode.PERCENT:
			p = p * float64(100-(*promoCodes[0].Profit).Amount)
		default:
			return nil, errors.ErrInvalidProfit
		}

		if p < 0 {
			p = 0
		}

		if promoCodes[0].Limit != nil && *promoCodes[0].Limit != 0 {
			if err = o.promoCodeDB.DecreaseLimit(context.Background(), promoCodes[0].Name); err != nil {
				return nil, err
			}
		}
	}

	sh := new(order.Sharing)

	if newOrder.Shareable {
		sh.Shareable = true
		sh.AvailableSeats = maximumSeats - 1 - len(newOrder.Passengers)
		result.JoinedUsers = []string{}
	}

	result.Sharing = sh

	return result, nil
}

// Earnings returns the total income for the specified period of time for analytics.
func (o *Order) Earnings(ctx context.Context, request *models.AnalyticsRequest) (float64, error) {
	return o.db.Earnings(ctx, request)
}

// PopularDestinations returns the percentage of ordering the distinct destinations for analytics.
func (o *Order) PopularDestinations(ctx context.Context, request *models.AnalyticsRequest) ([]*order.DestinationAnalytics, error) {
	pd, err := o.db.PopularDestinations(ctx, request)
	if err != nil {
		return nil, err
	}

	var totalCount float64

	for _, cnt := range pd {
		totalCount += cnt
	}

	result := make([]*order.DestinationAnalytics, 0, len(pd))

	for dst, cnt := range pd {
		pd[dst] = cnt / totalCount * 100

		result = append(result, &order.DestinationAnalytics{
			Location:   dst,
			Popularity: pd[dst],
		})
	}

	return result, nil
}

// calculateDistance calculates the distance between 2 points using 'haversine' formula.
func calculateDistance(from, to *order.Location) float64 {
	lat1 := degree2Radian(from.Latitude)
	lat2 := degree2Radian(to.Latitude)
	deltaLat := lat2 - lat1

	long1 := degree2Radian(from.Longitude)
	long2 := degree2Radian(to.Longitude)
	deltaLong := long2 - long1

	ans := math.Sin(deltaLat/2)*math.Sin(deltaLat/2) + math.Cos(lat1)*math.Cos(lat2)*math.Sin(deltaLong/2)*math.Sin(deltaLong/2)

	ans = 2 * math.Atan2(math.Sqrt(ans), math.Sqrt(1-ans))

	return math.Ceil(ans * earthRadius)
}

// degree2Radian converts degrees to radians.
func degree2Radian(degree float64) float64 {
	return degree * (math.Pi / 180)
}
