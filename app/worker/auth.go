package worker

import (
	"context"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/senior-aviation/seaplane/app/config"
	"gitlab.com/senior-aviation/seaplane/app/database"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models/auth"
	"gitlab.com/senior-aviation/seaplane/app/models/profile"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"golang.org/x/crypto/bcrypt"
	"math/rand"
	"net/smtp"
	"strconv"
	"strings"
	"time"
)

const (
	tokenExpiration = time.Minute * 30
	passwordCost    = 10
	passwordMinLen  = 6
)

const (
	otpEmail = `Subject: {subject}

    Dear User,

    Here is your OTP: {otp}

	Thank you for using our service!

    Kind Regards
    ONE Aviation Team.
`
)

type AuthWorker interface {
	Register(ctx context.Context, info *auth.RegistrationInfo) (string, error)
	LogIn(ctx context.Context, email, pw string) (*api.JWTResponse, error)
	ResetPasswordVerify(ctx context.Context, email string) (*api.RecoveryResponse, error)
	SendOTP(subject, email string) (string, error)
	ResetPassword(ctx context.Context, email, password string) error
}

type Auth struct {
	db database.Profile
}

// NewAuthWorker creates a new authorization worker instance.
func NewAuthWorker(db database.Profile) AuthWorker {
	return &Auth{db: db}
}

// Register registers new users.
func (a *Auth) Register(ctx context.Context, info *auth.RegistrationInfo) (string, error) {
	if err := info.Validate(); err != nil {
		return "", err
	}

	if !isPasswordStrong(info.Password) {
		return "", errors.ErrWeakPassword
	}

	var err error

	if info.Password, err = hashPassword(info.Password); err != nil {
		return "", err
	}

	p := registerNewUser(info)

	return a.db.CreateProfile(ctx, p)
}

// LogIn logs the users in based on their credentials' (phone & password) information.
func (a *Auth) LogIn(ctx context.Context, email, pw string) (*api.JWTResponse, error) {
	p, err := a.db.Profiles(ctx, &profile.Filter{Email: &email}, nil)
	if err != nil {
		return nil, err
	}

	if len(p) != 1 {
		return nil, errors.ErrBadCredentials
	}

	if !okPassword(pw, p[0].Password) {
		return nil, errors.ErrBadCredentials
	}

	accessToken, err := createAccessToken(p[0].ID, p[0].IsAdmin)
	if err != nil {
		return nil, err
	}

	refreshToken, err := createRefreshToken(p[0].ID, p[0].IsAdmin)
	if err != nil {
		return nil, err
	}

	return &api.JWTResponse{
		Status:       api.OK,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}, nil
}

// SendOTP sends the OTP to the user.
func (a *Auth) SendOTP(subject, email string) (string, error) {
	senderEmail := config.CurrentConfiguration.EmailLogin
	senderPassword := config.CurrentConfiguration.EmailPassword

	recipients := []string{email}

	host := "smtp.mail.ru"

	port := ":587"

	otp := otpGenerator()

	raw := strings.ReplaceAll(otpEmail, "{subject}", subject)
	raw = strings.ReplaceAll(raw, "{otp}", otp)

	body := []byte(raw)

	au := smtp.PlainAuth("", senderEmail, senderPassword, host)

	if err := smtp.SendMail(host+port, au, senderEmail, recipients, body); err != nil {
		return "", err
	}

	return otp, nil
}

// ResetPasswordVerify verifies the user based on their email to reset the password.
func (a *Auth) ResetPasswordVerify(ctx context.Context, email string) (*api.RecoveryResponse, error) {
	res, err := a.db.Profiles(ctx, &profile.Filter{Email: &email}, nil)
	if err != nil {
		return nil, err
	}

	otp, err := a.SendOTP("RESET PASSWORD", email)
	if err != nil {
		return nil, err
	}

	token, err := createAccessToken(res[0].ID, res[0].IsAdmin)
	if err != nil {
		return nil, err
	}

	return &api.RecoveryResponse{
		OTP:    otp,
		Status: api.OK,
		Token:  token,
	}, nil
}

// ResetPassword resets the password by email.
func (a *Auth) ResetPassword(ctx context.Context, email, password string) error {
	if !isPasswordStrong(password) {
		return errors.ErrWeakPassword
	}

	u, err := a.db.Profiles(context.Background(), &profile.Filter{Email: &email}, nil)
	if err != nil || len(u) == 0 {
		return err
	}

	if ctx.Value("user_id") != u[0].ID {
		return errors.ErrAccessDenied
	}

	password, err = hashPassword(password)
	if err != nil {
		return err
	}

	return a.db.ResetPassword(context.Background(), email, password)
}

// registerNewUser maps the registration info to register them in the database.
func registerNewUser(info *auth.RegistrationInfo) *profile.Profile {
	if info == nil {
		return nil
	}

	return &profile.Profile{
		FirstName:   info.FirstName,
		LastName:    info.LastName,
		MiddleName:  info.MiddleName,
		PhoneNumber: info.PhoneNumber,
		Email:       info.Email,
		Password:    info.Password,
		IsActive:    true,
	}
}

// hashPassword encrypts the provided password by the bcrypt algorithm and returns the result.
func hashPassword(pw string) (string, error) {
	if len(pw) < passwordMinLen {
		return "", errors.ErrShortPassword
	}

	bytes, err := bcrypt.GenerateFromPassword([]byte(pw), passwordCost)

	return string(bytes), err
}

// okPassword checks if the provided password matches with the encrypted one (in database).
func okPassword(pw, hash string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(pw)) == nil
}

// createAccessToken creates new JWT access token.
func createAccessToken(profileID string, isAdmin bool) (string, error) {
	claims := auth.JWTClaims{}

	claims.Id = profileID
	claims.IsAdmin = isAdmin

	return jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(config.CurrentConfiguration.JWTKey))
}

// createRefreshToken creates new JWT refresh token.
func createRefreshToken(profileID string, isAdmin bool) (string, error) {
	claims := auth.JWTClaims{}

	claims.Id = profileID
	claims.IsAdmin = isAdmin
	claims.ExpiresAt = time.Now().Add(tokenExpiration).Unix()

	return jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(config.CurrentConfiguration.JWTKey))
}

// otpGenerator generates a new one-time password in the range from 100000 and 999999.
func otpGenerator() string {
	min := 100000
	max := 999999

	return strconv.Itoa(rand.Intn(max-min) + min)
}

// isPasswordStrong checks whether the provided password is strong and secure.
func isPasswordStrong(password string) bool {
	var sixOrMore, number, upper, lower, special bool

	sixOrMore = len(password) >= 6

	for _, character := range password {
		if string(character) >= "0" && string(character) <= "9" {
			number = true
		} else if string(character) >= "A" && string(character) <= "Z" {
			upper = true
		} else if string(character) >= "a" && string(character) <= "z" {
			lower = true
		} else {
			special = true
		}
	}

	return sixOrMore && number && upper && lower && special
}
