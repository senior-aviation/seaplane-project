package worker

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/database"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/port"
)

type PortWorker interface {
	CreatePort(ctx context.Context, port *port.Port) (string, error)
	UpdatePort(ctx context.Context, port *port.Port) error
	DeletePort(ctx context.Context, id string) error
	PortByID(ctx context.Context, id string) (*port.Port, error)
	Ports(ctx context.Context, paging *models.Paging) ([]*port.Port, error)
	PortByCoordinates(ctx context.Context, lat, lng float64) (*port.Port, error)
	Count(ctx context.Context) (int64, error)
}

type Port struct {
	db database.Port
}

// NewPortWorker creates a new instance of port worker.
func NewPortWorker(db database.Port) PortWorker {
	return &Port{db: db}
}

// CreatePort creates the new port.
func (p *Port) CreatePort(ctx context.Context, port *port.Port) (string, error) {
	return p.db.CreatePort(ctx, port)
}

// UpdatePort updates the port information.
func (p *Port) UpdatePort(ctx context.Context, port *port.Port) error {
	return p.db.UpdatePort(ctx, port)
}

// DeletePort deletes the port by their internal id.
func (p *Port) DeletePort(ctx context.Context, id string) error {
	return p.db.DeletePort(ctx, id)
}

// PortByID returns the port by their internal ObjectID.
func (p *Port) PortByID(ctx context.Context, id string) (*port.Port, error) {
	return p.db.PortByID(ctx, id)
}

// Ports returns the list of ports.
func (p *Port) Ports(ctx context.Context, paging *models.Paging) ([]*port.Port, error) {
	return p.db.Ports(ctx, paging)
}

// PortByCoordinates returns the port object by the provided coordinates.
func (p *Port) PortByCoordinates(ctx context.Context, lat, lng float64) (*port.Port, error) {
	return p.db.PortByCoordinates(ctx, lat, lng)
}

// Count counts the needed ports by the provided filters.
func (p *Port) Count(ctx context.Context) (int64, error) {
	return p.db.Count(ctx)
}
