package worker

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/database"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/subscription"
	"net/mail"
)

type SubscriptionWorker interface {
	Subscribe(ctx context.Context, request *subscription.Subscription) error
	Unsubscribe(ctx context.Context, request *subscription.Subscription) error
	Subscriptions(ctx context.Context, paging *models.Paging, isActive *bool, email *string) ([]*subscription.Subscription, error)
	SubscriptionByID(ctx context.Context, id string) (*subscription.Subscription, error)
	CountSubscriptions(ctx context.Context, isActive *bool, email *string) (int64, error)
}

type Subscription struct {
	db        database.Subscription
	profileDB database.Profile
}

// NewSubscriptionWorker creates a new instance of subscription worker.
func NewSubscriptionWorker(db database.Subscription, profileDB database.Profile) SubscriptionWorker {
	return &Subscription{db: db, profileDB: profileDB}
}

// Subscribe subscribes the user to the system notifications.
func (s *Subscription) Subscribe(ctx context.Context, request *subscription.Subscription) error {
	if request.UserID != "" {
		usr, err := s.profileDB.ProfileByID(ctx, request.UserID)
		if err != nil {
			return err
		}

		request.Email = usr.Email
	} else {
		if request.Email == "" {
			return errors.ErrMissingEmail
		}

		if _, err := mail.ParseAddress(request.Email); err != nil {
			return err
		}
	}

	return s.db.Subscribe(ctx, request)
}

// Unsubscribe unsubscribes the user from the system notifications.
func (s *Subscription) Unsubscribe(ctx context.Context, request *subscription.Subscription) error {
	if request.UserID != "" {
		usr, err := s.profileDB.ProfileByID(ctx, request.UserID)
		if err != nil {
			return err
		}

		request.Email = usr.Email
	} else {
		if request.Email == "" {
			return errors.ErrMissingEmail
		}

		if _, err := mail.ParseAddress(request.Email); err != nil {
			return err
		}
	}

	return s.db.Unsubscribe(ctx, request)
}

// Subscriptions returns the list of subscriptions by paging and filters.
func (s *Subscription) Subscriptions(ctx context.Context, paging *models.Paging, isActive *bool, email *string) ([]*subscription.Subscription, error) {
	return s.db.Subscriptions(ctx, paging, isActive, email)
}

// SubscriptionByID returns the subscription by its internal object ID.
func (s *Subscription) SubscriptionByID(ctx context.Context, id string) (*subscription.Subscription, error) {
	return s.db.SubscriptionByID(ctx, id)
}

// CountSubscriptions returns the number of subscriptions matching the filters.
func (s *Subscription) CountSubscriptions(ctx context.Context, isActive *bool, email *string) (int64, error) {
	return s.db.CountSubscriptions(ctx, isActive, email)
}
