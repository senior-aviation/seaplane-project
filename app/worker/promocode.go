package worker

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/database"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/promocode"
)

type PromoCodeWalker interface {
	CreatePromoCode(ctx context.Context, promoCode *promocode.PromoCode) (string, error)
	UpdatePromoCode(ctx context.Context, promoCode *promocode.PromoCode) error
	DeletePromoCode(ctx context.Context, id string) error
	PromoCodeByID(ctx context.Context, id string) (*promocode.PromoCode, error)
	PromoCodes(ctx context.Context, paging *models.Paging, name *string, isActive *bool) ([]*promocode.PromoCode, error)
	Count(ctx context.Context, name *string, isActive *bool) (int64, error)
	ValidatePromoCode(ctx context.Context, promoCode string) error
}

type PromoCode struct {
	db database.PromoCode
}

// NewPromoCodeWalker creates a new instance of promocode worker.
func NewPromoCodeWalker(db database.PromoCode) PromoCodeWalker {
	return &PromoCode{db: db}
}

// CreatePromoCode creates the new promocode.
func (p *PromoCode) CreatePromoCode(ctx context.Context, promoCode *promocode.PromoCode) (string, error) {
	return p.db.CreatePromoCode(ctx, promoCode)
}

// UpdatePromoCode updates the promocode information.
func (p *PromoCode) UpdatePromoCode(ctx context.Context, promoCode *promocode.PromoCode) error {
	return p.db.UpdatePromoCode(ctx, promoCode)
}

// DeletePromoCode deletes the promocode by their internal id.
func (p *PromoCode) DeletePromoCode(ctx context.Context, id string) error {
	return p.db.DeletePromoCode(ctx, id)
}

// PromoCodeByID returns the promocode by their internal ObjectID.
func (p *PromoCode) PromoCodeByID(ctx context.Context, id string) (*promocode.PromoCode, error) {
	return p.db.PromoCodeByID(ctx, id)
}

// PromoCodes returns the list of promocodes.
func (p PromoCode) PromoCodes(ctx context.Context, paging *models.Paging, name *string, isActive *bool) ([]*promocode.PromoCode, error) {
	return p.db.PromoCodes(ctx, paging, name, isActive)
}

// Count returns the number of documents by the provided filters.
func (p *PromoCode) Count(ctx context.Context, name *string, isActive *bool) (int64, error) {
	return p.db.Count(ctx, name, isActive)
}

// ValidatePromoCode validates the entered promoCode.
func (p *PromoCode) ValidatePromoCode(ctx context.Context, promoCode string) error {
	t := true

	promoCodes, err := p.db.PromoCodes(ctx, nil, &promoCode, &t)
	if err != nil {
		return err
	}

	if len(promoCodes) == 0 {
		return errors.ErrPromoNotFound
	}

	return nil
}
