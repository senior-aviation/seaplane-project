package worker

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/database"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/profile"
)

type ProfileWorker interface {
	CreateProfile(ctx context.Context, profile *profile.Profile) (string, error)
	UpdateProfile(ctx context.Context, profile *profile.Profile) error
	DeleteProfile(ctx context.Context, id string) error
	ProfileByID(ctx context.Context, id string) (*profile.Profile, error)
	Profiles(ctx context.Context, filter *profile.Filter, paging *models.Paging) ([]*profile.Profile, error)
	Count(ctx context.Context, filter *profile.Filter) (int64, error)
	LikeBlog(ctx context.Context, userID, id string) error
	UnLikeBlog(ctx context.Context, userID, id string) error
	LikedBlogs(ctx context.Context, userID string) ([]string, error)
}

type Profile struct {
	db database.Profile
}

// NewProfileWorker creates a new instance of profile worker.
func NewProfileWorker(db database.Profile) ProfileWorker {
	return &Profile{db: db}
}

// CreateProfile creates the new profile.
func (p *Profile) CreateProfile(ctx context.Context, profile *profile.Profile) (string, error) {
	return p.db.CreateProfile(ctx, profile)
}

// UpdateProfile updates the profile information.
func (p *Profile) UpdateProfile(ctx context.Context, profile *profile.Profile) error {
	return p.db.UpdateProfile(ctx, profile)
}

// DeleteProfile deletes the profile by their internal id.
func (p *Profile) DeleteProfile(ctx context.Context, id string) error {
	return p.db.DeleteProfile(ctx, id)
}

// ProfileByID returns the profile by their internal ObjectID.
func (p *Profile) ProfileByID(ctx context.Context, id string) (*profile.Profile, error) {
	return p.db.ProfileByID(ctx, id)
}

// Profiles returns the list of profile based on the provided filters.
func (p *Profile) Profiles(ctx context.Context, filter *profile.Filter, paging *models.Paging) ([]*profile.Profile, error) {
	return p.db.Profiles(ctx, filter, paging)
}

// Count counts the needed profile by the provided filters.
func (p *Profile) Count(ctx context.Context, filter *profile.Filter) (int64, error) {
	return p.db.Count(ctx, filter)
}

// LikeBlog likes the blog by the given ID for the user.
func (p *Profile) LikeBlog(ctx context.Context, userID, id string) error {
	return p.db.LikeBlog(ctx, userID, id)
}

// UnLikeBlog unlikes the blog by the given ID for the user.
func (p *Profile) UnLikeBlog(ctx context.Context, userID, id string) error {
	return p.db.UnLikeBlog(ctx, userID, id)
}

// LikedBlogs returns the list of liked blogs for the user by their internal ID.
func (p *Profile) LikedBlogs(ctx context.Context, userID string) ([]string, error) {
	return p.db.LikedBlogs(ctx, userID)
}
