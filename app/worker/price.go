package worker

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/database"
	priceModels "gitlab.com/senior-aviation/seaplane/app/models/price"
)

type PriceWorker interface {
	CreatePrice(ctx context.Context, price string) error
	UpdatePrice(ctx context.Context, price string) error
	Price(ctx context.Context) (*priceModels.Price, error)
}

type Price struct {
	db database.Price
}

// NewPriceWorker creates a new instance of price worker.
func NewPriceWorker(db database.Price) PriceWorker {
	return &Price{db: db}
}

// CreatePrice creates a new price.
func (p *Price) CreatePrice(ctx context.Context, price string) error {
	return p.db.CreatePrice(ctx, price)
}

// UpdatePrice updates the price.
func (p *Price) UpdatePrice(ctx context.Context, price string) error {
	return p.db.UpdatePrice(ctx, price)
}

// Price returns the price.
func (p *Price) Price(ctx context.Context) (*priceModels.Price, error) {
	return p.db.Price(ctx)
}
