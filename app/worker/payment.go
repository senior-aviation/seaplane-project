package worker

import (
	creditcard "github.com/durango/go-credit-card"
	"gitlab.com/senior-aviation/seaplane/app/models/payment"
)

type PaymentWorker interface {
	ValidateCard(card *payment.CreditCard) error
}

type Payment struct{}

// NewPaymentWorker creates a new instance of payment worker.
func NewPaymentWorker() PaymentWorker {
	return &Payment{}
}

// ValidateCard validates the provided card information.
func (p *Payment) ValidateCard(card *payment.CreditCard) error {
	cardToValidate := creditcard.Card{
		Number: card.Number,
		Cvv:    card.CVV,
		Month:  card.Month,
		Year:   card.Year,
	}

	if err := cardToValidate.Method(); err != nil {
		return err
	}

	if err := cardToValidate.Validate(); err != nil {
		return err
	}

	return nil
}
