package worker

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/database"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/blog"
)

type BlogWorker interface {
	CreateBlog(ctx context.Context, blog *blog.Blog) (string, error)
	UpdateBlog(ctx context.Context, blog *blog.Blog) error
	DeleteBlog(ctx context.Context, id string) error
	BlogByID(ctx context.Context, id string, userID string) (*blog.Blog, error)
	BlogsByIDs(ctx context.Context, ids []string, paging *models.Paging) ([]*blog.Blog, error)
	Blogs(ctx context.Context, paging *models.Paging, isActive *bool, userID string) ([]*blog.Blog, error)
	Count(ctx context.Context) (int64, error)
	BlogsByIDsCount(ctx context.Context, ids []string) (int64, error)
	PopularBlogs(ctx context.Context) (map[string]float64, error)
}

type Blog struct {
	db        database.Blog
	profileDB database.Profile
}

// NewBlogWorker creates a new instance of blog worker.
func NewBlogWorker(db database.Blog, profileDB database.Profile) BlogWorker {
	return &Blog{db: db, profileDB: profileDB}
}

// CreateBlog creates the new blog.
func (b *Blog) CreateBlog(ctx context.Context, blog *blog.Blog) (string, error) {
	return b.db.CreateBlog(ctx, blog)
}

// UpdateBlog updates the blog information.
func (b *Blog) UpdateBlog(ctx context.Context, blog *blog.Blog) error {
	return b.db.UpdateBlog(ctx, blog)
}

// DeleteBlog deletes the blog by their internal id.
func (b *Blog) DeleteBlog(ctx context.Context, id string) error {
	return b.db.DeleteBlog(ctx, id)
}

// BlogByID returns the blog by their internal ObjectID.
func (b *Blog) BlogByID(ctx context.Context, id, userID string) (*blog.Blog, error) {
	result, err := b.db.BlogByID(ctx, id)
	if err != nil {
		return nil, err
	}

	if userID == "" {
		return result, nil
	}

	user, err := b.profileDB.ProfileByID(ctx, userID)

	if err == nil {
		for _, likedBlog := range user.LikedBlogs {
			if result.ID == likedBlog {
				result.Liked = true
			}
		}
	}

	return result, nil
}

// BlogsByIDs returns the blogs by the list of their internal ObjectIDs.
func (b *Blog) BlogsByIDs(ctx context.Context, ids []string, paging *models.Paging) ([]*blog.Blog, error) {
	isActive := true

	return b.db.BlogsByIDs(ctx, ids, paging, &isActive)
}

// Blogs returns the list of blogs.
func (b *Blog) Blogs(ctx context.Context, paging *models.Paging, isActive *bool, userID string) ([]*blog.Blog, error) {
	blogs, err := b.db.Blogs(ctx, paging, isActive)
	if err != nil {
		return nil, err
	}

	if userID == "" {
		return blogs, nil
	}

	user, err := b.profileDB.ProfileByID(ctx, userID)

	if err == nil {
		m := make(map[string]struct{}, len(user.LikedBlogs))

		for _, item := range user.LikedBlogs {
			m[item] = struct{}{}
		}

		for _, blg := range blogs {
			if _, ok := m[blg.ID]; ok {
				blg.Liked = true
			}
		}
	}

	return blogs, nil
}

// Count returns the number of documents by the provided filters.
func (b *Blog) Count(ctx context.Context) (int64, error) {
	return b.db.Count(ctx)
}

// BlogsByIDsCount counts the number of blogs requested by the list of ObjectIDs.
func (b *Blog) BlogsByIDsCount(ctx context.Context, ids []string) (int64, error) {
	isActive := true

	return b.db.BlogsByIDsCount(ctx, ids, &isActive)
}

// PopularBlogs returns the number of popular blogs' ObjectIDs for analytics.
func (b *Blog) PopularBlogs(ctx context.Context) (map[string]float64, error) {
	pb, err := b.profileDB.PopularBlogs(ctx)
	if err != nil {
		return nil, err
	}

	result := make(map[string]float64, len(pb))

	for blogID, count := range pb {
		blg, err := b.db.BlogByID(ctx, blogID)
		if err != nil {
			continue
		}

		result[blg.Title] = count
	}

	return result, nil
}
