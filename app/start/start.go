package start

import (
	"context"
	"gitlab.com/senior-aviation/seaplane/app/config"
	"gitlab.com/senior-aviation/seaplane/app/database"
	"gitlab.com/senior-aviation/seaplane/app/server"
	"log"
)

// Start starts the execution of the service with establishing the necessary connections.
func Start(ctx context.Context, config *config.Configuration) {
	db, err := database.NewDataBase(config)
	if err != nil {
		log.Println(err)
		return
	}

	defer func(ctx context.Context, db database.DataStore) {
		err = db.Close(ctx)
		if err != nil {
			log.Println("[ERROR] database closing", err)
		}
	}(ctx, db)

	if err = db.Connect(); err != nil {
		log.Println(err)
		return
	}

	server.Serve(config, db)
}
