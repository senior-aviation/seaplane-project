package subscription

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/subscription"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"gitlab.com/senior-aviation/seaplane/app/worker"
	"net/http"
	"net/mail"
	"strconv"
)

type Handler struct {
	subscriptionWorker worker.SubscriptionWorker
}

// NewHandler creates new instance of subscription handler.
func NewHandler(subscriptionWorker worker.SubscriptionWorker) *Handler {
	return &Handler{subscriptionWorker: subscriptionWorker}
}

// Routes for the chi router.
func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Put("/subscribe", h.subscribe)
	r.Put("/unsubscribe", h.unsubscribe)
	r.Get("/", h.subscriptions)
	r.Get("/{id}", h.subscriptionByID)

	return r
}

// @Summary Subscribe to the system notifications
// @Tags Subscription
// @Description This endpoint is used to subscribe the user to the system notifications by email.
// @Accept json
// @Produce json
// @Param email query string false "email of the user"
// @Success 200 {object} api.StatusResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string false "Authorization"
// @Router /api/v1/subscription/subscribe [put]
func (h *Handler) subscribe(w http.ResponseWriter, r *http.Request) {
	request := new(subscription.Subscription)

	if r.Context().Value("user_id") != nil {
		request.UserID = r.Context().Value("user_id").(string)
	}

	if param := r.URL.Query().Get("email"); param != "" {
		request.Email = param
	}

	if request.UserID == "" && request.Email == "" {
		api.HTTPError(w, errors.ErrMissingEmail, http.StatusBadRequest)
		return
	}

	if request.Email != "" {
		if _, err := mail.ParseAddress(request.Email); err != nil {
			api.HTTPError(w, err, http.StatusBadRequest)
			return
		}
	}

	err := h.subscriptionWorker.Subscribe(r.Context(), request)
	switch err {
	case nil:
	case errors.ErrEmptyStruct, errors.ErrAlreadyExists, errors.ErrMissingEmail:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Unsubscribe from the system notifications
// @Tags Subscription
// @Description This endpoint is used to unsubscribe the user from the system notifications by email.
// @Accept json
// @Produce json
// @Param email query string false "email of the user"
// @Success 200 {object} api.StatusResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string false "Authorization"
// @Router /api/v1/subscription/unsubscribe [put]
func (h *Handler) unsubscribe(w http.ResponseWriter, r *http.Request) {
	request := new(subscription.Subscription)

	if r.Context().Value("user_id") != nil {
		request.UserID = r.Context().Value("user_id").(string)
	}

	if param := r.URL.Query().Get("email"); param != "" {
		request.Email = param
	}

	if request.UserID == "" && request.Email == "" {
		api.HTTPError(w, errors.ErrMissingEmail, http.StatusBadRequest)
		return
	}

	err := h.subscriptionWorker.Unsubscribe(r.Context(), request)
	switch err {
	case nil:
	case errors.ErrEmptyStruct, errors.ErrAlreadyExists, errors.ErrMissingEmail:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Retrieve the list of subscriptions (admin)
// @Tags Subscription
// @Description This endpoint is used to retrieve the list of subscriptions by the provided paging and filters info.
// @Accept json
// @Produce json
// @Param is_active query bool false "filtering by the activity of the subscriptions"
// @Param email query string false "filtering by the email of the subscribed people"
// @Param limit query int false "limit of the returned subscriptions"
// @Param page query int false "the page of the returned subscriptions"
// @Param order_by query string false "the field by which the objects should be sorted"
// @Param order_key query int false "ascending (1) or descending (-1) sorting" Enums(1, -1)
// @Success 200 {object} api.Subscriptions
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/subscription [get]
func (h *Handler) subscriptions(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	pg, err := models.FetchPagingInfo(r)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	var isActive *bool

	if param := r.URL.Query().Get("is_active"); param != "" {
		val, err := strconv.ParseBool(param)
		if err == nil {
			isActive = &val
		}
	}

	var email *string

	if param := r.URL.Query().Get("email"); param != "" {
		email = &param
	}

	res, err := h.subscriptionWorker.Subscriptions(r.Context(), pg, isActive, email)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	count, err := h.subscriptionWorker.CountSubscriptions(r.Context(), isActive, email)
	switch err {
	case nil:
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	s := &api.Subscriptions{
		Subscriptions: res,
		Count:         int32(count),
	}

	render.JSON(w, r, s)
}

// @Summary Retrieve the subscription by ID (admin)
// @Tags Subscription
// @Description This endpoint is used to retrieve the subscription by its internal ID.
// @Accept json
// @Produce json
// @Param id path string true "ID of the subscription"
// @Success 200 {object} subscription.Subscription
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/subscription/{id} [get]
func (h *Handler) subscriptionByID(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	id := chi.URLParam(r, "id")

	res, err := h.subscriptionWorker.SubscriptionByID(r.Context(), id)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	case errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}
