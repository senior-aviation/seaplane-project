package promocode

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/promocode"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"gitlab.com/senior-aviation/seaplane/app/worker"
	"net/http"
	"strconv"
)

type Handler struct {
	promoCodeWalker worker.PromoCodeWalker
}

// NewHandler creates new instance of promocode handler.
func NewHandler(promoCodeWalker worker.PromoCodeWalker) *Handler {
	return &Handler{promoCodeWalker: promoCodeWalker}
}

// Routes for the chi router.
func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Post("/", h.createPromoCode)
	r.Put("/{id}", h.updatePromoCode)
	r.Delete("/{id}", h.deletePromoCode)
	r.Get("/{id}", h.promoCodeByID)
	r.Get("/", h.promoCodes)
	r.Get("/validate/{promocode}", h.validatePromoCode)

	return r
}

// @Summary Create new promocode (admin)
// @Tags Promocode
// @Description This endpoint is used to create a new promocode.
// @Accept json
// @Produce json
// @Param Promocode body promocode.PromoCode true "New promocode body"
// @Success 200 {object} api.IDResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/promocode [post]
func (h *Handler) createPromoCode(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	p := new(promocode.PromoCode)

	if err := json.NewDecoder(r.Body).Decode(p); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	if err := p.ValidateForModification(); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	res, err := h.promoCodeWalker.CreatePromoCode(r.Context(), p)
	switch err {
	case nil:
	case errors.ErrAlreadyExists, errors.ErrObjectID, errors.ErrEmptyStruct:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := &api.IDResponse{
		ID:     res,
		Status: api.OK,
	}

	render.JSON(w, r, j)
}

// @Summary Update promocode by ID (admin)
// @Tags Promocode
// @Description This endpoint is used to update an existing promocode by its internal ID.
// @Accept json
// @Produce json
// @Param Promocode body promocode.PromoCode true "Promocode body to update"
// @Param id path string true "ID of the promocode"
// @Success 200 {object} api.StatusResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/promocode/{id} [put]
func (h *Handler) updatePromoCode(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	p := new(promocode.PromoCode)

	if err := json.NewDecoder(r.Body).Decode(p); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	p.ID = chi.URLParam(r, "id")

	if err := p.ValidateForModification(); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	err := h.promoCodeWalker.UpdatePromoCode(r.Context(), p)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	case errors.ErrAlreadyExists, errors.ErrObjectID, errors.ErrEmptyStruct:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Delete promocode by ID (admin)
// @Tags Promocode
// @Description This endpoint is used to delete an existing promocode by its internal ID.
// @Accept json
// @Produce json
// @Param id path string true "ID of the promocode"
// @Success 200 {object} api.StatusResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/promocode/{id} [delete]
func (h *Handler) deletePromoCode(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	id := chi.URLParam(r, "id")

	err := h.promoCodeWalker.DeletePromoCode(r.Context(), id)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	case errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Retrieve the promocode by ID (admin)
// @Tags Promocode
// @Description This endpoint is used to retrieve the promocode by its internal ID.
// @Accept json
// @Produce json
// @Param id path string true "ID of the promocode"
// @Success 200 {object} promocode.PromoCode
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string false "Authorization"
// @Router /api/v1/promocode/{id} [get]
func (h *Handler) promoCodeByID(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	id := chi.URLParam(r, "id")

	res, err := h.promoCodeWalker.PromoCodeByID(r.Context(), id)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	case errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}

// @Summary Retrieve the list of promocodes (admin)
// @Tags Promocode
// @Description This endpoint is used to retrieve the list of promocodes by the provided paging info.
// @Accept json
// @Produce json
// @Param is_active query bool false "filtering by the activity of the promocodes"
// @Param name query string false "filtering by the name of the promocodes"
// @Param limit query int false "limit of the returned promocodes"
// @Param page query int false "the page of the returned promocodes"
// @Param order_by query string false "the field by which the objects should be sorted"
// @Param order_key query int false "ascending (1) or descending (-1) sorting" Enums(1, -1)
// @Success 200 {object} api.PromoCodes
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/promocode [get]
func (h *Handler) promoCodes(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	var isActive *bool

	var name *string

	if param := r.URL.Query().Get("is_active"); param != "" {
		val, err := strconv.ParseBool(param)
		if err != nil {
			api.HTTPError(w, err, http.StatusBadRequest)
			return
		}

		isActive = &val
	}

	if param := r.URL.Query().Get("name"); param != "" {
		name = &param
	}

	pg, err := models.FetchPagingInfo(r)

	res, err := h.promoCodeWalker.PromoCodes(r.Context(), pg, name, isActive)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	count, err := h.promoCodeWalker.Count(r.Context(), name, isActive)
	if err != nil {
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := &api.PromoCodes{
		PromoCodes: res,
		Count:      int32(count),
	}

	render.JSON(w, r, j)
}

// @Summary Check whether the promocode exists and is active
// @Tags Promocode
// @Description Check whether the promocode exists and is active.
// @Accept json
// @Produce json
// @Param promocode path string true "promocode"
// @Success 200 {object} api.StatusResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/promocode/validate/{promocode} [get]
func (h *Handler) validatePromoCode(w http.ResponseWriter, r *http.Request) {
	p := chi.URLParam(r, "promocode")

	err := h.promoCodeWalker.ValidatePromoCode(r.Context(), p)
	switch err {
	case nil:
	case errors.ErrDoesNotExist, errors.ErrPromoNotFound:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}
