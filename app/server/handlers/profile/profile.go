package profile

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/document"
	"gitlab.com/senior-aviation/seaplane/app/models/profile"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"gitlab.com/senior-aviation/seaplane/app/worker"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"strconv"
)

type Handler struct {
	profileWorker worker.ProfileWorker
}

// NewHandler creates new instance of profile handler.
func NewHandler(worker worker.ProfileWorker) *Handler {
	return &Handler{profileWorker: worker}
}

// Routes for the chi router.
func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Get("/", h.profiles)
	r.Get("/{id}", h.profileByID)
	r.Post("/", h.createProfile)
	r.Put("/{id}", h.updateProfile)
	r.Get("/my", h.myProfile)
	r.Get("/document-types", h.documentTypes)

	return r
}

// @Summary Create new profile (admin)
// @Tags Profile
// @Description This endpoint is used to create a new profile.
// @Accept json
// @Produce json
// @Param Profile body profile.Profile true "New Profile body"
// @Success 200 {object} api.IDResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/profile [post]
func (h *Handler) createProfile(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	input := new(profile.Profile)

	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	if err := input.Validate(); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	id, err := h.profileWorker.CreateProfile(r.Context(), input)
	switch err {
	case nil:
	case errors.ErrAlreadyExists:
		api.HTTPError(w, err, http.StatusConflict)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.IDResponse{
		ID:     id,
		Status: api.OK,
	}

	render.JSON(w, r, j)
}

// @Summary Update profile by ID (admin)
// @Tags Profile
// @Description This endpoint is used to update an existing profile by its internal ID.
// @Accept json
// @Produce json
// @Param Profile body profile.Profile true "Profile body to update"
// @Param id path string true "ID of the profile"
// @Success 200 {object} api.StatusResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/profile/{id} [put]
func (h *Handler) updateProfile(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	id := chi.URLParam(r, "id")

	if _, err := primitive.ObjectIDFromHex(id); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	input := new(profile.Profile)

	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	if err := input.Validate(); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	input.ID = id

	err := h.profileWorker.UpdateProfile(r.Context(), input)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	case errors.ErrAlreadyExists:
		api.HTTPError(w, err, http.StatusConflict)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Retrieve the profile by ID
// @Tags Profile
// @Description This endpoint is used to retrieve the profile by its internal ID.
// @Accept json
// @Produce json
// @Param id path string true "ID of the profile"
// @Success 200 {object} profile.Profile
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/profile/{id} [get]
func (h *Handler) profileByID(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	if r.Context().Value("is_admin") != true && r.Context().Value("user_id") != id {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	if _, err := primitive.ObjectIDFromHex(id); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	res, err := h.profileWorker.ProfileByID(r.Context(), id)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}

// @Summary Retrieve the list of profiles (admin)
// @Tags Profile
// @Description This endpoint is used to retrieve the list of profiles by the provided filters and paging info.
// @Accept json
// @Produce json
// @Param first_name query string false "user's first name"
// @Param last_name query string false "user's last name"
// @Param middle_name query string false "user's middle name"
// @Param phone_number query string false "user's prone number"
// @Param email query string false "user's email address"
// @Param is_active query bool false "user's activity flag"
// @Param limit query int false "limit of the returned profiles"
// @Param page query int false "the page of the returned profiles"
// @Param order_by query string false "the field by which the objects should be sorted"
// @Param order_key query int false "ascending (1) or descending (-1) sorting" Enums(1, -1)
// @Success 200 {object} api.Profiles
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/profile [get]
func (h *Handler) profiles(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	paging, err := models.FetchPagingInfo(r)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	filters := fetchFilters(r)

	if err = filters.Validate(); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	profiles, err := h.profileWorker.Profiles(r.Context(), filters, paging)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	count, err := h.profileWorker.Count(r.Context(), filters)
	if err != nil {
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.Profiles{
		Profiles: profiles,
		Count:    int32(count),
	}

	render.JSON(w, r, j)
}

// @Summary Retrieve the profile by token (authorization)
// @Tags Profile
// @Description This endpoint is used to retrieve the profile by the user's token.
// @Accept json
// @Produce json
// @Success 200 {object} profile.Profile
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/profile/my [get]
func (h *Handler) myProfile(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("user_id") == nil {
		api.HTTPError(w, errors.ErrUnauthorized, http.StatusUnauthorized)
		return
	}

	userID := r.Context().Value("user_id").(string)

	res, err := h.profileWorker.ProfileByID(r.Context(), userID)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}

// @Summary Retrieve the list of document types
// @Tags Profile
// @Description This endpoint is used to retrieve the list of document types.
// @Accept json
// @Produce json
// @Success 200 {array} string
// @Router /api/v1/profile/document-types [get]
func (h *Handler) documentTypes(w http.ResponseWriter, r *http.Request) {
	availableTypes := document.Types()
	result := make([]document.Type, 0, len(availableTypes))

	for t := range document.Types() {
		result = append(result, t)
	}

	render.JSON(w, r, result)
}

// fetchFilters fetches filters from the HTTP request.
func fetchFilters(r *http.Request) *profile.Filter {
	filter := new(profile.Filter)

	if param := r.URL.Query().Get("first_name"); param != "" {
		filter.FirstName = &param
	}

	if param := r.URL.Query().Get("last_name"); param != "" {
		filter.LastName = &param
	}

	if param := r.URL.Query().Get("middle_name"); param != "" {
		filter.MiddleName = &param
	}

	if param := r.URL.Query().Get("phone_number"); param != "" {
		filter.PhoneNumber = &param
	}

	if param := r.URL.Query().Get("email"); param != "" {
		filter.Email = &param
	}

	if param := r.URL.Query().Get("is_active"); param != "" {
		val, err := strconv.ParseBool(param)
		if err == nil {
			filter.IsActive = &val
		}
	}

	return filter
}
