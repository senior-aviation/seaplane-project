package auth

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models/auth"
	"gitlab.com/senior-aviation/seaplane/app/models/profile"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"gitlab.com/senior-aviation/seaplane/app/worker"
	"net/http"
)

type Handler struct {
	authWorker worker.AuthWorker
}

// NewHandler creates new instance of profile handler.
func NewHandler(worker worker.AuthWorker) *Handler {
	return &Handler{authWorker: worker}
}

// Routes for the chi router.
func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Post("/register", h.register)
	r.Post("/login", h.logIn)
	r.Put("/verify/email", h.verifyEmail)
	r.Put("/reset/password/verify", h.resetPasswordVerify)
	r.Put("/reset/password", h.resetPassword)

	return r
}

// @Summary Registers the user.
// @Tags Auth
// @Description This endpoint is used to register the user.
// @Accept json
// @Produce json
// @Param Profile body auth.RegistrationInfo true "New User Info"
// @Success 200 {object} api.IDResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/auth/register [post]
func (h *Handler) register(w http.ResponseWriter, r *http.Request) {
	info := new(auth.RegistrationInfo)

	if err := json.NewDecoder(r.Body).Decode(&info); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	if info == nil {
		api.HTTPError(w, errors.ErrEmptyStruct, http.StatusBadRequest)
		return
	}

	id, err := h.authWorker.Register(r.Context(), info)
	switch err {
	case nil:
		render.JSON(w, r, api.IDResponse{
			ID:     id,
			Status: api.OK,
		})

		return
	case errors.ErrInvalidEmail, errors.ErrInvalidPhoneNum,
		errors.ErrNoFirstName, errors.ErrNoLastName, errors.ErrNoPassword, errors.ErrWeakPassword, errors.ErrShortPassword:
		api.HTTPError(w, err, http.StatusBadRequest)

		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)

		return
	}
}

// @Summary Logs the user in.
// @Tags Auth
// @Description This endpoint is used to log in the user.
// @Accept json
// @Produce json
// @Param Credentials body api.Credentials true "User's credentials"
// @Success 200 {object} api.JWTResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/auth/login [post]
func (h *Handler) logIn(w http.ResponseWriter, r *http.Request) {
	credentials := new(api.Credentials)

	if err := json.NewDecoder(r.Body).Decode(&credentials); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	if credentials == nil {
		api.HTTPError(w, errors.ErrEmptyStruct, http.StatusBadRequest)
		return
	}

	if credentials.Email == "" {
		api.HTTPError(w, errors.ErrInvalidPhoneNum, http.StatusBadRequest)
		return
	}

	if credentials.Password == "" {
		api.HTTPError(w, errors.ErrNoPassword, http.StatusBadRequest)
		return
	}

	res, err := h.authWorker.LogIn(r.Context(), credentials.Email, credentials.Password)
	switch err {
	case nil:
	case errors.ErrDoesNotExist, errors.ErrBadCredentials:
		api.HTTPError(w, err, http.StatusUnauthorized)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}

// @Summary Verifies the user's email.
// @Tags Auth
// @Description This endpoint is used verify the user's email by the generated one-time password.
// @Accept json
// @Produce json
// @Param email query string true "User's email"
// @Success 200 {object} api.OTPResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/auth/verify/email [put]
func (h *Handler) verifyEmail(w http.ResponseWriter, r *http.Request) {
	var email string

	if param := r.URL.Query().Get("email"); param != "" {
		email = param
	} else {
		api.HTTPError(w, errors.ErrInvalidEmail, http.StatusBadRequest)
		return
	}

	otp, err := h.authWorker.SendOTP("VERIFICATION", email)
	if err != nil {
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, api.OTPResponse{
		OTP:    otp,
		Status: api.OK,
	})
}

// @Summary Verifies the users to reset the password.
// @Tags Auth
// @Description This endpoint is used to verify the user in password resetting.
// @Accept json
// @Produce json
// @Param email query string true "User's email"
// @Success 200 {object} api.RecoveryResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/auth/reset/password/verify [put]
func (h *Handler) resetPasswordVerify(w http.ResponseWriter, r *http.Request) {
	email := ""

	if param := r.URL.Query().Get("email"); param != "" {
		email = param
	}

	if !profile.IsEmailValid(email) {
		api.HTTPError(w, errors.ErrInvalidEmail, http.StatusBadRequest)
		return
	}

	res, err := h.authWorker.ResetPasswordVerify(r.Context(), email)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}

// @Summary Resets the user's password.
// @Tags Auth
// @Description This endpoint is used to reset the user's password.
// @Accept json
// @Produce json
// @Param Credentials body api.Credentials true "User's credentials"
// @Success 200 {object} api.StatusResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/auth/reset/password [put]
func (h *Handler) resetPassword(w http.ResponseWriter, r *http.Request) {
	in := new(api.Credentials)

	if err := json.NewDecoder(r.Body).Decode(in); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	err := h.authWorker.ResetPassword(r.Context(), in.Email, in.Password)
	switch err {
	case nil:
	case errors.ErrAccessDenied:
		api.HTTPError(w, err, http.StatusForbidden)
		return
	case errors.ErrShortPassword, errors.ErrWeakPassword:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}
