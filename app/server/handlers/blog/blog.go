package blog

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/blog"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"gitlab.com/senior-aviation/seaplane/app/worker"
	"net/http"
	"strconv"
)

type Handler struct {
	blogWorker    worker.BlogWorker
	profileWorker worker.ProfileWorker
}

// NewHandler creates new instance of blog handler.
func NewHandler(blogWorker worker.BlogWorker, profileWorker worker.ProfileWorker) *Handler {
	return &Handler{blogWorker: blogWorker, profileWorker: profileWorker}
}

// Routes for the chi router.
func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Post("/", h.createBlog)
	r.Put("/{id}", h.updateBlog)
	r.Delete("/{id}", h.deleteBlog)
	r.Get("/{id}", h.blogByID)
	r.Get("/", h.blogs)
	r.Get("/like/{id}", h.likeBlog)
	r.Get("/unlike/{id}", h.unlikeBlog)
	r.Get("/liked-blogs", h.likedBlogs)

	return r
}

// @Summary Create new blog (admin)
// @Tags Blog
// @Description This endpoint is used to create a new blog.
// @Accept json
// @Produce json
// @Param Blog body blog.Blog true "New blog body"
// @Success 200 {object} api.IDResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/blog [post]
func (h *Handler) createBlog(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	b := new(blog.Blog)

	if err := json.NewDecoder(r.Body).Decode(b); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	res, err := h.blogWorker.CreateBlog(r.Context(), b)
	switch err {
	case nil:
	case errors.ErrAlreadyExists:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := &api.IDResponse{
		ID:     res,
		Status: api.OK,
	}

	render.JSON(w, r, j)
}

// @Summary Update blog by ID (admin)
// @Tags Blog
// @Description This endpoint is used to update an existing blog by its internal ID.
// @Accept json
// @Produce json
// @Param Blog body blog.Blog true "Blog body to update"
// @Param id path string true "ID of the blog"
// @Success 200 {object} api.StatusResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/blog/{id} [put]
func (h *Handler) updateBlog(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	id := chi.URLParam(r, "id")

	b := new(blog.Blog)

	if err := json.NewDecoder(r.Body).Decode(b); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	b.ID = id

	err := h.blogWorker.UpdateBlog(r.Context(), b)
	switch err {
	case nil:
	case errors.ErrObjectID, errors.ErrEmptyStruct:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Delete blog by ID (admin)
// @Tags Blog
// @Description This endpoint is used to delete an existing blog by its internal ID.
// @Accept json
// @Produce json
// @Param id path string true "ID of the blog"
// @Success 200 {object} api.StatusResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/blog/{id} [delete]
func (h *Handler) deleteBlog(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	id := chi.URLParam(r, "id")

	err := h.blogWorker.DeleteBlog(r.Context(), id)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	case errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Retrieve the blog by ID
// @Tags Blog
// @Description This endpoint is used to retrieve the blog by its internal ID.
// @Accept json
// @Produce json
// @Param id path string true "ID of the blog"
// @Success 200 {object} blog.Blog
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string false "Authorization"
// @Router /api/v1/blog/{id} [get]
func (h *Handler) blogByID(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	var userID string

	if r.Context().Value("user_id") != nil {
		userID = r.Context().Value("user_id").(string)
	}

	res, err := h.blogWorker.BlogByID(r.Context(), id, userID)
	switch err {
	case nil:
	case errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}

// @Summary Retrieve the list of blogs
// @Tags Blog
// @Description This endpoint is used to retrieve the list of blogs by the provided paging info.
// @Accept json
// @Produce json
// @Param is_active query bool false "filtering by the activity of the blogs"
// @Param limit query int false "limit of the returned blogs"
// @Param page query int false "the page of the returned blogs"
// @Param order_by query string false "the field by which the objects should be sorted"
// @Param order_key query int false "ascending (1) or descending (-1) sorting" Enums(1, -1)
// @Success 200 {object} api.Blogs
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string false "Authorization"
// @Router /api/v1/blog [get]
func (h *Handler) blogs(w http.ResponseWriter, r *http.Request) {
	pg, err := models.FetchPagingInfo(r)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	var userID string

	if r.Context().Value("user_id") != nil {
		userID = r.Context().Value("user_id").(string)
	}

	var isActive *bool

	if param := r.URL.Query().Get("is_active"); param != "" {
		val, err := strconv.ParseBool(param)
		if err == nil {
			isActive = &val
		}
	}

	res, err := h.blogWorker.Blogs(r.Context(), pg, isActive, userID)
	switch err {
	case nil, errors.ErrDoesNotExist:
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	count, err := h.blogWorker.Count(r.Context())
	if err != nil {
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := &api.Blogs{
		Blogs: res,
		Count: int32(count),
	}

	render.JSON(w, r, j)
}

// @Summary Like blog by ID
// @Tags Blog
// @Description This endpoint is used to like the blog by its internal ID.
// @Accept json
// @Produce json
// @Param id path string true "ID of the blog"
// @Success 200 {object} api.StatusResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/blog/like/{id} [get]
func (h *Handler) likeBlog(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("user_id") == nil {
		api.HTTPError(w, errors.ErrUnauthorized, http.StatusUnauthorized)
		return
	}

	userID := r.Context().Value("user_id").(string)

	id := chi.URLParam(r, "id")

	err := h.profileWorker.LikeBlog(r.Context(), userID, id)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	case errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Unlike blog by ID
// @Tags Blog
// @Description This endpoint is used to unlike the blog by its internal ID.
// @Accept json
// @Produce json
// @Param id path string true "ID of the blog"
// @Success 200 {object} api.StatusResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/blog/unlike/{id} [get]
func (h *Handler) unlikeBlog(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("user_id") == nil {
		api.HTTPError(w, errors.ErrUnauthorized, http.StatusUnauthorized)
		return
	}

	userID := r.Context().Value("user_id").(string)

	id := chi.URLParam(r, "id")

	err := h.profileWorker.UnLikeBlog(r.Context(), userID, id)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	case errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Retrieve the list of liked blogs
// @Tags Blog
// @Description This endpoint is used to retrieve the list of liked blogs by the provided paging info.
// @Accept json
// @Produce json
// @Param limit query int false "limit of the returned blogs"
// @Param page query int false "the page of the returned blogs"
// @Param order_by query string false "the field by which the objects should be sorted"
// @Param order_key query int false "ascending (1) or descending (-1) sorting" Enums(1, -1)
// @Success 200 {object} api.Blogs
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/blog/liked-blogs [get]
func (h *Handler) likedBlogs(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("user_id") == nil {
		api.HTTPError(w, errors.ErrUnauthorized, http.StatusUnauthorized)
		return
	}

	userID := r.Context().Value("user_id").(string)

	blogIDs, err := h.profileWorker.LikedBlogs(r.Context(), userID)
	switch err {
	case nil:
	case errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	pg, err := models.FetchPagingInfo(r)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	res, err := h.blogWorker.BlogsByIDs(r.Context(), blogIDs, pg)
	switch err {
	case nil:
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	count, err := h.blogWorker.BlogsByIDsCount(r.Context(), blogIDs)
	if err != nil {
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := &api.Blogs{
		Blogs: res,
		Count: int32(count),
	}

	render.JSON(w, r, j)
}
