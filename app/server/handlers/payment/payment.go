package payment

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	orderModels "gitlab.com/senior-aviation/seaplane/app/models/order"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"gitlab.com/senior-aviation/seaplane/app/worker"
	"net/http"
)

type Handler struct {
	paymentWorker worker.PaymentWorker
	orderWorker   worker.OrderWorker
}

// NewHandler creates new instance of payment handler.
func NewHandler(worker worker.PaymentWorker, orderWorker worker.OrderWorker) *Handler {
	return &Handler{paymentWorker: worker, orderWorker: orderWorker}
}

// Routes for the chi router.
func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Post("/pay", h.processPayment)

	return r
}

// @Summary Processing the payment for the order
// @Tags Payment
// @Description This endpoint is used to the payment for the order.
// @Accept json
// @Produce json
// @Param Order body api.PaymentRequest true "Payment request"
// @Success 200 {object} api.PaymentResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/payments/pay [post]
func (h *Handler) processPayment(w http.ResponseWriter, r *http.Request) {
	var userID string

	if r.Context().Value("user_id") != nil {
		userID = r.Context().Value("user_id").(string)
	}

	paymentRequest := new(api.PaymentRequest)

	if err := json.NewDecoder(r.Body).Decode(paymentRequest); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	order, err := h.orderWorker.OrderByID(r.Context(), paymentRequest.OrderID)
	if err != nil {
		api.HTTPError(w, err, http.StatusNotFound)
		return
	}

	if err = h.paymentWorker.ValidateCard(paymentRequest.CreditCardInfo); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	order.Status = orderModels.ACTIVE

	for _, p := range order.Passengers {
		if p.UserID == userID || (len(paymentRequest.Emails) != 0 && doesEmailExistInList(p.Email, paymentRequest.Emails)) {
			p.Status = orderModels.ACTIVE
		}
	}

	if err = h.orderWorker.UpdateOrder(r.Context(), order); err != nil {
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := &api.PaymentResponse{
		Price:  order.Price,
		Status: api.OK,
	}

	render.JSON(w, r, j)
}

// doesEmailExistInList checks if the provided email exists in the provided list.
func doesEmailExistInList(email string, emails []string) bool {
	for _, item := range emails {
		if email == item {
			return true
		}
	}

	return false
}
