package port

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/order"
	"gitlab.com/senior-aviation/seaplane/app/models/port"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"gitlab.com/senior-aviation/seaplane/app/worker"
	"math"
	"net/http"
)

const (
	earthRadius = 6371
)

type Handler struct {
	portWorker worker.PortWorker
}

// NewHandler creates new instance of port handler.
func NewHandler(worker worker.PortWorker) *Handler {
	return &Handler{portWorker: worker}
}

// Routes for the chi router.
func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Post("/", h.createPort)
	r.Put("/{id}", h.updatePort)
	r.Delete("/{id}", h.deletePort)
	r.Get("/{id}", h.portByID)
	r.Get("/", h.ports)

	return r
}

// @Summary Create new port (admin)
// @Tags Port
// @Description This endpoint is used to create a new legal port.
// @Accept json
// @Produce json
// @Param Port body port.Port true "New port body"
// @Success 200 {object} api.IDResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/port [post]
func (h *Handler) createPort(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	p := new(port.Port)

	if err := json.NewDecoder(r.Body).Decode(p); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	res, err := h.portWorker.CreatePort(r.Context(), p)
	switch err {
	case nil:
	case errors.ErrAlreadyExists:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := &api.IDResponse{
		ID:     res,
		Status: api.OK,
	}

	render.JSON(w, r, j)
}

// @Summary Update port by ID (admin)
// @Tags Port
// @Description This endpoint is used to update an existing legal port by its internal ID.
// @Accept json
// @Produce json
// @Param Port body port.Port true "Port body to update"
// @Param id path string true "ID of the port"
// @Success 200 {object} api.StatusResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/port/{id} [put]
func (h *Handler) updatePort(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	id := chi.URLParam(r, "id")

	p := new(port.Port)

	if err := json.NewDecoder(r.Body).Decode(p); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	p.ID = id

	err := h.portWorker.UpdatePort(r.Context(), p)
	switch err {
	case nil:
	case errors.ErrObjectID, errors.ErrEmptyStruct:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Delete port by ID (admin)
// @Tags Port
// @Description This endpoint is used to delete an existing legal port by its internal ID.
// @Accept json
// @Produce json
// @Param id path string true "ID of the port"
// @Success 200 {object} api.StatusResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/port/{id} [delete]
func (h *Handler) deletePort(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	id := chi.URLParam(r, "id")

	err := h.portWorker.DeletePort(r.Context(), id)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	case errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Retrieve the port by ID
// @Tags Port
// @Description This endpoint is used to retrieve the port by its internal ID.
// @Accept json
// @Produce json
// @Param id path string true "ID of the port"
// @Success 200 {object} port.Port
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/port/{id} [get]
func (h *Handler) portByID(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	res, err := h.portWorker.PortByID(r.Context(), id)
	switch err {
	case nil:
	case errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}

// @Summary Retrieve the list of ports
// @Tags Port
// @Description This endpoint is used to retrieve the list of port by the provided paging info.
// @Accept json
// @Produce json
// @Param limit query int false "limit of the returned ports"
// @Param page query int false "the page of the returned ports"
// @Param order_by query string false "the field by which the objects should be sorted"
// @Param order_key query int false "ascending (1) or descending (-1) sorting" Enums(1, -1)
// @Success 200 {object} api.Ports
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/port [get]
func (h *Handler) ports(w http.ResponseWriter, r *http.Request) {
	pg, err := models.FetchPagingInfo(r)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	res, err := h.portWorker.Ports(r.Context(), pg)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	count, err := h.portWorker.Count(r.Context())
	if err != nil {
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := &api.Ports{
		Ports: res,
		Count: int32(count),
	}

	render.JSON(w, r, j)
}

// @Summary Validate the distance between the ports
// @Tags Port
// @Description This endpoint is used to validate the distance between the ports.
// @Accept json
// @Produce json
// @Param Ports body api.PortsDistanceValidationRequest true "Ports information"
// @Success 200 {object} api.StatusResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/port/validate [put]
func (h *Handler) validatePortsDistance(w http.ResponseWriter, r *http.Request) {
	ports := new(api.PortsDistanceValidationRequest)

	if err := json.NewDecoder(r.Body).Decode(ports); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	d := calculateDistance(ports.From, ports.To)

	if d > 1000 || d < 3 {
		api.HTTPError(w, errors.ErrInvalidDistance, http.StatusBadRequest)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// calculateDistance calculates the distance between 2 points using 'haversine' formula.
func calculateDistance(from, to *order.Location) float64 {
	lat1 := degree2Radian(from.Latitude)
	lat2 := degree2Radian(to.Latitude)
	deltaLat := lat2 - lat1

	long1 := degree2Radian(from.Longitude)
	long2 := degree2Radian(to.Longitude)
	deltaLong := long2 - long1

	ans := math.Sin(deltaLat/2)*math.Sin(deltaLat/2) + math.Cos(lat1)*math.Cos(lat2)*math.Sin(deltaLong/2)*math.Sin(deltaLong/2)

	ans = 2 * math.Atan2(math.Sqrt(ans), math.Sqrt(1-ans))

	return math.Ceil(ans * earthRadius)
}

// degree2Radian converts degrees to radians.
func degree2Radian(degree float64) float64 {
	return degree * (math.Pi / 180)
}
