package message

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/message"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"gitlab.com/senior-aviation/seaplane/app/worker"
	"net/http"
	"strconv"
)

type Handler struct {
	messageWorker worker.MessageWorker
}

// NewHandler creates new instance of message handler.
func NewHandler(messageWorker worker.MessageWorker) *Handler {
	return &Handler{messageWorker: messageWorker}
}

// Routes for the chi router.
func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Post("/", h.createMessage)
	r.Get("/", h.messages)
	r.Get("/{id}", h.messageByID)
	r.Put("/{id}", h.processMessage)

	return r
}

// @Summary Create new message
// @Tags Message
// @Description This endpoint is used to create a new message.
// @Accept json
// @Produce json
// @Param Message body message.Message true "New message body"
// @Success 200 {object} api.IDResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/message [post]
func (h *Handler) createMessage(w http.ResponseWriter, r *http.Request) {
	m := new(message.Message)

	if err := json.NewDecoder(r.Body).Decode(m); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	if err := m.Validate(); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	res, err := h.messageWorker.CreateMessage(r.Context(), m)
	if err != nil {
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := &api.IDResponse{
		ID:     res,
		Status: api.OK,
	}

	render.JSON(w, r, j)
}

// @Summary Retrieve the list of messages (admin)
// @Tags Message
// @Description This endpoint is used to retrieve the list of messages by the provided paging and filters info.
// @Accept json
// @Produce json
// @Param email query string false "filtering by the email of the messages' author"
// @Param is_processed query bool false "filtering by the processing flag of the messages"
// @Param limit query int false "limit of the returned messages"
// @Param page query int false "the page of the returned messages"
// @Param order_by query string false "the field by which the objects should be sorted"
// @Param order_key query int false "ascending (1) or descending (-1) sorting" Enums(1, -1)
// @Success 200 {object} api.Messages
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/message [get]
func (h *Handler) messages(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	pg, err := models.FetchPagingInfo(r)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	var email *string

	var isProcessed *bool

	if param := r.URL.Query().Get("email"); param != "" {
		email = &param
	}

	if param := r.URL.Query().Get("is_processed"); param != "" {
		val, err := strconv.ParseBool(param)
		if err != nil {
			api.HTTPError(w, err, http.StatusBadRequest)
			return
		}

		isProcessed = &val
	}

	res, err := h.messageWorker.Messages(r.Context(), pg, email, isProcessed)
	switch err {
	case nil, errors.ErrDoesNotExist:
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	count, err := h.messageWorker.Count(r.Context(), email, isProcessed)
	if err != nil {
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := &api.Messages{
		Messages: res,
		Count:    int32(count),
	}

	render.JSON(w, r, j)
}

// @Summary Retrieve the message by ID (admin)
// @Tags Message
// @Description This endpoint is used to retrieve the message by its internal ID.
// @Accept json
// @Produce json
// @Param id path string true "ID of the message"
// @Success 200 {object} message.Message
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/message/{id} [get]
func (h *Handler) messageByID(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	id := chi.URLParam(r, "id")

	res, err := h.messageWorker.MessageByID(r.Context(), id)
	switch err {
	case nil:
	case errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}

// @Summary Process message by ID (admin)
// @Tags Message
// @Description This endpoint is used to process the message by its internal ID.
// @Accept json
// @Produce json
// @Param id path string true "ID of the message"
// @Param is_processed query bool true "message's processing flag"
// @Success 200 {object} api.StatusResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/message/{id} [put]
func (h *Handler) processMessage(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	id := chi.URLParam(r, "id")

	var isProcessed bool

	if param := r.URL.Query().Get("is_processed"); param != "" {
		val, err := strconv.ParseBool(param)
		if err != nil {
			api.HTTPError(w, err, http.StatusBadRequest)
			return
		}

		isProcessed = val
	} else {
		api.HTTPError(w, fmt.Errorf("missing is_processed query parameter"), http.StatusBadRequest)
		return
	}

	err := h.messageWorker.ProcessMessage(r.Context(), id, isProcessed)
	switch err {
	case nil:
	case errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}
