package price

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"gitlab.com/senior-aviation/seaplane/app/worker"
	"net/http"
	"strconv"
)

const (
	bitSize = 64
)

type Handler struct {
	priceWorker worker.PriceWorker
}

// NewHandler creates new instance of price handler.
func NewHandler(worker worker.PriceWorker) *Handler {
	return &Handler{priceWorker: worker}
}

// Routes for the chi router.
func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Post("/", h.createPrice)
	r.Put("/", h.updatePrice)
	r.Get("/", h.price)

	return r
}

// @Summary Create the price (admin)
// @Tags Price
// @Description This endpoint is used to create a price.
// @Accept json
// @Produce json
// @Param Price body api.CreatePrice true "New price"
// @Success 200 {object} api.StatusResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/price [post]
func (h *Handler) createPrice(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	input := new(api.CreatePrice)

	if err := json.NewDecoder(r.Body).Decode(input); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	if _, err := strconv.ParseFloat(input.PriceValue, bitSize); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	err := h.priceWorker.CreatePrice(r.Context(), input.PriceValue)
	switch err {
	case nil:
	case errors.ErrInconvertiblePrice, errors.ErrAlreadyExists:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Update the price (admin)
// @Tags Price
// @Description This endpoint is used to update a price.
// @Accept json
// @Produce json
// @Param Price body api.CreatePrice true "Updated price"
// @Success 200 {object} api.StatusResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/price [put]
func (h *Handler) updatePrice(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	input := new(api.CreatePrice)

	if err := json.NewDecoder(r.Body).Decode(input); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	if _, err := strconv.ParseFloat(input.PriceValue, 64); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	err := h.priceWorker.UpdatePrice(r.Context(), input.PriceValue)
	switch err {
	case nil:
	case errors.ErrInconvertiblePrice, errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Get the price
// @Tags Price
// @Description This endpoint is used to get a price.
// @Accept json
// @Produce json
// @Success 200 {object} price.Price
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/price [get]
func (h *Handler) price(w http.ResponseWriter, r *http.Request) {
	res, err := h.priceWorker.Price(r.Context())
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}
