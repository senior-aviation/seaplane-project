package api

import (
	"encoding/json"
	"gitlab.com/senior-aviation/seaplane/app/models/blog"
	"gitlab.com/senior-aviation/seaplane/app/models/document"
	"gitlab.com/senior-aviation/seaplane/app/models/message"
	"gitlab.com/senior-aviation/seaplane/app/models/order"
	"gitlab.com/senior-aviation/seaplane/app/models/payment"
	"gitlab.com/senior-aviation/seaplane/app/models/port"
	"gitlab.com/senior-aviation/seaplane/app/models/profile"
	"gitlab.com/senior-aviation/seaplane/app/models/promocode"
	"gitlab.com/senior-aviation/seaplane/app/models/subscription"
	"net/http"
	"time"
)

const (
	OK = "OK"
)

func OKResponse() StatusResponse {
	return StatusResponse{Status: OK}
}

type IDResponse struct {
	ID     string `json:"id" example:"4af9f0700faf68c3039d0749"` // created object's internal ID
	Status string `json:"status" example:"OK"`                   // status of the request
}

type StatusResponse struct {
	Status string `json:"status" example:"OK"` // status of the request
}

type JWTResponse struct {
	Status       string `json:"status" example:"OK"`                                                                                                                                                                                                     // status of registration
	AccessToken  string `json:"access_token" example:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoX3V1aWQiOiIxZGQ5MDEwYy00MzI4LTRmZjMtYjllNi05NDRkODQ4ZTkzNzUiLCJhdXRob3JpemVkIjp0cnVlLCJ1c2VyX2lkIjo3fQ.Qy8l-9GUFsXQm4jqgswAYTAX9F4cngrl28WJVYNDwtM"`  // access token
	RefreshToken string `json:"refresh_token" example:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoX3V1aWQiOiIxZGQ5MDEwYy00MzI4LTRmZjMtYjllNi05NDRkODQ4ZTkzNzUiLCJhdXRob3JpemVkIjp0cnVlLCJ1c2VyX2lkIjo3fQ.Qy8l-9GUFsXQm4jqgswAYTAX9F4cngrl28WJVYNDwtM"` // refresh token
}

type Profiles struct {
	Profiles []*profile.Profile `json:"result"`             // array of profiles
	Count    int32              `json:"count" example:"10"` // number of returned profiles
}

type Credentials struct {
	Email    string `json:"email" example:"luke.skywalker@gmail.com"` // the user's email
	Password string `json:"password" example:"qwerty123"`             // the user's password
}

type OTPResponse struct {
	OTP    string `json:"otp" example:"145236"` // one-time password to verify
	Status string `json:"status" example:"OK"`  // the status showing if the email has been sent
}

type RecoveryResponse struct {
	OTP    string `json:"otp" example:"145236"`                                                                                                                                                                                            // one-time password to verify
	Status string `json:"status" example:"OK"`                                                                                                                                                                                             // the status showing if the email has been sent
	Token  string `json:"token" example:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoX3V1aWQiOiIxZGQ5MDEwYy00MzI4LTRmZjMtYjllNi05NDRkODQ4ZTkzNzUiLCJhdXRob3JpemVkIjp0cnVlLCJ1c2VyX2lkIjo3fQ.Qy8l-9GUFsXQm4jqgswAYTAX9F4cngrl28WJVYNDwtM"` // user's access token
}

type Orders struct {
	Orders []*order.Order `json:"orders"`             // array of orders
	Count  int32          `json:"count" example:"10"` // number of returned orders
}

type OrderIDResponse struct {
	ID     int    `json:"order_id" example:"215879"` // created order's ID
	Status string `json:"status" example:"OK"`       // status of the request
}

type CreatePrice struct {
	PriceValue string `json:"price_value" example:"94.99"` // price value
}

type NewOrder struct {
	From          *order.Location    `json:"from"`                                          // departure port's coordinates
	To            *order.Location    `json:"to"`                                            // destination port's coordinates
	DepartureTime time.Time          `json:"departure_time" example:"1970-01-01T07:00:00Z"` // departure time
	Passengers    []*order.Passenger `json:"passengers"`                                    // order's passengers
	PhoneNumber   string             `json:"phone_number" example:"77071234567"`            // customer's phone number
	Email         string             `json:"email" example:"luke.skywalker@gmail.com"`      // customer's email
	UserID        string             `json:"-"`                                             // user ID
	Document      *document.Document `json:"document"`                                      // customer's identity document
	ReturnTime    time.Time          `json:"return_time" example:"1970-01-01T07:00:00Z"`    // roundtrip departure time
	PromoCode     *string            `json:"promocode" example:"GIVEMEDISCOUNT123"`         // promocode
	Shareable     bool               `json:"shareable" bson:"shareable" example:"true"`     // flag showing whether that flight is shareable
}

type NewJoinOrder struct {
	OrderID     int                `json:"order_id"`                                 // order's ID
	Passengers  []*order.Passenger `json:"passengers"`                               // order's passengers
	PhoneNumber string             `json:"phone_number" example:"77071234567"`       // customer's phone number
	Email       string             `json:"email" example:"luke.skywalker@gmail.com"` // customer's email
	UserID      string             `json:"-"`                                        // user ID
	Document    *document.Document `json:"document"`                                 // customer's identity document
	PromoCode   *string            `json:"promocode" example:"GIVEMEDISCOUNT123"`    // promocode
}

type JoinOrderResponse struct {
	Status string `json:"status" example:"OK"` // status of the request
	Price  string `json:"price" example:"100"` // price of the order
}

type SharingFlights struct {
	Flights []*order.SharingOrderInfo `json:"flights"` // array of shareable flights
	Count   int32                     `json:"count"`   // the number of shareable flights
}

type Ports struct {
	Ports []*port.Port `json:"ports"`              // array of legal ports
	Count int32        `json:"count" example:"10"` // number of returned orders
}

type PaymentRequest struct {
	CreditCardInfo *payment.CreditCard `json:"credit_card_info"` // credit card info
	OrderID        int                 `json:"order_id"`         // order ID
	Emails         []string            `json:"email"`            // passenger's emails
}

type PaymentResponse struct {
	Price  string `json:"price"`  // the charged price
	Status string `json:"status"` // status of the payment
}

type Blogs struct {
	Blogs []*blog.Blog `json:"blogs"`              // array of blogs
	Count int32        `json:"count" example:"10"` // number of returned blogs
}

type Subscriptions struct {
	Subscriptions []*subscription.Subscription `json:"subscriptions"`      // array of subscriptions
	Count         int32                        `json:"count" example:"10"` // number of returned subscriptions
}

type PromoCodes struct {
	PromoCodes []*promocode.PromoCode `json:"promocodes"`         // array of promocodes
	Count      int32                  `json:"count" example:"10"` // number of returned promocodes
}

type Messages struct {
	Messages []*message.Message `json:"messages"`           // array of messages
	Count    int32              `json:"count" example:"10"` // number of returned messages
}

// PortsDistanceValidationRequest receives the ports to validate the distance between them to make an order.
type PortsDistanceValidationRequest struct {
	From *order.Location `json:"from"` // departure port
	To   *order.Location `json:"to"`   // destination port
}

type EarningsResponse struct {
	Earning float64 `json:"earning" example:"56840"` // earning for the analytics
}

type ErrResponse struct {
	Error string `json:"error"` // error content
	Code  int    `json:"code"`  // error code
}

func HTTPError(w http.ResponseWriter, err error, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)

	res := &ErrResponse{
		Error: err.Error(),
		Code:  code,
	}

	if err = json.NewEncoder(w).Encode(res); err != nil {
		return
	}
}
