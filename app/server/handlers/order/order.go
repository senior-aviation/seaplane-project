package order

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/models/order"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"gitlab.com/senior-aviation/seaplane/app/worker"
	"io"
	"net/http"
	"strconv"
)

type Handler struct {
	orderWorker worker.OrderWorker
}

// NewHandler creates new instance of profile handler.
func NewHandler(orderWorker worker.OrderWorker) *Handler {
	return &Handler{orderWorker: orderWorker}
}

// Routes for the chi router.
func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Post("/", h.createOrder)
	r.Put("/{id}", h.updateOrder)
	r.Get("/{id}", h.orderByID)
	r.Get("/", h.orders)
	r.Get("/history", h.history)
	r.Post("/sharing", h.sharingOrders)
	r.Get("/sharing/{id}", h.sharingOrder)
	r.Post("/join", h.joinShareable)

	return r
}

// @Summary Create new order
// @Tags Order
// @Description This endpoint is used to create a new order.
// @Accept json
// @Produce json
// @Param Order body api.NewOrder true "New Order body"
// @Success 200 {object} worker.CreateOrderResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string false "Authorization"
// @Router /api/v1/order [post]
func (h *Handler) createOrder(w http.ResponseWriter, r *http.Request) {
	var userID string

	if r.Context().Value("user_id") != nil {
		userID = r.Context().Value("user_id").(string)
	}

	body := new(api.NewOrder)

	if err := json.NewDecoder(r.Body).Decode(body); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	if userID == "" && (body.Email == "" || body.PhoneNumber == "") {
		api.HTTPError(w, errors.ErrMissingContactInfo, http.StatusBadRequest)
		return
	}

	body.UserID = userID

	res, err := h.orderWorker.CreateOrder(r.Context(), body)
	switch err {
	case nil:
	case errors.ErrEmptyStruct:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	case errors.ErrInvalidPort, errors.ErrMissingName, errors.ErrMissingDocument, errors.ErrInvalidDistance,
		errors.ErrInvalidTimes:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	case errors.ErrAlreadyExists:
		api.HTTPError(w, err, http.StatusConflict)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}

// @Summary Update order by ID (admin)
// @Tags Order
// @Description This endpoint is used to update an existing order by its internal ID.
// @Accept json
// @Produce json
// @Param Order body order.Order true "Order body to update"
// @Param id path int true "ID of the order"
// @Success 200 {object} api.StatusResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/order/{id} [put]
func (h *Handler) updateOrder(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrUnauthorized, http.StatusUnauthorized)
		return
	}

	id := chi.URLParam(r, "id")

	idInt, err := strconv.Atoi(id)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	body := new(order.Order)

	if err = json.NewDecoder(r.Body).Decode(&body); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	body.ID = idInt

	if err = body.Validate(); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	err = h.orderWorker.UpdateOrder(r.Context(), body)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	case errors.ErrEmptyStruct, errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := api.OKResponse()

	render.JSON(w, r, j)
}

// @Summary Retrieve the order by ID (admin)
// @Tags Order
// @Description This endpoint is used to retrieve the order by its internal ID.
// @Accept json
// @Produce json
// @Param id path int true "ID of the order"
// @Success 200 {object} order.Order
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/order/{id} [get]
func (h *Handler) orderByID(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrUnauthorized, http.StatusUnauthorized)
		return
	}

	id := chi.URLParam(r, "id")

	idInt, err := strconv.Atoi(id)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	res, err := h.orderWorker.OrderByID(r.Context(), idInt)
	switch err {
	case nil:
	case errors.ErrDoesNotExist, errors.ErrObjectID:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	if r.Context().Value("user_id").(string) != res.UserID && !r.Context().Value("is_admin").(bool) {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	render.JSON(w, r, res)
}

// @Summary Retrieve the list of orders (admin)
// @Tags Order
// @Description This endpoint is used to retrieve the list of orders by the provided filters and paging info.
// @Accept json
// @Produce json
// @Param order_id query int false "the ID of the order"
// @Param status query string false "the status of the order" Enums(ACTIVE, CANCELED, WAITING, FINISHED)
// @Param user_id query string false "the order's initiator's ID"
// @Param limit query int false "limit of the returned ports"
// @Param page query int false "the page of the returned ports"
// @Param order_by query string false "the field by which the objects should be sorted"
// @Param order_key query int false "ascending (1) or descending (-1) sorting" Enums(1, -1)
// @Success 200 {object} api.Orders
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/order [get]
func (h *Handler) orders(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	filters, err := filtersParse(r)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	if err = filters.Validate(); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	paging, err := models.FetchPagingInfo(r)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	res, err := h.orderWorker.Orders(r.Context(), filters, paging)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	count, err := h.orderWorker.Count(r.Context(), filters)
	if err != nil {
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, api.Orders{
		Orders: res,
		Count:  int32(count),
	})
}

// @Summary Retrieve the history of orders for the user
// @Tags Order
// @Description This endpoint is used to retrieve the history of orders.
// @Accept json
// @Produce json
// @Param limit query int false "limit of the returned ports"
// @Param page query int false "the page of the returned ports"
// @Param order_by query string false "the field by which the objects should be sorted"
// @Param order_key query int false "ascending (1) or descending (-1) sorting" Enums(1, -1)
// @Success 200 {object} api.Orders
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/order/history [get]
func (h *Handler) history(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("user_id") == nil {
		api.HTTPError(w, errors.ErrUnauthorized, http.StatusUnauthorized)
		return
	}

	userID := r.Context().Value("user_id").(string)

	paging, err := models.FetchPagingInfo(r)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	res, err := h.orderWorker.Orders(r.Context(), &order.Filter{UserID: &userID}, paging)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		res = make([]*order.Order, 0)
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	count, err := h.orderWorker.Count(r.Context(), &order.Filter{UserID: &userID})
	if err != nil {
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, api.Orders{
		Orders: res,
		Count:  int32(count),
	})
}

// @Summary Retrieve the list of shareable orders for the user
// @Tags Order
// @Description This endpoint is used to retrieve the list of shareable orders.
// @Accept json
// @Produce json
// @Param Order body order.SharingOrderFilters false "sharing orders' filters"
// @Param limit query int false "limit of the returned ports"
// @Param page query int false "the page of the returned ports"
// @Param order_by query string false "the field by which the objects should be sorted"
// @Param order_key query int false "ascending (1) or descending (-1) sorting" Enums(1, -1)
// @Success 200 {object} api.SharingFlights
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/order/sharing [post]
func (h *Handler) sharingOrders(w http.ResponseWriter, r *http.Request) {
	paging, err := models.FetchPagingInfo(r)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	filters := new(order.SharingOrderFilters)

	err = json.NewDecoder(r.Body).Decode(filters)
	switch err {
	case nil, io.EOF:
	default:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	res := make([]*order.SharingOrderInfo, 0)

	var c int64

	if filters.DateFrom != nil && filters.DateTo != nil {
		f1 := &order.SharingOrderFilters{
			LocationFrom:       filters.LocationFrom,
			LocationTo:         filters.LocationTo,
			DateFrom:           filters.DateFrom,
			NumberOfPassengers: filters.NumberOfPassengers,
			Flexible:           filters.Flexible,
		}

		res1, err := h.orderWorker.SharingOrders(r.Context(), f1, paging)
		switch err {
		case nil:
		case errors.ErrDoesNotExist:
		default:
			api.HTTPError(w, err, http.StatusInternalServerError)
			return
		}

		c1, err := h.orderWorker.SharingOrdersCount(r.Context(), f1)
		if err != nil {
			api.HTTPError(w, err, http.StatusInternalServerError)
			return
		}

		f2 := &order.SharingOrderFilters{
			LocationFrom:       filters.LocationFrom,
			LocationTo:         filters.LocationTo,
			DateTo:             filters.DateTo,
			NumberOfPassengers: filters.NumberOfPassengers,
			Flexible:           filters.Flexible,
		}

		res2, err := h.orderWorker.SharingOrders(r.Context(), f2, paging)
		switch err {
		case nil:
		case errors.ErrDoesNotExist:
		default:
			api.HTTPError(w, err, http.StatusInternalServerError)
			return
		}

		c2, err := h.orderWorker.SharingOrdersCount(r.Context(), f1)
		if err != nil {
			api.HTTPError(w, err, http.StatusInternalServerError)
			return
		}

		res = append(res, res1...)
		res = append(res, res2...)

		c = c1 + c2
	} else {
		res, err = h.orderWorker.SharingOrders(r.Context(), filters, paging)
		switch err {
		case nil:
		case errors.ErrDoesNotExist:
		default:
			api.HTTPError(w, err, http.StatusInternalServerError)
			return
		}

		c, err = h.orderWorker.SharingOrdersCount(r.Context(), filters)
		if err != nil {
			api.HTTPError(w, err, http.StatusInternalServerError)
			return
		}
	}

	j := api.SharingFlights{
		Flights: res,
		Count:   int32(c),
	}

	render.JSON(w, r, j)
}

// @Summary Retrieve the shareable order information for the user
// @Tags Order
// @Description This endpoint is used to retrieve the shareable order information.
// @Accept json
// @Produce json
// @Param id path int true "order ID"
// @Success 200 {object} order.SharingOrderInfo
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 404 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Router /api/v1/order/sharing/{id} [get]
func (h *Handler) sharingOrder(w http.ResponseWriter, r *http.Request) {
	idParam := chi.URLParam(r, "id")

	id, err := strconv.Atoi(idParam)
	if err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	o, err := h.orderWorker.SharingOrderInfo(r.Context(), id)
	switch err {
	case nil:
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, o)
}

// @Summary Join the shareable order
// @Tags Order
// @Description This endpoint is used to join to the shareable order.
// @Accept json
// @Produce json
// @Param Order body api.NewJoinOrder true "Joining information"
// @Success 200 {object} api.JoinOrderResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string false "Authorization"
// @Router /api/v1/order/join [post]
func (h *Handler) joinShareable(w http.ResponseWriter, r *http.Request) {
	var userID string

	if r.Context().Value("user_id") != nil {
		userID = r.Context().Value("user_id").(string)
	}

	body := new(api.NewJoinOrder)

	if err := json.NewDecoder(r.Body).Decode(body); err != nil {
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	}

	if userID == "" && (body.Email == "" || body.PhoneNumber == "") {
		api.HTTPError(w, errors.ErrMissingContactInfo, http.StatusBadRequest)
		return
	}

	body.UserID = userID

	price, err := h.orderWorker.JoinShareable(r.Context(), body)
	switch err {
	case nil:
	case errors.ErrTheSameUser, errors.ErrNoDirection:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	case errors.ErrDoesNotExist:
		api.HTTPError(w, err, http.StatusNotFound)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := &api.JoinOrderResponse{
		Status: api.OK,
		Price:  price,
	}

	render.JSON(w, r, j)
}

// filtersParse parses filter info from http queries.
func filtersParse(r *http.Request) (*order.Filter, error) {
	filter := new(order.Filter)

	if param := r.URL.Query().Get("order_id"); param != "" {
		id, err := strconv.Atoi(param)
		if err != nil {
			return nil, err
		}

		filter.OrderID = &id
	}

	if param := r.URL.Query().Get("status"); param != "" {
		filter.Status = (*order.Status)(&param)
	}

	if param := r.URL.Query().Get("user_id"); param != "" {
		filter.UserID = &param
	}

	return filter, nil
}
