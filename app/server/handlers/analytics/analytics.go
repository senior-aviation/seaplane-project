package analytics

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"gitlab.com/senior-aviation/seaplane/app/worker"
	"net/http"
	"time"
)

type Handler struct {
	orderWorker worker.OrderWorker
	blogWorker  worker.BlogWorker
}

// NewHandler creates new instance of analytics handler.
func NewHandler(orderWorker worker.OrderWorker, blogWorker worker.BlogWorker) *Handler {
	return &Handler{orderWorker: orderWorker, blogWorker: blogWorker}
}

// Routes for the chi router.
func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Get("/popular-destinations", h.popularDestinations)
	r.Get("/earnings", h.earnings)
	r.Get("/popular-blogs", h.popularBlogs)

	return r
}

// @Summary Popular destinations (admin)
// @Tags Analytics
// @Description This endpoint is used to get the popularity of the ordered destinations.
// @Accept json
// @Produce json
// @Param from query string false "starting time filter"
// @Param to query string false "ending time filter"
// @Success 200 {object} []order.DestinationAnalytics
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/analytics/popular-destinations [get]
func (h *Handler) popularDestinations(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	req := new(models.AnalyticsRequest)

	if param := r.URL.Query().Get("from"); param != "" {
		from, err := time.Parse(time.RFC3339, param)
		if err != nil {
			api.HTTPError(w, err, http.StatusBadRequest)
			return
		}

		req.From = &from
	}

	if param := r.URL.Query().Get("to"); param != "" {
		to, err := time.Parse(time.RFC3339, param)
		if err != nil {
			api.HTTPError(w, err, http.StatusBadRequest)
			return
		}

		req.To = &to
	}

	res, err := h.orderWorker.PopularDestinations(r.Context(), req)
	switch err {
	case nil:
	case errors.ErrInvalidTimeFrame:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}

// @Summary Total earning for the paid orders (admin)
// @Tags Analytics
// @Description This endpoint is used to get the total earnings number for the orders.
// @Accept json
// @Produce json
// @Param from query string false "starting time filter"
// @Param to query string false "ending time filter"
// @Success 200 {object} api.EarningsResponse
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/analytics/earnings [get]
func (h *Handler) earnings(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	req := new(models.AnalyticsRequest)

	if param := r.URL.Query().Get("from"); param != "" {
		from, err := time.Parse(time.RFC3339, param)
		if err != nil {
			api.HTTPError(w, err, http.StatusBadRequest)
			return
		}

		req.From = &from
	}

	if param := r.URL.Query().Get("to"); param != "" {
		to, err := time.Parse(time.RFC3339, param)
		if err != nil {
			api.HTTPError(w, err, http.StatusBadRequest)
			return
		}

		req.To = &to
	}

	res, err := h.orderWorker.Earnings(r.Context(), req)
	switch err {
	case nil:
	case errors.ErrInvalidTimeFrame:
		api.HTTPError(w, err, http.StatusBadRequest)
		return
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	j := &api.EarningsResponse{Earning: res}

	render.JSON(w, r, j)
}

// @Summary Popular liked blogs (admin)
// @Tags Analytics
// @Description This endpoint is used to get the popularity of the blogs' liking.
// @Accept json
// @Produce json
// @Success 200 {object} map[string]float64
// @Failure 400 {object} api.ErrResponse
// @Failure 401 {object} api.ErrResponse
// @Failure 409 {object} api.ErrResponse
// @Failure 500 {object} api.ErrResponse
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /api/v1/analytics/popular-blogs [get]
func (h *Handler) popularBlogs(w http.ResponseWriter, r *http.Request) {
	if r.Context().Value("is_admin") != true {
		api.HTTPError(w, errors.ErrAccessDenied, http.StatusUnauthorized)
		return
	}

	res, err := h.blogWorker.PopularBlogs(r.Context())
	switch err {
	case nil:
	default:
		api.HTTPError(w, err, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, res)
}
