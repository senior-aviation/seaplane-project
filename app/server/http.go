package server

import (
	"context"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/senior-aviation/seaplane/app/config"
	"gitlab.com/senior-aviation/seaplane/app/database"
	authModels "gitlab.com/senior-aviation/seaplane/app/models/auth"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/analytics"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/api"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/auth"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/blog"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/message"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/order"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/payment"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/port"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/price"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/profile"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/promocode"
	"gitlab.com/senior-aviation/seaplane/app/server/handlers/subscription"
	"gitlab.com/senior-aviation/seaplane/app/worker"
	"log"
	"net/http"
	"strings"
	"time"

	httpSwagger "github.com/swaggo/http-swagger"
)

const (
	apiV1 = "/api/v1"

	splitNumber = 2

	readTimeout  = time.Second * 5
	writeTimeout = time.Second * 40
)

// Serve runs the HTTP server.
func Serve(configuration *config.Configuration, db database.DataStore) {
	s := http.Server{
		Addr:         configuration.ListenAddress,
		Handler:      mountRouter(db),
		ReadTimeout:  readTimeout,
		WriteTimeout: writeTimeout,
	}

	log.Printf("[INFO] serving HTTP server on %s", configuration.ListenAddress)

	if err := s.ListenAndServe(); err != nil {
		log.Println("[ERROR] starting the HTTP server", err)
		return
	}
}

// mountRouter mounts the routing for the HTTP server.
func mountRouter(db database.DataStore) chi.Router {
	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(fetchJWT)
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{http.MethodGet, http.MethodPost, http.MethodPut, http.MethodDelete},
		AllowedHeaders: []string{"*"},
	}))

	profileWorker := worker.NewProfileWorker(db.Profile())
	authWorker := worker.NewAuthWorker(db.Profile())
	orderWorker := worker.NewOrderWorker(db.Order(), db.Price(), db.Profile(), db.Port(), db.PromoCode())
	priceWorker := worker.NewPriceWorker(db.Price())
	portWorker := worker.NewPortWorker(db.Port())
	paymentWorker := worker.NewPaymentWorker()
	blogWorker := worker.NewBlogWorker(db.Blog(), db.Profile())
	subscriptionWorker := worker.NewSubscriptionWorker(db.Subscription(), db.Profile())
	promoCodeWorker := worker.NewPromoCodeWalker(db.PromoCode())
	messageWorker := worker.NewMessageWorker(db.Message())

	r.Mount("/", httpSwagger.WrapHandler)

	r.Mount(apiV1+"/auth", auth.NewHandler(authWorker).Routes())
	r.Mount(apiV1+"/profile", profile.NewHandler(profileWorker).Routes())
	r.Mount(apiV1+"/order", order.NewHandler(orderWorker).Routes())
	r.Mount(apiV1+"/price", price.NewHandler(priceWorker).Routes())
	r.Mount(apiV1+"/port", port.NewHandler(portWorker).Routes())
	r.Mount(apiV1+"/payments", payment.NewHandler(paymentWorker, orderWorker).Routes())
	r.Mount(apiV1+"/blog", blog.NewHandler(blogWorker, profileWorker).Routes())
	r.Mount(apiV1+"/analytics", analytics.NewHandler(orderWorker, blogWorker).Routes())
	r.Mount(apiV1+"/subscription", subscription.NewHandler(subscriptionWorker).Routes())
	r.Mount(apiV1+"/promocode", promocode.NewHandler(promoCodeWorker).Routes())
	r.Mount(apiV1+"/message", message.NewHandler(messageWorker).Routes())

	return r
}

// fetchJWT fetches JWT token from an incoming header, and writes it to the context for the further verification.
func fetchJWT(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s := r.Header.Get("Authorization")

		splitToken := strings.Split(s, "Bearer")
		if len(splitToken) != splitNumber {
			handler.ServeHTTP(w, r.WithContext(r.Context()))
			return
		}

		token := strings.TrimSpace(splitToken[1])
		claims := authModels.JWTClaims{}

		if _, err := jwt.ParseWithClaims(token, &claims, func(token *jwt.Token) (interface{}, error) {
			return []byte(config.CurrentConfiguration.JWTKey), nil
		}); err != nil {
			api.HTTPError(w, err, http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(r.Context(), "is_admin", claims.IsAdmin)
		ctx = context.WithValue(ctx, "user_id", claims.Id)

		handler.ServeHTTP(w, r.WithContext(ctx))
	})
}
