package main

import (
	"context"
	"fmt"
	"github.com/caarlos0/env"
	"gitlab.com/senior-aviation/seaplane/app/config"
	"gitlab.com/senior-aviation/seaplane/app/start"
	_ "gitlab.com/senior-aviation/seaplane/swagger"
	"log"
	"os"
)

const version = "dev"

// @title Seaplane Project API Documentation
// @version 1.0
// @description This is the documentation for the Seaplane Project API
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host one-aviation.herokuapp.com
// @BasePath /
// @query.collection.format multi
func main() {
	log.Printf("WELCOME TO THE ONE AVIATION SERVICE, version: %s", version)

	log.Println("[INFO] the service started")

	c := config.NewConfiguration()

	if err := env.Parse(c); err != nil {
		log.Println("[ERROR] configuration parsing error:", err)
	}

	c.ListenAddress = os.Getenv("PORT")

	c.ListenAddress = fmt.Sprintf(":%s", c.ListenAddress)

	config.CurrentConfiguration = *c

	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	start.Start(ctx, c)

	log.Println("[INFO] the service finished")
}
