package config

// TODO: get rid of this global variable!

var CurrentConfiguration Configuration

// Configuration stores the information to configure the service.
type Configuration struct {
	DatabaseURL   string `env:"DB_URL"`         // database access URL
	DatabaseName  string `env:"DB_NAME"`        // database name
	ListenAddress string `env:"LISTEN_ADDR"`    // HTTP listen address
	JWTKey        string `env:"JWT_KEY"`        // JWT signature key
	EmailLogin    string `env:"EMAIL_LOGIN"`    // company's email login
	EmailPassword string `env:"EMAIL_PASSWORD"` // company's email password
}

// NewConfiguration returns the configuration structure filled with the default values.
func NewConfiguration() *Configuration {
	return &Configuration{
		DatabaseURL:   "mongodb://localhost:27017",
		DatabaseName:  "one-aviation",
		ListenAddress: ":8080",
		JWTKey:        "hesoyam",
	}
}
