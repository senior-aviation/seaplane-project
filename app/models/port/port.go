package port

import "time"

// Port stores the available ports for the seaplanes to land.
type Port struct {
	ID        string    `json:"id" bson:"_id" example:"4af9f0700faf68c3039d0749"`            // ID of the port
	CreatedAt time.Time `json:"created_at" bson:"created_at" example:"1970-01-01T07:00:00Z"` // port's creation time
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at" example:"1970-01-01T07:00:00Z"` // port's last updated time
	Name      string    `json:"name" bson:"name" example:"East Coast"`                       // port's name
	Latitude  float64   `json:"latitude" bson:"latitude" example:"41.90"`                    // latitude of the port
	Longitude float64   `json:"longitude" bson:"longitude" example:"12.49"`                  // longitude of the port
}
