package profile

import (
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models/document"
	"net/mail"
	"time"
)

// Profile contains the information about the profile.
type Profile struct {
	ID          string             `json:"id" bson:"_id" example:"4af9f0700faf68c3039d0749"`            // profile's internal id
	CreatedAt   time.Time          `json:"created_at" bson:"created_at" example:"1970-01-01T07:00:00Z"` // the profile creation time
	UpdatedAt   time.Time          `json:"updated_at" bson:"updated_at" example:"1970-01-01T07:00:00Z"` // the profile's last updated time
	FirstName   string             `json:"first_name" bson:"first_name" example:"Luke"`                 // user's first name
	LastName    string             `json:"last_name" bson:"last_name" example:"Skywalker"`              // user's last name
	MiddleName  string             `json:"middle_name" bson:"middle_name" example:"Anakin"`             // user's middle name
	BirthDate   time.Time          `json:"birth_date" bson:"birth_date" example:"1970-01-01T07:00:00Z"` // user's birthdate
	PhoneNumber string             `json:"phone_number" bson:"phone_number" example:"77071234567"`      // user's phone number
	Email       string             `json:"email" bson:"email" example:"luke.skywalker@gmail.com"`       // user's email
	Password    string             `json:"password" bson:"password" example:"qwerty123"`                // user's encrypted password
	Document    *document.Document `json:"document" bson:"document"`                                    // user's document information
	LikedBlogs  []string           `json:"-" bson:"liked_blogs"`                                        // user's liked blogs' IDs
	IsAdmin     bool               `json:"is_admin" bson:"is_admin" example:"false"`                    // the flag showing if the profile has admin privileges
	IsActive    bool               `json:"is_active" bson:"is_active" example:"true"`                   // user's activity flag
}

// Validate validates the input of the profile information.
func (u *Profile) Validate() error {
	if u == nil {
		return errors.ErrEmptyStruct
	}

	if len(u.PhoneNumber) != 0 {
		if len(u.PhoneNumber) != 11 && u.PhoneNumber[0] != '7' {
			return errors.ErrInvalidPhoneNum
		}
	}

	if len(u.Email) != 0 {
		if !IsEmailValid(u.Email) {
			return errors.ErrInvalidEmail
		}
	}

	if u.Document != nil {
		if _, ok := document.Types()[u.Document.Type]; !ok {
			return errors.ErrInvalidDocType
		}
	}

	return nil
}

// IsEmailValid validates the provided email address.
func IsEmailValid(e string) bool {
	_, err := mail.ParseAddress(e)

	return err == nil
}
