package profile

import (
	"gitlab.com/senior-aviation/seaplane/app/errors"
)

// Filter store the information to filter the profile' list result.
type Filter struct {
	FirstName   *string `json:"first_name" bson:"first_name" example:"Luke"`            // user's first name
	LastName    *string `json:"last_name" bson:"last_name" example:"Skywalker"`         // user's last name
	MiddleName  *string `json:"middle_name" bson:"middle_name" example:"Anakin"`        // user's middle name
	PhoneNumber *string `json:"phone_number" bson:"phone_number" example:"77071234567"` // user's phone number
	Email       *string `json:"email" bson:"email" example:"luke.skywalker@gmail.com"`  // user's email
	IsActive    *bool   `json:"is_active" bson:"is_active" example:"true"`              // profile's activity flag
}

// Validate validates the provided info that will be used as search filters.
func (f *Filter) Validate() error {
	if f == nil {
		return errors.ErrEmptyStruct
	}

	if f.PhoneNumber != nil {
		if len(*f.PhoneNumber) != 0 {
			toCheck := *f.PhoneNumber
			if len(toCheck) != 11 && toCheck[0] != '7' {
				return errors.ErrInvalidPhoneNum
			}
		}
	}

	if f.Email != nil {
		if !IsEmailValid(*f.Email) {
			return errors.ErrInvalidEmail
		}
	}

	return nil
}
