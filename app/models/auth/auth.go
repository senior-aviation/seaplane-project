package auth

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models/profile"
)

type JWTClaims struct {
	jwt.StandardClaims
	IsAdmin bool `json:"is_admin"`
}

// RegistrationInfo contains the user registration information.
type RegistrationInfo struct {
	FirstName   string `json:"first_name" example:"Luke"`                // user's first name
	LastName    string `json:"last_name" example:"Skywalker"`            // user's last name
	MiddleName  string `json:"middle_name" example:"Anakin"`             // user's middle name
	PhoneNumber string `json:"phone_number" example:"77071234567"`       // user's phone number
	Email       string `json:"email" example:"luke.skywalker@gmail.com"` // user's email
	Password    string `json:"password" example:"qwerty123"`             // user's encrypted password
}

// Validate validates the registration request information.
func (ri *RegistrationInfo) Validate() error {
	if ri.FirstName == "" {
		return errors.ErrNoFirstName
	}

	if ri.LastName == "" {
		return errors.ErrNoLastName
	}

	if ri.Password == "" {
		return errors.ErrNoPassword
	}

	if ri.Email == "" || !profile.IsEmailValid(ri.Email) {
		return errors.ErrInvalidEmail
	}

	return nil
}
