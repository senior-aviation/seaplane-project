package order

import (
	"gitlab.com/senior-aviation/seaplane/app/models/document"
)

// Direction of the passenger.
type Direction string

const (
	Forward  Direction = "FORWARD"
	BackWard Direction = "BACKWARD"
	Full     Direction = "FULL"
)

// Directions returns the valid directions.
func Directions() map[Direction]struct{} {
	return map[Direction]struct{}{
		Forward:  {},
		BackWard: {},
		Full:     {},
	}
}

// Passenger stores the information about the flight passenger.
type Passenger struct {
	FirstName   string             `json:"first_name" bson:"first_name" example:"Han"`                       // passenger's first name
	LastName    string             `json:"last_name" bson:"last_name" example:"Solo"`                        // passenger's last name
	MiddleName  string             `json:"middle_name" bson:"middle_name" example:"Corellia"`                // passenger's middle name
	PhoneNumber string             `json:"phone_number,omitempty" bson:"phone_number" example:"77071234567"` // passenger's phone number
	Email       string             `json:"email,omitempty" bson:"email" example:"han.solo@gmail.com"`        // passenger's email address`
	Document    *document.Document `json:"document" bson:"document"`                                         // passenger's identity document number
	Direction   Direction          `json:"direction" bson:"direction" enums:"FORWARD,BACKWARD,FULL"`         // passenger's direction
	UserID      string             `json:"-" bson:"user_id" example:"4af9f0700faf68c3039d0749"`              // passenger's internal user ID
	Status      Status             `json:"status" bson:"status" example:"ACTIVE"`                            // passenger's status
}
