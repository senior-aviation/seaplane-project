package order

import "time"

// Sharing stores the information needed to organize the shared flights.
type Sharing struct {
	AvailableSeats int  `json:"available_seats" bson:"available_seats" example:"4"` // the number of remaining seats to be shared
	Shareable      bool `json:"shareable" bson:"shareable" example:"true"`          // the flag showing the availablity of the flight sharing
}

// SharingOrderInfo stores the information for the potential clients of the shareable races.
type SharingOrderInfo struct {
	ID             int       `json:"id" bson:"_id" example:"215487"`                                      // the order ID
	Status         Status    `json:"status" bson:"status" example:"ACTIVE"`                               // the order's status
	From           *Location `json:"from" bson:"from"`                                                    // departure port's coordinates
	To             *Location `json:"to" bson:"to"`                                                        // departure port's coordinates
	AvailableSeats int       `json:"available_seats" bson:"available_seats" example:"4"`                  // the number of remaining seats to be shared
	DepartureTime  time.Time `json:"departure_time" bson:"departure_time" example:"1970-01-01T07:00:00Z"` // departure time
	ArrivalTime    time.Time `json:"arrival_time" bson:"arrival_time" example:"1970-01-01T07:00:00Z"`     // arrival time
	Price          string    `json:"price" bson:"price" example:"95"`                                     // order's total price
}

// SharingOrderFilters store the filtering information for the shareable orders list.
type SharingOrderFilters struct {
	LocationFrom       *Location  `json:"location_from"`                            // desired departure location
	LocationTo         *Location  `json:"location_to"`                              // desired target location
	DateFrom           *time.Time `json:"date_from" example:"1970-01-01T07:00:00Z"` // starting desired date of travelling
	DateTo             *time.Time `json:"date_to" example:"1970-01-01T07:00:00Z"`   // ending desired date of travelling
	NumberOfPassengers *int       `json:"number_of_passengers" example:"4"`         // the number of passengers to travel
	Flexible           bool       `json:"flexible" example:"true"`                  // the flexibility of the search +- 5 days
}
