package order

import "gitlab.com/senior-aviation/seaplane/app/errors"

// Filter store the information to filter the order's list result.
type Filter struct {
	OrderID *int    `json:"order_id" bson:"order_id" example:"522149"`                 // order's ID
	Status  *Status `json:"status" bson:"status" example:"FINISHED"`                   // order's status
	UserID  *string `json:"user_id" bson:"user_id" example:"614984b01a0d82c10710fbdb"` // customer's internal ID
}

// Validate validates the input order filters.
func (f *Filter) Validate() error {
	if f == nil {
		return errors.ErrEmptyStruct
	}

	if f.Status != nil {
		if _, ok := Statuses()[*f.Status]; !ok {
			return errors.ErrInvalidStatus
		}
	}

	return nil
}
