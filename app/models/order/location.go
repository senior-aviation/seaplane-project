package order

// Location stores the coordinates of the ports.
type Location struct {
	Latitude  float64 `json:"latitude" bson:"latitude" example:"41.90"`   // latitude of the port
	Longitude float64 `json:"longitude" bson:"longitude" example:"12.49"` // longitude of the port
	Name      string  `json:"name" bson:"name" example:"Madrid"`          // name of the port
}

// DestinationAnalytics stores the destination popularity information.
type DestinationAnalytics struct {
	Location   *Location `json:"location"`   // location of the destination
	Popularity float64   `json:"popularity"` //popularity of the destination
}
