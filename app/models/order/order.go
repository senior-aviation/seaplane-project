package order

import (
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models/document"
	"strconv"
	"time"
)

const (
	ACTIVE    Status = "ACTIVE"
	CANCELED  Status = "CANCELED"
	WAITING   Status = "WAITING"
	FINISHED  Status = "FINISHED"
	INPROCESS Status = "IN PROCESS"
)

const (
	bitSize = 64
)

// Status of the order.
type Status string

// Statuses returns the valid order statuses.
func Statuses() map[Status]struct{} {
	return map[Status]struct{}{
		ACTIVE:    {},
		CANCELED:  {},
		WAITING:   {},
		FINISHED:  {},
		INPROCESS: {},
	}
}

// Order holds the information about the orders.
type Order struct {
	ID                 int                `json:"id" bson:"_id" example:"215487"`                                                            // order's ID
	CreatedAt          time.Time          `json:"created_at" bson:"created_at" example:"1970-01-01T07:00:00Z"`                               // order's creation time
	UpdatedAt          time.Time          `json:"updated_at" bson:"updated_at" example:"1970-01-01T07:00:00Z"`                               // order's last updated time
	Status             Status             `json:"status" bson:"status" example:"FINISHED"`                                                   // order's status
	From               *Location          `json:"from" bson:"from"`                                                                          // departure port's coordinates
	To                 *Location          `json:"to" bson:"to"`                                                                              // destination port's coordinates
	DepartureTime      time.Time          `json:"departure_time" bson:"departure_time" example:"1970-01-01T07:00:00Z"`                       // departure time
	ArrivalTime        time.Time          `json:"arrival_time" bson:"arrival_time" example:"1970-01-01T07:00:00Z"`                           // arrival time
	RoundDepartureTime time.Time          `json:"round_departure_time,omitempty" bson:"round_departure_time" example:"1970-01-01T07:00:00Z"` // roundtrip departure time
	RoundArrivalTime   time.Time          `json:"round_arrival_time,omitempty" bson:"round_arrival_time" example:"1970-01-01T07:00:00Z"`     // roundtrip arrival time
	Passengers         []*Passenger       `json:"passengers" bson:"passengers"`                                                              // order's passengers
	Sharing            *Sharing           `json:"sharing" bson:"sharing"`                                                                    // flight sharing information
	Price              string             `json:"price" bson:"price" example:"95"`                                                           // order's total price
	PhoneNumber        string             `json:"phone_number" bson:"phone_number" example:"77071234567"`                                    // customer's phone number
	Email              string             `json:"email" bson:"email" example:"luke.skywalker@gmail.com"`                                     // customer's email
	UserID             string             `json:"user_id" bson:"user_id" example:"614984b01a0d82c10710fbdb"`                                 // customer's internal ID
	JoinedUsers        []string           `json:"joined_users" bson:"joined_users" example:"614984b01a0d82c10710fbdb"`                       // array of joined user ids
	Document           *document.Document `json:"document" bson:"document"`                                                                  // customer's identity document
	FirstName          string             `json:"first_name" bson:"first_name" example:"Han"`                                                // customer's first name
	LastName           string             `json:"last_name" bson:"last_name" example:"Solo"`                                                 // customer's last name
	MiddleName         string             `json:"middle_name" bson:"middle_name" example:"Corellia"`                                         // customer's middle name
}

// Validate validates the order information.
func (o *Order) Validate() error {
	if o == nil {
		return errors.ErrEmptyStruct
	}

	if o.Status != "" {
		if _, ok := Statuses()[o.Status]; !ok {
			return errors.ErrInvalidStatus
		}
	}

	if _, err := strconv.ParseFloat(o.Price, bitSize); err != nil {
		return errors.ErrInconvertiblePrice
	}

	return nil
}
