package models

import (
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"net/http"
	"strconv"
)

// Paging is used to organize the responses by the needed parameters.
type Paging struct {
	Limit    *int64 // maximum number of objects per page
	Page     *int64 // the page of the objects to be returned
	OrderBy  string // the order key by which the result should be sorted
	OrderKey int    // sets ascending (1) or descending (-1) order of the objects
}

// FetchPagingInfo fetches the paging information from the HTTP request queries.
func FetchPagingInfo(r *http.Request) (*Paging, error) {
	p := new(Paging)

	if param := r.URL.Query().Get("limit"); param != "" {
		val, err := strconv.Atoi(param)
		if err != nil {
			return nil, err
		}

		if val < 1 {
			return nil, errors.ErrInvalidLimit
		}

		p.Limit = makeInt64Pointer(val)
	}

	if param := r.URL.Query().Get("page"); param != "" {
		val, err := strconv.Atoi(param)
		if err != nil {
			return nil, err
		}

		if val < 1 {
			return nil, errors.ErrInvalidPage
		}

		val -= 1

		if p.Limit != nil {
			val *= int(*(p.Limit))
		} else {
			val *= 10
		}

		p.Page = makeInt64Pointer(val)
	}

	var absentKey bool

	if param := r.URL.Query().Get("order_by"); param != "" {
		p.OrderBy = param
	} else {
		absentKey = true
		p.OrderBy = "created_at"
	}

	if param := r.URL.Query().Get("order_key"); param != "" {
		ok, err := strconv.Atoi(param)
		if err != nil {
			return nil, err
		}

		if ok != 1 && ok != -1 {
			return nil, errors.ErrInvalidOrderKey
		}

		p.OrderKey = ok
	} else if absentKey {
		p.OrderKey = -1
	} else {
		p.OrderKey = 1
	}

	return p, nil
}

// makeInt64Pointer makes *int64 from int for the pagination parameters.
func makeInt64Pointer(i int) *int64 {
	result := int64(i)

	return &result
}
