package document

type Type string

const (
	Passport Type = "PASSPORT"
	Driver   Type = "DRIVER'S LICENSE"
	Birth    Type = "BIRTH'S CERTIFICATE"
	National Type = "NATIONAL ID"
)

// Types returns the valid document types.
func Types() map[Type]struct{} {
	return map[Type]struct{}{
		Passport: {},
		Driver:   {},
		Birth:    {},
		National: {},
	}
}

// Document stores the personal identity document information.
type Document struct {
	Number string `json:"number" bson:"number" example:"1234567890"` // document number
	Type   Type   `json:"type" bson:"type" example:"passport"`       // document type
}
