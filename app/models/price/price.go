package price

import (
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"strconv"
	"time"
)

// Price holds the price per kilometer.
type Price struct {
	ID         string    `json:"id" bson:"_id" example:"4af9f0700faf68c3039d0749"`            // ID of the price
	CreatedAt  time.Time `json:"created_at" bson:"created_at" example:"1970-01-01T07:00:00Z"` // creation time
	UpdatedAt  time.Time `json:"updated_at" bson:"updated_at" example:"1970-01-01T07:00:00Z"` // last modification time
	PriceValue string    `json:"price_value" bson:"price_value" example:"94.99"`              // price value
}

// Validate validates the input of the price information.
func (p *Price) Validate() error {
	if p == nil {
		return errors.ErrEmptyStruct
	}

	if _, err := strconv.Atoi(p.PriceValue); err != nil {
		return errors.ErrInconvertiblePrice
	}

	return nil
}
