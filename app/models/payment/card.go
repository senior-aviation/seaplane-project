package payment

// CreditCard stores the credit card info.
type CreditCard struct {
	Number string `json:"number" example:"4242424242424242"`
	CVV    string `json:"cvv" example:"123"`
	Month  string `json:"month" example:"05"`
	Year   string `json:"year" example:"2026"`
	Name   string `json:"name" example:"LUKE SKYWALKER"`
}
