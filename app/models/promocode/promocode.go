package promocode

import (
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"gitlab.com/senior-aviation/seaplane/app/models/order"
	"strconv"
	"time"
)

type ProcessingState string

const (
	Profile     ProcessingState = "profile"
	Departure   ProcessingState = "departure"
	Destination ProcessingState = "destination"
	Price       ProcessingState = "price"
)

type ProfitType string

const (
	FIXED   ProfitType = "FIXED"
	PERCENT ProfitType = "PERCENT"
)

// Profit stores the information about the profit the promocode can offer.
type Profit struct {
	Type   ProfitType `json:"type" bson:"type" enums:"FIXED,PERCENT"` // promocode's type of benefit
	Amount int        `json:"amount" bson:"amount"`                   // promocode's amount of benefit
}

// PromoCode stores the promocodes information.
type PromoCode struct {
	ID        string                     `json:"id" bson:"_id" example:"4af9f0700faf68c3039d0749"`            // ID of the promocode
	CreatedAt time.Time                  `json:"created_at" bson:"created_at" example:"1970-01-01T07:00:00Z"` // promocode's creation time
	UpdatedAt time.Time                  `json:"updated_at" bson:"updated_at" example:"1970-01-01T07:00:00Z"` // promocode's last updated time
	Name      string                     `json:"name" bson:"name" example:"GIVEMEDISCOUNT123"`                // promocode's name
	StartDate *time.Time                 `json:"start_date" bson:"start_date" example:"1970-01-01T07:00:00Z"` // promocode's start date
	EndDate   *time.Time                 `json:"end_date" bson:"end_date" example:"1970-01-01T07:00:00Z"`     // promocode's end date
	Limit     *int                       `json:"limit" bson:"limit" example:"5"`                              // promocode's usage limit
	Condition map[ProcessingState]string `json:"condition" bson:"condition"`                                  // promocode's conditions to be applied
	Profit    *Profit                    `json:"profit" bson:"profit"`                                        // promocode's profit
	IsActive  bool                       `json:"is_active" bson:"is_active" example:"true"`                   // promocode's activity flag
}

// Validate validates the incoming promocode before being applied.
func (p *PromoCode) Validate(o *order.Order) error {
	if p == nil || o == nil {
		return errors.ErrEmptyStruct
	}

	if !p.IsActive {
		return errors.ErrPromoInactive
	}

	currentTime := time.Now()

	if p.StartDate != nil {
		start := (*p.StartDate).In(currentTime.Location())

		if start.After(currentTime) {
			return errors.ErrPromoUnavailable
		}
	}

	if p.EndDate != nil {
		end := (*p.EndDate).In(currentTime.Location())

		if end.Before(currentTime) {
			return errors.ErrPromoExpired
		}
	}

	if p.Limit != nil && *p.Limit < 1 {
		return errors.ErrPromoLimit
	}

	if !p.isUserEligible(o) {
		return errors.ErrNotEligible
	}

	return nil
}

// isUserEligible checks if the user is eligible to apply the promocode.
func (p *PromoCode) isUserEligible(o *order.Order) bool {
	if val, ok := p.Condition[Profile]; ok {
		if o.PhoneNumber == "" || o.PhoneNumber == val {
			return false
		}

		return true
	}

	if val, ok := p.Condition[Departure]; ok {
		if o.From == nil || o.From.Name == "" || o.From.Name != val {
			return false
		}

		return true
	}

	if val, ok := p.Condition[Destination]; ok {
		if o.To == nil || o.To.Name == "" || o.To.Name != val {
			return false
		}

		return true
	}

	if val, ok := p.Condition[Price]; ok {
		orderPrice, err := strconv.ParseFloat(o.Price, 64)
		if err != nil {
			return false
		}

		promoPrice, err := strconv.ParseFloat(val, 64)
		if err != nil {
			return false
		}

		if orderPrice < promoPrice {
			return false
		}

		return true
	}

	return true
}

// ValidateForModification validates the incoming promocode for creation.
func (p *PromoCode) ValidateForModification() error {
	if p == nil {
		return errors.ErrEmptyStruct
	}

	if p.Name == "" {
		return errors.ErrMissingPromoCodeName
	}

	if p.StartDate != nil && p.EndDate != nil {
		sd := *p.StartDate
		ed := *p.EndDate

		if sd.After(ed) {
			return errors.ErrInvalidDates
		}
	}

	if p.Limit != nil {
		if *p.Limit < 1 {
			return errors.ErrNegativeLimit
		}
	}

	if !validateCondition(p.Condition) {
		return errors.ErrPromoCondition
	}

	if p.Profit == nil || !p.Profit.validate() {
		return errors.ErrInvalidProfit
	}

	return nil
}

// validateCondition validates the incoming conditions of the promocode.
func validateCondition(condition map[ProcessingState]string) bool {
	if len(condition) != 1 {
		return false
	}

	if val, ok := condition[Profile]; ok && val == "" {
		return false
	} else if val != "" {
		return true
	}

	if val, ok := condition[Departure]; ok && val == "" {
		return false
	} else if val != "" {
		return true
	}

	if val, ok := condition[Destination]; ok && val == "" {
		return false
	} else if val != "" {
		return true
	}

	if val, ok := condition[Price]; ok && val != "" {
		if _, err := strconv.ParseFloat(val, 64); err != nil {
			return false
		}

		return true
	}

	return false
}

// validate the incoming profit.
func (p *Profit) validate() bool {
	if p == nil {
		return false
	}

	switch p.Type {
	case FIXED:
		if p.Amount <= 0 {
			return false
		}
	case PERCENT:
		if p.Amount > 100 || p.Amount <= 0 {
			return false
		}
	default:
		return false
	}

	return true
}
