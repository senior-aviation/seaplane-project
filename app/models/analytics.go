package models

import (
	"time"
)

type AnalyticsRequest struct {
	From *time.Time `json:"from"`
	To   *time.Time `json:"to"`
}
