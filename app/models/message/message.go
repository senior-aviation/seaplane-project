package message

import (
	"gitlab.com/senior-aviation/seaplane/app/errors"
	"net/mail"
	"time"
)

// Message stores the information of the messages sent by the clients.
type Message struct {
	ID          string    `json:"id" bson:"_id" example:"4af9f0700faf68c3039d0749"`                         // message's ID
	CreatedAt   time.Time `json:"created_at" bson:"created_at" example:"1970-01-01T07:00:00Z"`              // message's creation time
	Name        string    `json:"name" bson:"name" example:"Luke Skuwalker"`                                // user's name
	Email       string    `json:"email" bson:"email" example:"luke.skywalker@gmail.com"`                    // user's email
	Message     string    `json:"message" bson:"message" example:"Hello, I would like to fly to Tatooine!"` // user's message
	IsProcessed bool      `json:"is_processed" bson:"is_processed" example:"true"`                          // message's processing flag
}

// Validate validates the incoming message information.
func (m *Message) Validate() error {
	if m.Name == "" {
		return errors.ErrMissingName
	}

	if m.Email == "" {
		return errors.ErrMissingEmail
	}

	if m.Message == "" {
		return errors.ErrEmptyMessage
	}

	if _, err := mail.ParseAddress(m.Email); err != nil {
		return err
	}

	return nil
}
