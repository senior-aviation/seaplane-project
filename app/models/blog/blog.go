package blog

import (
	"time"
)

// Blog stores the information for the blogs to be visible on the platform.
type Blog struct {
	ID        string    `json:"id" bson:"_id" example:"4af9f0700faf68c3039d0749"`              // blog's ID
	CreatedAt time.Time `json:"created_at" bson:"created_at" example:"1970-01-01T07:00:00Z"`   // blog's creation time
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at" example:"1970-01-01T07:00:00Z"`   // blog's last updated time
	Title     string    `json:"title" bson:"title" example:"sales of the week"`                // blog's name
	Content   string    `json:"content" bson:"content" example:"do not miss this opportunity"` // blog's content
	Image     string    `json:"image" bson:"image" example:"https://giveMeAnImage/image.png"`  // blog's image
	Liked     bool      `json:"liked" bson:"-" example:"true"`                                 // shows whether the blog has been liked by the user
	IsActive  bool      `json:"is_active" bson:"is_active" example:"true"`                     // blog's activity flag
}
