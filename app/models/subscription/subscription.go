package subscription

import (
	"time"
)

// Subscription stores the information of the customers subscribed to the platform.
type Subscription struct {
	ID        string    `json:"id" bson:"_id" example:"4af9f0700faf68c3039d0749"`            // ID of the subscription
	CreatedAt time.Time `json:"created_at" bson:"created_at" example:"1970-01-01T07:00:00Z"` // subscription's creation time
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at" example:"1970-01-01T07:00:00Z"` // subscription's last updated time
	Email     string    `json:"email" bson:"email" example:"luke.skywalker@gmail.com"`       // email of the subscriber
	UserID    string    `json:"user_id" bson:"user_id" example:"4af9f0700faf68c3039d0749"`   // ID of the subscriber
	IsActive  bool      `json:"is_active" bson:"is_active" example:"true"`                   // subscription's activity flag
}
