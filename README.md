# ONE Aviation Seaplane Service

This repository contains the basic functionality for the seaplane service.

### The basic configurations for successful running are as follows:

`DB_URL: the URL of the database (ex: mongodb://localhost:27017)` <br />
`DB_NAME: the database name (ex: one-aviation)` <br />
`LISTEN_ADDR: the HTTP listen address (ex: :8080)` <br />
`JWT_KEY: the secret JWT Key (ex: qwerty)` <br />
`EMAIL_LOGIN: the email login to send the emails to the users (ex: test@gmail.com)` <br />
`EMAIL_PASSWORD: the password of the email (ex: qwerty123)` <br />

The documentation for API can be found on the following [link](https://one-aviation.herokuapp.com).

For any questions and suggestions regarding the functionality contact me on [Telegram](https://t.me/alandeqz) or [LinkedIn](https://kz.linkedin.com/in/alanseksenali).